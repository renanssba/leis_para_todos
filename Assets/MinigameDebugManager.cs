﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinigameDebugManager : MonoBehaviour {

  #if UNITY_EDITOR
  public GameObject cursorObject;

  public Sprite[] tokenSprites;

  [HideInInspector] public bool isPlotting;
  [HideInInspector] public TokenType currentTokenType;

  // Update is called once per frame
  void Update() {
    if (Application.isEditor){

      if(isPlotting) {
        if(Input.GetMouseButtonDown(0)) {
          PlotToken(currentTokenType);
        } else if(Input.GetMouseButtonDown(1)) {
          CancelPlot();
        }

        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        cursorObject.transform.position = new Vector3(mousePosition.x + 0.75f, mousePosition.y - 0.75f, 0f);
      }

      if(Input.GetKeyDown(KeyCode.Alpha1)) {
        SetDebugCursorPlotter(TokenType.Internet);
      } else if(Input.GetKeyDown(KeyCode.Alpha2)) {
        SetDebugCursorPlotter(TokenType.Conversation);
      } else if(Input.GetKeyDown(KeyCode.Alpha3)) {
        SetDebugCursorPlotter(TokenType.Statistics);
      } else if(Input.GetKeyDown(KeyCode.Alpha4)) {
        SetDebugCursorPlotter(TokenType.Reading);
      } else if(Input.GetKeyDown(KeyCode.Alpha5)) {
        SetDebugCursorPlotter(TokenType.MoveToken);
      } else if(Input.GetKeyDown(KeyCode.Alpha6)) {
        SetDebugCursorPlotter(TokenType.Focus);
      } else if(Input.GetKeyDown(KeyCode.Alpha7)) {
        SetDebugCursorPlotter(TokenType.Distraction);
      } else if(Input.GetKeyDown(KeyCode.Alpha8)) {
        switch(GridManager.instance.chapter) {
          case 1:
            SetDebugCursorPlotter(TokenType.GeneticFood);
            break;
          case 2:
            SetDebugCursorPlotter(TokenType.GunToken);
            break;
          case 3:
            SetDebugCursorPlotter(TokenType.HealthToken);
            break;
          case 4:
            SetDebugCursorPlotter(TokenType.CorruptionToken);
            break;
          default:
            break;
        }

      }
    }
  }

  void CancelPlot() {
    GridManager.instance.SetCanAct(true);
    cursorObject.SetActive(false);

    isPlotting = false;
  }

  void PlotToken(TokenType currentTokenType) {
    Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

    int roundedX = Mathf.RoundToInt(mousePosition.x);
    int roundedY = Mathf.RoundToInt(mousePosition.y);

    if(roundedX >= 0 && roundedX < GridManager.instance.sizeX
        && roundedY >= 0 && roundedY < GridManager.instance.sizeY) {
      
      GridItem tokenChanged = GridManager.instance.tokens[roundedX, roundedY];

      GridManager.instance.ChangeTokenType(tokenChanged, currentTokenType, false, false);
    }
  }

  void SetDebugCursorPlotter(TokenType tokenType) {
    GridManager.instance.SetCanAct(false);
    isPlotting = true;
    currentTokenType = tokenType;

    cursorObject.gameObject.SetActive(true);
    cursorObject.GetComponent<SpriteRenderer>().sprite = GetSprite(tokenType);
  }

  Sprite GetSprite(TokenType tokenType) {
    switch(tokenType) {
      case TokenType.Internet:
        return tokenSprites[0];
      case TokenType.Conversation:
        return tokenSprites[1];
      case TokenType.Statistics:
        return tokenSprites[2];
      case TokenType.Reading:
        return tokenSprites[3];
      case TokenType.MoveToken:
        return tokenSprites[4];
      case TokenType.Focus:
        return tokenSprites[5];
      case TokenType.Distraction:
        return tokenSprites[6];
      case TokenType.GeneticFood:
        return tokenSprites[7];
      case TokenType.GunToken:
        return tokenSprites[8];
      case TokenType.HealthToken:
        return tokenSprites[9];
      case TokenType.CorruptionToken:
        return tokenSprites[10];
      default:
        return tokenSprites[0];
    }
        
  }

  #endif
}
