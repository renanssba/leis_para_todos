using UnityEngine;
using System.Collections;
using DG.Tweening;
using System.Collections.Generic;

public class GridItem : MonoBehaviour {

	public TokenType tokenType;
	
	private Vector3 screenPoint;
	private Vector3 offset;

	[HideInInspector] public bool previewing = false;

	[HideInInspector] public int posX;
	[HideInInspector] public int posY;

	[HideInInspector] public int previewX;
	[HideInInspector] public int previewY;

  [HideInInspector] public bool isInMatch = false;
	[HideInInspector] public bool hasMatched = false;

  [HideInInspector] public bool hasQueryTaint = false;

	[HideInInspector] public bool willGeneticallyMutate = false;
	[HideInInspector] public bool resistCorrupted = false;

	[HideInInspector] public bool matchingVertically;
	[HideInInspector] public bool matchingHorizontally;

	[HideInInspector] public GridManager gridManager;
	[HideInInspector] public List<GridItem> horizontalSequence = new List<GridItem>();
	[HideInInspector] public List<GridItem> verticalSequence = new List<GridItem>();
	[HideInInspector] public int scoreTextValue = 0;
	[HideInInspector] public bool countedForPoints = false;

	private bool mouseDown = false;

	void Update(){
    
	}

  public void TriggerMatched(){
    GetComponent<SpriteRenderer>().DOFade(0f, 0.4f);
    KillItself();
  }

	public void SetPosition(int tempX, int tempY){
		posX = tempX;
		posY = tempY;
		gameObject.name = string.Format("Token [{0}][{1}]", posX, posY);
	}

	void OnMouseUp(){
		if (OnMouseDropItemEventHandler != null && !hasMatched && mouseDown){
			mouseDown = false;
			OnMouseDropItemEventHandler(this);
		}

		DOTween.Kill(gridManager.cross.GetComponent<CanvasGroup>());
		gridManager.cross.GetComponent<CanvasGroup>().DOFade(0f, 0.3f);
	}

	public void FlashWhite(){
		GetComponent<SpriteRenderer>().material.DOFloat(1f, "_FlashAmount", gridManager.MOVSPEED * 0.3f).SetLoops(2, LoopType.Yoyo);
	}

	public void KillItself(){
		Destroy(this.gameObject, 2.0f);
	}

	void OnMouseDrag(){
		if (!hasMatched && gridManager.canAct && mouseDown){
			GetComponent<SpriteRenderer>().sortingOrder = 3;
			Vector3 cursorPoint;
			if (Application.isMobilePlatform){
				cursorPoint = new Vector3(Input.touches[0].position.x, Input.touches[0].position.y, screenPoint.z);
			}
			else {
				cursorPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
			}
			Vector3 cursorPosition = Camera.main.ScreenToWorldPoint(cursorPoint) + offset;
			transform.position = cursorPosition;

			if (OnMouseDragOverEventHandler != null && !hasMatched && gridManager.canAct){
				OnMouseDragOverEventHandler(this);
			}
		}
	}

	void OnMouseDown(){	
    if (gridManager.canAct){			
			DOTween.Kill(gridManager.cross);
			gridManager.cross.gameObject.transform.position = this.transform.position;
			gridManager.cross.GetComponent<CanvasGroup>().DOFade(0.75f, 0.8f).SetLoops(-1, LoopType.Yoyo);
			mouseDown = true;
			screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
			offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));		
		}
		

	}

	public delegate void OnMouseDropItem(GridItem item);
	public delegate void OnMouseDragOver(GridItem item);
	public static event OnMouseDropItem OnMouseDropItemEventHandler;
	public static event OnMouseDragOver OnMouseDragOverEventHandler;

}
