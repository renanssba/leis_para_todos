﻿using System;
using System.Collections.Generic;

public class MinigameConsumableListFactory {
  
  private MinigameConsumableListFactory() {
  }

  public static ScoreConsumableList CreateScoreConsumableList(List<GridItem> tokenList, int flatPointsPerToken){
    return new ScoreConsumableList(tokenList, flatPointsPerToken);
  }
    
  public static MinigameMatch CreateMinigameMatch(List<GridItem> tokenList, MatchDirection direction){
    TokenType matchType = MinigameConsumableListFactory.FindTokenTypeFromList(tokenList);

    switch(matchType){
      case TokenType.MoveToken:
        return new MoveTokenMatch(tokenList, direction);
      case TokenType.Distraction:
        return new DistractionTokenMatch(tokenList, direction);
      case TokenType.Focus:
        return new FocusTokenMatch(tokenList, direction);
      case TokenType.GeneticFood:
        return new TransgenicTokenMatch(tokenList, direction);
      case TokenType.HealthToken:
        return new HealthTokenMatch(tokenList, direction);
      case TokenType.CorruptionToken:
        return new CorruptionTokenMatch(tokenList, direction);
      case TokenType.GunToken:
        return new GunTokenMatch(tokenList, direction);
      case TokenType.None:
        return null;
      default:
        return new ScoreTokenMatch(tokenList, direction);
    }
  }

  static private TokenType FindTokenTypeFromList(List<GridItem> tokenList){
    TokenType assumedType = tokenList[0].tokenType;

    // Check if they're all the same, otherwise it is not well formed and returns TokenType.None
    foreach (GridItem token in tokenList){
      if (token.tokenType != assumedType){
        return TokenType.None;
      }
    }

    return assumedType;
  }

}

