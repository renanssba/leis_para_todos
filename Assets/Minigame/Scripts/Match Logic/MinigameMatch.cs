using System;
using System.Collections.Generic;
using UnityEngine.Assertions;
using UnityEngine;

public abstract class MinigameMatch : MinigameConsumableList, IComparable {

	public TokenType tokenType;
  public MatchDirection direction;

  public MinigameMatch(List<GridItem> matchedTokens, MatchDirection direction) : base(matchedTokens){
    
    this.tokenType = matchedTokens[0].tokenType;
    this.direction = direction;   

    foreach(GridItem token in matchedTokens) {
      token.isInMatch = true;
    }

    if (matchedTokens.Count >= 8){
      AchievementsManager.UnlockAchievement("Jogada de mestre");
    }

    if (matchedTokens.Count >= 4){
      MinigameStatistics.timesMatch4 += 1;       
    }

	}



  #region utility methods

  int IComparable.CompareTo(object obj) {
    MinigameMatch otherMatch = obj as MinigameMatch;

    if (this.tokenType == TokenType.Focus) return -1;
    else if (otherMatch.tokenType == TokenType.Focus) return 1;

    else if (this.isScoreTokenMatch()) return -1;
    else if (otherMatch.isScoreTokenMatch()) return 1;

    else if (this.tokenType == TokenType.Distraction) return -1;
    else if (otherMatch.tokenType == TokenType.Distraction) return 1;

    else if (this.tokenType == TokenType.MoveToken) return -1;
    else if (otherMatch.tokenType == TokenType.MoveToken) return 1;

    else return 0;
  }

  public bool isScoreTokenMatch(){    
    if (tokenType == TokenType.Internet
        || tokenType == TokenType.Conversation
        || tokenType == TokenType.Reading
        || tokenType == TokenType.Statistics){
      return true;
    } else{
      return false;
    }
  }

  public Vector2 FindMatchMiddlePosition(){    
    if (tokensToConsume.Count % 2 == 0){
      // Even number
      return new Vector2(tokensToConsume[tokensToConsume.Count/2].posX+0.5f, tokensToConsume[tokensToConsume.Count/2].posY+0.5f);
    } else{
      // Odd number
      GridItem pivotToken = tokensToConsume[(tokensToConsume.Count/2)];
      return new Vector2(pivotToken.posX, pivotToken.posY);
    }
  }

  #endregion

}