﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class CorruptionTokenMatch : MinigameMatch {

  public CorruptionTokenMatch(List<GridItem> matchedTokens, MatchDirection direction) : base(matchedTokens, direction) {

  }

  public override void TriggerEffect() {
    
    GridManager manager = GridManager.instance;   
    List<GridItem> potentialMutates = new List<GridItem>();

    int matchPoints = Mathf.RoundToInt(manager.pointsPerMatch * manager.variables.corruptedTokenMultiplier * manager.focusLevel * GetMatchLengthBonus());
    int pointsPerMatchedToken = Mathf.RoundToInt((float) matchPoints / this.consumableListLength);

    manager.AddScore(matchPoints);
    manager.AddSuperBar(manager.variables.superMeterBuild);

    MinigameStatistics.totalPointsGainedByCorruptionToken += matchPoints;

    Debug.Log("Corruption token trigger, points gained: " + matchPoints);

    foreach(GridItem token in tokensToConsume) {  
      token.scoreTextValue += pointsPerMatchedToken;
      token.hasMatched = true;
      token.TriggerMatched();
      UIManager.instance.DisplayMatchText(new Vector2(token.posX, token.posY), token.scoreTextValue.ToString());
      UIManager.instance.ShootParticleBall(new Vector3(token.posX, token.posY), GridManager.instance.victoryBarSliders[0].transform.position);
    }

    if(direction == MatchDirection.Horizontal) {
      int fixedY = tokensToConsume[0].posY; // if it's horizontal, then the Y is fixed

      for(int x = 0; x < manager.sizeX; x++) {
        if(manager.tokens[x, fixedY].hasMatched == false) {
          potentialMutates.Add(manager.tokens[x, fixedY]);
        }
      }

    } else if(direction == MatchDirection.Vertical) {
      int fixedX = tokensToConsume[0].posX; // if it's vertical, then the X is fixed

      for(int y = 0; y < manager.sizeY; y++) {
        if(manager.tokens[fixedX, y].hasMatched == false) {
          potentialMutates.Add(manager.tokens[fixedX, y]);
        }
      }
    }

    List<GridItem> willMutateTokens = new List<GridItem>();
    foreach(GridItem token in potentialMutates) {     
      if(token.tokenType != TokenType.Distraction) {
        if(UnityEngine.Random.value <= (manager.corruptedTokenChance)) {         
          willMutateTokens.Add(token);
        }
      }
    }
    manager.ChangeTokenType(willMutateTokens, TokenType.Distraction);

  }

  private float GetMatchLengthBonus() {
    float bonus = 1f;

    switch(consumableListLength) {
      case 4:
        bonus = GridManager.instance.variables.match4;
        break;
      case 5:
        bonus = GridManager.instance.variables.match5;
        break;
      case 6:
        bonus = GridManager.instance.variables.match6;
        break;
      case 7:
        bonus = GridManager.instance.variables.match7;
        break;
      case 8:
        bonus = GridManager.instance.variables.match8;
        break;
      default:
        if(consumableListLength > 8)
          bonus = GridManager.instance.variables.match8;
        else
          bonus = 1f;
        break;
    }

    return bonus;
  }
}

