using System;
using System.Collections.Generic;

public class MoveTokenMatch : MinigameMatch {

  public MoveTokenMatch(List<GridItem> matchedTokens, MatchDirection direction) : base(matchedTokens, direction){



  }

  public override void TriggerEffect() {
    GridManager manager = GridManager.instance;

    int amountOfMovesAdded = this.consumableListLength-2 + (manager.focusLevel - 1);

    manager.AddMoves(amountOfMovesAdded);
    manager.AddSuperBar(manager.variables.superMeterBuild);

    string floatingText = "+" + amountOfMovesAdded + " TURNO";
    if (amountOfMovesAdded > 1) floatingText += "S"; //plural

    UIManager.instance.DisplayMatchText(this.FindMatchMiddlePosition(), floatingText);
    foreach (GridItem token in tokensToConsume){      
      token.hasMatched = true;
      token.TriggerMatched();

    }
  }
}

