﻿using System;
using UnityEngine;

public class MinigameUtils {

  public static float GetPartyLevelBonus(TokenType type){
    if (Persistence.GetInstance().GetVariableValue("isFreeplayMode") == 1){
      return 1.50f;
    }
    int points = 0;
    switch(type) {
      case TokenType.Conversation:
        points = Persistence.GetInstance().GetVariableValue("PPC");
        Debug.Log("Points PPC: " + points);
        break;
      case TokenType.Statistics:
        points = Persistence.GetInstance().GetVariableValue("PDN");
        Debug.Log("Points PDN: " + points);
        break;
      case TokenType.Internet:
        points = Persistence.GetInstance().GetVariableValue("PN");
        Debug.Log("Points PN: " + points);
        break;
      case TokenType.Reading:
        points = Persistence.GetInstance().GetVariableValue("PSH");
        Debug.Log("Points PSH: " + points);
        break;
      default:        
        break;
    }

    float levelBonus = ConvertPartyPointsToLevel(points);

    Debug.Log("Bonus: " + levelBonus);
    return levelBonus;
  }

  private static float ConvertPartyPointsToLevel(int points) {    
    if(points == 0)
      return 1f;

    int level = points / 40;
    Debug.Log("Level: " + level);
    if(level >= 4) {
      return 1.50f;
    } else if(level == 3) {
      return 1.30f;
    } else if(level == 2) {
      return 1.20f;
    } else if(level == 1) {
      return 1.10f;
    } else {
      return 1f;
    }

  }

}


