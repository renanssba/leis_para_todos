﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class HealthTokenMatch : MinigameMatch {

  public HealthTokenMatch(List<GridItem> matchedTokens, MatchDirection direction) : base(matchedTokens, direction){



  }

  public override void TriggerEffect() {
    GridManager manager = GridManager.instance;

   

    int lengthBonus = consumableListLength - 2; // match of 3 = bonus of 1, match of 4 = bonus of 2
    int focusBonus = manager.focusLevel - 1;

    int amountOfSuperAdded = manager.variables.healthTokenSkillmeterRate * (lengthBonus + focusBonus);

    Debug.Log("Will add " + amountOfSuperAdded);
    manager.AddSuperBar(amountOfSuperAdded);

    MinigameStatistics.totalSuperAddedByHealthToken += amountOfSuperAdded;

    foreach (GridItem token in tokensToConsume){
      token.hasMatched = true;
      token.TriggerMatched();
    }

  }
}

