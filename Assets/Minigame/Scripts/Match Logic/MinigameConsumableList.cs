using System;
using System.Collections.Generic;
using UnityEngine.Assertions;
using UnityEngine;

public abstract class MinigameConsumableList{
  public int consumableListLength;
  public List<GridItem> tokensToConsume;

  public MinigameConsumableList(List<GridItem> tokensToConsume){
    this.consumableListLength = tokensToConsume.Count;
    this.tokensToConsume = tokensToConsume;

  }

  public abstract void TriggerEffect();
}
