using System;
using System.Collections.Generic;
using UnityEngine;

public class FocusTokenMatch : MinigameMatch {

  public FocusTokenMatch(List<GridItem> matchedTokens, MatchDirection direction) : base(matchedTokens, direction){



  }

  public override void TriggerEffect() {
    GridManager manager = GridManager.instance;

    int amountOfFocusAdded = this.consumableListLength-2;

    manager.AddFocus(amountOfFocusAdded);
    manager.AddSuperBar(manager.variables.superMeterBuild);

    Debug.Log("Focus match trigger, focus gained: " + amountOfFocusAdded + "x");

    UIManager.instance.DisplayMatchText(this.FindMatchMiddlePosition(), "+" + amountOfFocusAdded + "x FOCO");
    foreach (GridItem token in tokensToConsume){      
      token.hasMatched = true;
      token.TriggerMatched();
    }
  }
}

