﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ScoreConsumableList : MinigameConsumableList {

  private int flatPointsPerToken;

  public ScoreConsumableList(List<GridItem> consumableList, int flatPointsPerToken) : base(consumableList){

    this.flatPointsPerToken = flatPointsPerToken;

  }
    

  public override void TriggerEffect() {
    GridManager manager = GridManager.instance;

    float bonusMulti = 1f;

    int gainedPoints = 0;
    foreach (GridItem token in tokensToConsume){
      int tokenPoints;
      tokenPoints = Mathf.RoundToInt(this.flatPointsPerToken * MinigameUtils.GetPartyLevelBonus(token.tokenType) * manager.focusLevel);
      token.scoreTextValue = tokenPoints; 

      gainedPoints += tokenPoints;
    }
      
    int pointsPerMatchedToken = Mathf.RoundToInt((float) gainedPoints / this.consumableListLength);

    manager.AddScore(gainedPoints);
    manager.AddSuperBar(manager.variables.superMeterBuild);

    Debug.Log("Score consumable list trigger, gained points: " + gainedPoints);

    if (tokensToConsume.Count >= 50){
      AchievementsManager.UnlockAchievement("Sede de conhecimento");
    }

    foreach (GridItem token in tokensToConsume){      
      token.hasMatched = true;
      token.TriggerMatched();
      UIManager.instance.DisplayMatchText(new Vector2(token.posX, token.posY), token.scoreTextValue.ToString());
      UIManager.instance.ShootParticleBall(new Vector3(token.posX, token.posY), GridManager.instance.victoryBarSliders[0].transform.position);
    }
  }

}


