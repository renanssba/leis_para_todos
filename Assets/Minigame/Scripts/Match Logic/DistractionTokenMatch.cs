using System;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DistractionTokenMatch : MinigameMatch {

  public DistractionTokenMatch(List<GridItem> matchedTokens, MatchDirection direction) : base(matchedTokens, direction){

  }

  public override void TriggerEffect() {
    
    GridManager manager = GridManager.instance;

    int lostPoints = Mathf.RoundToInt(manager.distractionDamage * GetMatchLengthBonus());
    int lostPointsPerMatchedToken = Mathf.RoundToInt((float) lostPoints / this.consumableListLength);

    manager.AddScore(-lostPoints);
    Debug.Log("Distraction match trigger, lost points: " + (-lostPoints));
    Camera.main.DOShakePosition(0.25f, 0.3f, 20, 20f);

    foreach (GridItem token in tokensToConsume){
      token.scoreTextValue -= lostPointsPerMatchedToken;
      token.hasMatched = true;
      token.TriggerMatched();
      UIManager.instance.DisplayMatchText(new Vector2(token.posX, token.posY), token.scoreTextValue.ToString());
    }
  }

  private float GetMatchLengthBonus() {
    float bonus = 1f;

    switch(consumableListLength) {
      case 4:
        bonus = GridManager.instance.variables.match4;
        break;
      case 5:
        bonus = GridManager.instance.variables.match5;
        break;
      case 6:
        bonus = GridManager.instance.variables.match6;
        break;
      case 7:
        bonus = GridManager.instance.variables.match7;
        break;
      case 8:
        bonus = GridManager.instance.variables.match8;
        break;
      default:
        if(consumableListLength > 8)
          bonus = GridManager.instance.variables.match8;
        else
          bonus = 1f;
        break;
    }

    return bonus;
  }
}

