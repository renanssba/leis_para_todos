﻿using System;
using System.Collections.Generic;

public class MinigameMatchChecker {
  public GridItem[,] board;
  private int sizeX;
  private int sizeY;

  public MinigameMatchChecker(GridItem[,] board) {
    this.board = board;

    this.sizeX = board.GetLength(0);
    this.sizeY = board.GetLength(1);


  }

  public List<MinigameMatch> FindAllMatches(){
    List<MinigameMatch> foundMatches = new List<MinigameMatch>();

    for (int j = 0 ; j < sizeY ; j++) {
      for (int i = 0; i < sizeX; i++) {
        List<GridItem> potentialMatch = new List<GridItem>();

        GridItem token = board[i, j];
        potentialMatch.Add(token);

        int offset = 1;
        // Start checking for matches using offset in the RIGHT -> direction 
        while( (i+offset) < sizeX  && board[i + offset, j].tokenType == token.tokenType) {
          potentialMatch.Add(board[i + offset, j]);
          offset++;

        }

        // Broke the "match checking":
        if(potentialMatch.Count >= 3) {
          MinigameMatch match = MinigameConsumableListFactory.CreateMinigameMatch(potentialMatch, MatchDirection.Horizontal);
          foundMatches.Add(match);
          i += potentialMatch.Count;
        }

      }
    }

    for (int i = 0 ; i < sizeX ; i++) {
      for (int j = 0; j < sizeY; j++) {
        List<GridItem> potentialMatch = new List<GridItem>();

        GridItem token = board[i, j];
        potentialMatch.Add(token);

        int offset = 1;
        // Start checking for matches using offset in a UPWARDS /\ direction
        while( (j+offset) < sizeY  && board[i, j+offset].tokenType == token.tokenType) {
          potentialMatch.Add(board[i, j+offset]);
          offset++;

        }

        // Broke the "match checking":
        if(potentialMatch.Count >= 3) {
          MinigameMatch match = MinigameConsumableListFactory.CreateMinigameMatch(potentialMatch, MatchDirection.Vertical);
          foundMatches.Add(match);
          j += potentialMatch.Count;
        }

      }
    }

    return foundMatches;
  }

}


