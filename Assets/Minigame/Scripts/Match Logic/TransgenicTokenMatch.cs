﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class TransgenicTokenMatch : MinigameMatch {

  public TransgenicTokenMatch(List<GridItem> matchedTokens, MatchDirection direction) : base(matchedTokens, direction){

  }

  public override void TriggerEffect() {
    GridManager manager = GridManager.instance;   
    List<GridItem> tokensToMutate = new List<GridItem>();

    int matchPoints = Mathf.RoundToInt(manager.variables.pointsPerTransgenicMatch * manager.focusLevel * GetMatchLengthBonus());
    int pointsPerMatchedToken = Mathf.RoundToInt((float) matchPoints / this.consumableListLength);

    foreach (GridItem token in tokensToConsume){
      token.scoreTextValue += pointsPerMatchedToken;
      token.hasMatched = true;
      token.TriggerMatched();
      UIManager.instance.DisplayMatchText(new Vector2(token.posX, token.posY), token.scoreTextValue.ToString());
      UIManager.instance.ShootParticleBall(new Vector3(token.posX, token.posY), GridManager.instance.victoryBarSliders[0].transform.position);
    }

    int bonusChecks = ( (consumableListLength-2) + (manager.focusLevel-1) ) - 1;

    Debug.Log("Bonus checks for transgenic matches: 1");

    if (direction == MatchDirection.Horizontal){
      int fixedY = tokensToConsume[0].posY; // if it's horizontal, then the Y is fixed

      tokensToMutate.AddRange(GetUnmatchedTokensFromLine(fixedY));

      int offset = 1;

      while(bonusChecks > 0) {
        bonusChecks--;

        tokensToMutate.AddRange(GetUnmatchedTokensFromLine(fixedY + offset));

        if(offset > 0) {
          offset = -offset;
        } else if(offset < 0) {
          offset = -offset;
          offset++;
        }
      }

    } else if (direction == MatchDirection.Vertical){
      int fixedX = tokensToConsume[0].posX; // if it's vertical, then the X is fixed

      tokensToMutate.AddRange(GetUnmatchedTokensFromColumn(fixedX));

      int offset = 1;

      while(bonusChecks > 0) {
        bonusChecks--;

        tokensToMutate.AddRange(GetUnmatchedTokensFromColumn(fixedX + offset));

        if(offset > 0) {
          offset = -offset;
        } else if(offset < 0) {
          offset = -offset;
          offset++;
        }
      }

    }

    List<GridItem> willMutateTokens = new List<GridItem>();
    foreach(GridItem token in tokensToMutate) {

      if(UnityEngine.Random.value <= (manager.variables.geneticTokenChance)) {
        willMutateTokens.Add(token);
      }
    }

    manager.ChangeTokenTypeToRandom(willMutateTokens);
    MinigameStatistics.timesTransgenicEffectTriggered += willMutateTokens.Count;

    Debug.Log("Transgenic match trigger, gained points: " + matchPoints + ", mutated tokens: " + willMutateTokens.Count);

  }

  private List<GridItem> GetUnmatchedTokensFromLine(int line){
    List<GridItem> tokensToReturn = new List<GridItem>();
    GridManager manager = GridManager.instance;

    if(line >= 0 && line < manager.sizeY) {
      for(int x = 0; x < manager.sizeX; x++) {
        if(manager.tokens[x, line].hasMatched == false) {
          tokensToReturn.Add(manager.tokens[x, line]);
        }
      }
    }

    return tokensToReturn;

  }

  private List<GridItem> GetUnmatchedTokensFromColumn(int column){
    List<GridItem> tokensToReturn = new List<GridItem>();
    GridManager manager = GridManager.instance;

    if(column >= 0 && column < manager.sizeX) {
      for(int y = 0; y < manager.sizeY; y++) {
        if(manager.tokens[column, y].hasMatched == false) {
          tokensToReturn.Add(manager.tokens[column, y]);
        }
      }
    }

    return tokensToReturn;
  }

  private float GetMatchLengthBonus(){
    float bonus = 1f;

    switch(consumableListLength){
      case 4:
        bonus = GridManager.instance.variables.match4;
        break;
      case 5:
        bonus = GridManager.instance.variables.match5;
        break;
      case 6:
        bonus = GridManager.instance.variables.match6;
        break;
      case 7:
        bonus = GridManager.instance.variables.match7;
        break;
      case 8:
        bonus = GridManager.instance.variables.match8;
        break;
      default:
        if (consumableListLength > 8) bonus = GridManager.instance.variables.match8;
        else bonus = 1f;
        break;
    }

    return bonus;
  }
}

