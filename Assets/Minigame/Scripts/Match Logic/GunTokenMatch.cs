﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class GunTokenMatch : MinigameMatch {

  public GunTokenMatch(List<GridItem> matchedTokens, MatchDirection direction) : base(matchedTokens, direction){

  }

  public override void TriggerEffect() {
    GridManager manager = GridManager.instance;   
    List<GridItem> tokensToDestroy = new List<GridItem>();

    int matchPoints = Mathf.RoundToInt(manager.variables.pointsPerGunMatch * manager.focusLevel * GetMatchLengthBonus());
    int pointsPerMatchedToken = Mathf.RoundToInt((float) matchPoints / this.consumableListLength);

    foreach(GridItem token in tokensToConsume) {
      token.scoreTextValue += pointsPerMatchedToken;
      token.hasMatched = true;
      token.TriggerMatched();
      UIManager.instance.DisplayMatchText(new Vector2(token.posX, token.posY), token.scoreTextValue.ToString());
      UIManager.instance.ShootParticleBall(new Vector3(token.posX, token.posY), GridManager.instance.victoryBarSliders[0].transform.position);
    }

    manager.AddScore(matchPoints);
    manager.AddSuperBar(manager.variables.superMeterBuild);

    Debug.Log("GunTokenMatch!");
    int bonusChecks = ((consumableListLength - 3) + manager.focusLevel);

    // Inside this for loop, check for 3 tokens to destroy, where 1~3 are distraction tokens. Destroying != consuming.
    // Repeat for each bonusCheck (hence, the for loop).
    for(int i = 0; i < bonusChecks; i++) {
      List<GridItem> tokens = new List<GridItem>();

      int numberOfDistractionTokens = UnityEngine.Random.Range(1, 4);
      MinigameStatistics.timesGunEffectDestroyedDistraction += numberOfDistractionTokens;

      // Fill with distraction tokens, from 1 to a random number between 1~3
      for(int j = 0; j < numberOfDistractionTokens; j++) {
        GridItem token = manager.FindRandomTokenOfType(TokenType.Distraction, MinigameQueryType.Equal, false, true);

        if(token == null) {
          tokens.Add(manager.FindRandomTokenOfType(TokenType.Distraction, MinigameQueryType.NotEqual, false, true));
        } else {
          tokens.Add(token);
        }
      }

      // Now fill the remaining with non-distraction tokens 'til you have 3    
      int remainingNonDistractionTokensToFill = 3 - numberOfDistractionTokens;
      for(int j = 0; j < remainingNonDistractionTokensToFill; j++) {
        tokens.Add(manager.FindRandomTokenOfType(TokenType.Distraction, MinigameQueryType.NotEqual, false, true));
      }
             
      tokensToDestroy.AddRange(tokens);
    }         

    foreach(GridItem token in tokensToDestroy) {
      token.isInMatch = true;
      token.hasMatched = true;
      token.TriggerMatched();
    }





  }

  private float GetMatchLengthBonus(){
    float bonus = 1f;

    switch(consumableListLength){
      case 4:
        bonus = GridManager.instance.variables.match4;
        break;
      case 5:
        bonus = GridManager.instance.variables.match5;
        break;
      case 6:
        bonus = GridManager.instance.variables.match6;
        break;
      case 7:
        bonus = GridManager.instance.variables.match7;
        break;
      case 8:
        bonus = GridManager.instance.variables.match8;
        break;
      default:
        if (consumableListLength > 8) bonus = GridManager.instance.variables.match8;
        else bonus = 1f;
        break;
    }

    return bonus;
  }
}

