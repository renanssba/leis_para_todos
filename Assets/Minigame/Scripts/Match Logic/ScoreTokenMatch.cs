﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ScoreTokenMatch : MinigameMatch {

  public ScoreTokenMatch(List<GridItem> matchedTokens, MatchDirection direction) : base(matchedTokens, direction){
    
  }

  public override void TriggerEffect() {
    GridManager manager = GridManager.instance;

    float bonusMulti = 1f;

    int matchPoints = Mathf.RoundToInt(manager.pointsPerMatch * manager.focusLevel
                                       * GetMatchLengthBonus() * MinigameUtils.GetPartyLevelBonus(this.tokenType));
    int pointsPerMatchedToken = Mathf.RoundToInt((float) matchPoints / this.consumableListLength);

    manager.AddScore(matchPoints);
    manager.AddSuperBar(manager.variables.superMeterBuild);

    Debug.Log("Score match trigger, gained points: " + matchPoints);

    foreach (GridItem token in tokensToConsume){
      token.scoreTextValue += pointsPerMatchedToken;
      token.hasMatched = true;
      token.TriggerMatched();
      UIManager.instance.DisplayMatchText(new Vector2(token.posX, token.posY), token.scoreTextValue.ToString());
      UIManager.instance.ShootParticleBall(new Vector3(token.posX, token.posY), GridManager.instance.victoryBarSliders[0].transform.position);
    }



  }



  private float GetMatchLengthBonus(){
    float bonus = 1f;

    switch(consumableListLength){
      case 4:
        bonus = GridManager.instance.variables.match4;
        break;
      case 5:
        bonus = GridManager.instance.variables.match5;
        break;
      case 6:
        bonus = GridManager.instance.variables.match6;
        break;
      case 7:
        bonus = GridManager.instance.variables.match7;
        break;
      case 8:
        bonus = GridManager.instance.variables.match8;
        break;
      default:
        if (consumableListLength > 8) bonus = GridManager.instance.variables.match8;
        else bonus = 1f;
        break;
    }

    return bonus;
  }
}

