﻿using System;
using UnityEngine;

public class MinigameAudioController {

  private static void PlaySfx(string soundName){
    AudioController.GetInstance().PlaySfx(soundName);
  }

  public static void PlayMatchSound(int intensity){
    switch(intensity) {
      case 1:
        MinigameAudioController.PlaySfx("puzzle/ match1");
        break;
      case 2:
        MinigameAudioController.PlaySfx("puzzle/ match2");
        break;
      case 3:
        MinigameAudioController.PlaySfx("puzzle/ match3");
        break;
      default:
        MinigameAudioController.PlaySfx("puzzle/ match3");
        break;
    }
  }

  public static void PlayCancelSound(){
    MinigameAudioController.PlaySfx("puzzle/peca_cancelar");
  }

  public static void PlaySkillBarSound(){
    MinigameAudioController.PlaySfx("puzzle/barra_habilidade");
  }

  public static void PlayScoreBarSound(){
    MinigameAudioController.PlaySfx("puzzle/barra_pontos");
  }

  public static void PlayBadMatchSound(){
    MinigameAudioController.PlaySfx("puzzle/match_ruim");
  }
}


