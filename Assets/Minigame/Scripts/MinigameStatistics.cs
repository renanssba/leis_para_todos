﻿using System;


public class MinigameStatistics {

  public static int timesUsedSkills;
  public static int timesMatch4;
  public static int timesFilledResearchMeter;
  public static int timesTransgenicEffectTriggered;
  public static int timesGunEffectDestroyedDistraction;
  public static int totalSuperAddedByHealthToken;
  public static int totalPointsGainedByCorruptionToken;

  public static void InitializeMinigameStatistics(){
    timesUsedSkills = 0;
    timesMatch4 = 0;
    timesFilledResearchMeter = 0;
    timesTransgenicEffectTriggered = 0;
    timesGunEffectDestroyedDistraction = 0;
    totalSuperAddedByHealthToken = 0;
    totalPointsGainedByCorruptionToken = 0;
  }

  public static void UpdateProgressAchievements(){
    AchievementsManager.IncrementAchievement("Saúde de Ferro", totalSuperAddedByHealthToken);
    AchievementsManager.IncrementAchievement("Quanto mais, melhor", timesMatch4);
    AchievementsManager.IncrementAchievement("Habilidoso", timesUsedSkills);
    AchievementsManager.IncrementAchievement("Pesquisador", timesFilledResearchMeter);
    AchievementsManager.IncrementAchievement("Mudando o jogo", timesTransgenicEffectTriggered);
    AchievementsManager.IncrementAchievement("Desarmando", timesGunEffectDestroyedDistraction);
    AchievementsManager.IncrementAchievement("Corrupção", totalPointsGainedByCorruptionToken);

    AchievementsManager.CommitLocalAchievements();
  }

}


