﻿using System;

public class MelissaLevel1Skill : MinigameSkill{

  public MelissaLevel1Skill(){
    this.name = "melissa1";
    this.skillbarCost = 100;
  }

  public override void TriggerEffect() {
    GridManager gridManager = GridManager.instance;
    int flatPointsPerToken = gridManager.variables.albertoLv3FlatPoints;

    gridManager.AddSuperBar (-skillbarCost);

    gridManager.AddMoves(gridManager.focusLevel);
    gridManager.SetFocus(1);



  }

}


