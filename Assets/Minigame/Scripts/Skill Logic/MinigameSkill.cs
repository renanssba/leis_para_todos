﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MinigameSkill {

  public string name;
  public int skillbarCost;

  public abstract void TriggerEffect();

}
