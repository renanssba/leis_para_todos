﻿using System;
using UnityEngine;

public class MelissaLevel3Skill : MinigameSkill{

  public MelissaLevel3Skill(){
    this.name = "melissa3";
    this.skillbarCost = 300;
  }

  public override void TriggerEffect() {
    GridManager gridManager = GridManager.instance;

    gridManager.AddSuperBar(-skillbarCost);

    if(gridManager.canAct) {
      gridManager.AddSuperBar(-300);
      gridManager.melissaUltimateActive = true;
      gridManager.melissaUltimateSeconds = 20;
      gridManager.ultimateText.text = gridManager.melissaUltimateSeconds + " segundos";
      gridManager.StartMelissaCountdown();
    }
  }
    
}