﻿using System;
using System.Collections.Generic;

public class AlbertoLevel2Skill : MinigameSkill{

  public AlbertoLevel2Skill(){
    this.name = "alberto2";
    this.skillbarCost = 200;
  }

  public override void TriggerEffect() {
    GridManager gridManager = GridManager.instance;
    int flatPointsPerToken = gridManager.variables.albertoLv2FlatPoints;

    gridManager.AddSuperBar (-skillbarCost);

    if (gridManager.canAct) {
      List<GridItem> toDestroy = new List<GridItem> ();
      for (int x = 0; x < gridManager.sizeX; x++) {
        for (int y = 0; y < gridManager.sizeY; y++) {
          if (gridManager.tokens[x, y].tokenType == TokenType.Distraction) {
            gridManager.canAct = false;
            toDestroy.Add (gridManager.tokens [x, y]);
          }
        }
      }

      ScoreConsumableList consumableList = MinigameConsumableListFactory.CreateScoreConsumableList(toDestroy, flatPointsPerToken);

      gridManager.TriggerScoreConsumableList(consumableList);

    }
  }

}


