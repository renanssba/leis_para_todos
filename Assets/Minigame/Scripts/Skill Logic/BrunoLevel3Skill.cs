﻿using System;

public class BrunoLevel3Skill : MinigameSkill{

  public BrunoLevel3Skill(){
    this.name = "bruno3";
    this.skillbarCost = 300;
  }

  public override void TriggerEffect() {
    GridManager gridManager = GridManager.instance;

    gridManager.AddSuperBar(-skillbarCost);

    if(gridManager.canAct) {
      gridManager.brunoUltimateActive = true;
      gridManager.brunoUltimateTurns = 5;
      gridManager.ultimateText.text = gridManager.brunoUltimateTurns.ToString();
    }
  }

}


