﻿using System;
using System.Collections.Generic;

public class AlbertoLevel3Skill : MinigameSkill{

  public AlbertoLevel3Skill(){
    this.name = "alberto3";
    this.skillbarCost = 300;
  }

  public override void TriggerEffect() {
    GridManager gridManager = GridManager.instance;
    int flatPointsPerToken = gridManager.variables.albertoLv3FlatPoints;

    gridManager.AddSuperBar (-skillbarCost);

    if (gridManager.canAct) {
      List<GridItem> toDestroy = new List<GridItem> ();
      for (int x = 0; x < gridManager.sizeX; x++) {
        for (int y = 0; y < gridManager.sizeY; y++) {
          if (gridManager.tokens[x, y].tokenType == TokenType.Internet
              || gridManager.tokens[x, y].tokenType == TokenType.Conversation
              || gridManager.tokens[x, y].tokenType == TokenType.Statistics
              || gridManager.tokens[x, y].tokenType == TokenType.Reading) {
            gridManager.canAct = false;
            toDestroy.Add (gridManager.tokens [x, y]);
          }
        }
      }

      ScoreConsumableList consumableList = MinigameConsumableListFactory.CreateScoreConsumableList(toDestroy, flatPointsPerToken);

      gridManager.TriggerScoreConsumableList(consumableList);

    }
  }

}


