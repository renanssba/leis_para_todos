﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AlbertoLevel1Skill : MinigameSkill{

  public AlbertoLevel1Skill(){
    this.name = "alberto1";
    this.skillbarCost = 100;
  }
  
  public override void TriggerEffect() {
    
    GridManager gridManager = GridManager.instance;
    TokenType randomTokenType;
    int flatPointsPerToken = gridManager.variables.albertoLv1FlatPoints;

    randomTokenType = GetRandomTokenType();

    gridManager.AddSuperBar (-skillbarCost);

    if (gridManager.canAct) {
      List<GridItem> toDestroy = new List<GridItem> ();
      for (int x = 0; x < gridManager.sizeX; x++) {
        for (int y = 0; y < gridManager.sizeY; y++) {
          if (gridManager.tokens[x, y].tokenType == randomTokenType) {
            gridManager.canAct = false;
            toDestroy.Add (gridManager.tokens [x, y]);
          }
        }
      }

      ScoreConsumableList consumableList = MinigameConsumableListFactory.CreateScoreConsumableList(toDestroy, flatPointsPerToken);

      gridManager.TriggerScoreConsumableList(consumableList);

    }
  }
  
  private TokenType GetRandomTokenType() {
    int random = UnityEngine.Random.Range(0, 4);
    switch(random){
      case 0:
        return TokenType.Internet;
      case 1:
        return TokenType.Conversation;
      case 2:
        return TokenType.Reading;
      case 3:
        return TokenType.Statistics;
      default:
        return TokenType.None;
    }
  }
}


