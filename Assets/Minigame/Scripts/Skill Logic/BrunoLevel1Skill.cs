﻿using System;

public class BrunoLevel1Skill : MinigameSkill{

  public BrunoLevel1Skill(){
    this.name = "bruno1";
    this.skillbarCost = 100;
  }

  public override void TriggerEffect() {
    GridManager gridManager = GridManager.instance;

    gridManager.AddSuperBar (-skillbarCost);

    gridManager.AddFocus(2);

    }

}


