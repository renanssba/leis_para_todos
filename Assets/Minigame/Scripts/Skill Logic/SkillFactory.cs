﻿using System;


public class SkillFactory {
  private SkillFactory() {
  }

  public static MinigameSkill GetMinigameSkill(MinigameData.SkillIds skillId){
    MinigameSkill returnedSkill;

    switch(skillId){
      case MinigameData.SkillIds.AlbertoLv1:
        returnedSkill = new AlbertoLevel1Skill();
        break;
      case MinigameData.SkillIds.AlbertoLv2:
        returnedSkill = new AlbertoLevel2Skill();
        break;
      case MinigameData.SkillIds.AlbertoLv3:
        returnedSkill = new AlbertoLevel3Skill();
        break;
      case MinigameData.SkillIds.BrunoLv1:
        returnedSkill = new BrunoLevel1Skill();
        break;
      case MinigameData.SkillIds.BrunoLv2:
        returnedSkill = new BrunoLevel2Skill();
        break;
      case MinigameData.SkillIds.BrunoLv3:
        returnedSkill = new BrunoLevel3Skill();
        break;
      case MinigameData.SkillIds.MelissaLv1:
        returnedSkill = new MelissaLevel1Skill();
        break;
      case MinigameData.SkillIds.MelissaLv2:
        returnedSkill = new MelissaLevel2Skill();
        break;
      case MinigameData.SkillIds.MelissaLv3:
        returnedSkill = new MelissaLevel3Skill();
        break;
      default:
        returnedSkill = null;
        break;
    }

    return returnedSkill;
  }

}


