﻿using System;
using System.Collections.Generic;

public class BrunoLevel2Skill : MinigameSkill{

  public BrunoLevel2Skill(){
    this.name = "bruno2";
    this.skillbarCost = 200;
  }

  public override void TriggerEffect() {
    GridManager gridManager = GridManager.instance;


    gridManager.AddSuperBar (-skillbarCost);

    if (gridManager.canAct) {
      gridManager.canAct = false;
      List<GridItem> willChangeTokens = new List<GridItem> ();

      for (int x = 0; x < gridManager.sizeX; x++) {
        for (int y = 0; y < gridManager.sizeY; y++) {
          GridItem token = gridManager.tokens[x, y];
          if (token.tokenType == TokenType.Distraction) {
            willChangeTokens.Add(token);           
          }
        }
      }
      gridManager.ChangeTokenType(willChangeTokens, TokenType.Focus);

    }

  }


}

