﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;
using System.Collections.Generic;

public class MinigameSkillButton : MonoBehaviour {

  public Image[] stars;
  public int skillLevel;
  public MinigameSkill skill;
  public Text skillNameText;

  private Button buttonScript;

  public Sprite enabledButtonSprite;
  public Sprite disabledButtonSprite;

  public Sprite lightStar;
  public Sprite darkStar;

  public Sprite emptySkillSprite;

  // "onClick" logic is added dinamically on the UIManager

  public void Awake() {
    this.buttonScript = GetComponent<Button>();
  }

  public void AssociateSkill(MinigameData.SkillIds skillId) {
    this.skill = SkillFactory.GetMinigameSkill(skillId);
  }

  public void SetEnabled(bool value) {    
    if(value) {      
      // SKILL CAN BE USED

      skillNameText.color = new Color(1f, 1f, 1f, 1f);
      GetComponent<Image>().sprite = enabledButtonSprite;
      skillNameText.GetComponent<Shadow>().enabled = true;
      for(int i = 0; i < stars.Length; i++) {
        if(i < skillLevel) {
          stars[i].GetComponent<Image>().sprite = lightStar;
        } else {
          stars[i].GetComponent<CanvasGroup>().alpha = 1f;
        }
      }

    } else {
      // SKILL CANNOT BE USED

      GetComponent<Image>().sprite = disabledButtonSprite;
      skillNameText.color = new Color(0f, 0f, 0f, 1f);
      skillNameText.GetComponent<Shadow>().enabled = false;
      for(int i = 0; i < stars.Length; i++) {
        if(i < skillLevel) {
          stars[i].GetComponent<Image>().sprite = darkStar;
        } else {
          stars[i].GetComponent<CanvasGroup>().alpha = 0.5f;
        }
      }
    }
  }

  public void UpdateSkillName(string text) {
    skillNameText.text = text;
  }

  public void SetAsEmptySkill() {
    skillNameText.text = "";
    foreach(Image star in stars) {
      star.gameObject.SetActive(false);
    }

    GetComponent<Image>().sprite = emptySkillSprite;
    GetComponent<Button>().interactable = false;
  }
}
