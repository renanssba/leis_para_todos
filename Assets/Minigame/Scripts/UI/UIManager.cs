﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine.UI;

public class UIManager : MonoBehaviour{

  [HideInInspector] public static UIManager instance;

  public GameObject uiCanvas;
  public GameObject floatingTextPrefab;
  public GameObject skillButtonPrefab;
  public GameObject particleBall;

  public Button[] skillButtons;

  public Image nightBackground;
  public Text skillPointsText;

  private UIManager() {
  }

  void Awake(){
    instance = this;
  }

  public void SetSkillPoints(int amount){
    skillPointsText.text = "x" + amount;
  }

  public void ShootParticleBall(Vector3 fromPosition, Vector3 toPosition){
    int jumpMultiplier;
    if (Random.value <= 0.5f) jumpMultiplier = 1;
    else jumpMultiplier = -1;

    GameObject particleObject = Instantiate(particleBall, fromPosition, Quaternion.identity) as GameObject;


    float duration = Random.Range(0.5f, 1f);
    float delay = Random.Range(0f, 0.2f);
    particleObject.transform.DOJump(toPosition, jumpMultiplier * 1.5f, 1, duration).SetDelay(delay);
    particleObject.GetComponent<SpriteRenderer>().DOFade(0f, duration/2f).SetDelay(duration).OnComplete(()=>{
      Destroy(particleObject.gameObject);
    });
  }

  public void DisplayMatchText(Vector2 position, string text){
    GameObject floatingText = Instantiate(floatingTextPrefab, Vector3.zero, Quaternion.identity) as GameObject;
    floatingText.transform.SetParent(uiCanvas.transform);
    floatingText.transform.localScale = Vector3.one;
    floatingText.transform.position = position;
    floatingText.GetComponent<Text>().text = text;
    floatingText.GetComponent<MinigameFloatingText>().PlayFadeUpAnimation(1.3f);

    if (GridManager.instance.focusLevel > 1){
      // Colors: C72953FF, maybe EB59CAFF?
      floatingText.GetComponent<Text>().color = GridManager.instance.variables.focusColor;
    }
  }

  public void AttachSkillToButton(int skillButtonIndex, MinigameData.SkillIds skillId){
    Button skillButton = skillButtons[skillButtonIndex];

    if (skillId != MinigameData.SkillIds.NoSkill){
      if (skillButton != null){
        skillButton.GetComponent<MinigameSkillButton>().AssociateSkill(skillId);
        skillButton.GetComponent<Button>().onClick.AddListener(() => {
          if (GridManager.instance.canAct){
            Debug.Log("Used skill " + skillButton.GetComponent<MinigameSkillButton>().skill.name);
            MinigameStatistics.timesUsedSkills += 1;
            skillButton.GetComponent<MinigameSkillButton>().skill.TriggerEffect();
          }
        });
        skillButton.GetComponent<MinigameSkillButton>().UpdateSkillName(SkillsScreen.GetSkillName((int)skillId));
      }
    } else{
      //skillButton.gameObject.SetActive(false);
      skillButton.GetComponent<MinigameSkillButton>().SetAsEmptySkill();
    }
  }

  public void SetNightBackgroundPercent(float percent){    
    if (percent < 0f) percent = 0f;
    if (percent > 1f) percent = 1f;
    nightBackground.GetComponent<CanvasGroup>().alpha = percent;
  }

}

