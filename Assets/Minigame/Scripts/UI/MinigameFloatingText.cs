﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MinigameFloatingText : MonoBehaviour {

  public void PlayFadeUpAnimation(float duration){    
    GetComponent<CanvasGroup>().DOFade(0f, duration/2f).SetDelay(duration/2f).SetEase(Ease.Linear);
    transform.DOMoveY(0.25f, duration).SetRelative().SetEase(Ease.Linear).OnComplete(()=>{
      Destroy(this.gameObject);
    });
  }

}
