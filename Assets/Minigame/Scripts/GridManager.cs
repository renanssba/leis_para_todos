using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine.UI;

[System.Serializable]
public class MinigameVariables {
  [Header("Health Token Skillmeter Build Rate")]
  [Tooltip("(e.g \"8\" means every health token in a health match will grant 8 points to the skill bar")]

  public int healthTokenSkillmeterRate;

  [Header("Points per Transgenic Match")]
  [Tooltip("Base points for transgenic match with length 3")]

  public int pointsPerTransgenicMatch;

  [Header("Points per Gun Match")]
  [Tooltip("Base points for gun match with length 3")]

  public int pointsPerGunMatch;

  [Header("Alberto Lv1 Flat Points per Token")]
  [Tooltip("How many points a token consumed by Alberto's Lv1 skill are going to give (e.g 3)")]
  [Range(0, 20)]
  public int albertoLv1FlatPoints;

  [Header("Alberto Lv2 Flat Points per Token")]
  [Tooltip("How many points a token consumed by Alberto's Lv2 skill are going to give (e.g 3)")]
  [Range(0, 20)]
  public int albertoLv2FlatPoints;

  [Header("Alberto Lv3 Flat Points per Token")]
  [Tooltip("How many points a token consumed by Alberto's Lv3 skill are going to give (e.g 3)")]
  [Range(0, 20)]
  public int albertoLv3FlatPoints;

  [Header("Skill meter build rate")]
  [Tooltip("The number of ticks each match will fill the super bar. 100 Ticks = 1 super bar.")]

  public int superMeterBuild;

  [Header("Corrupted Token multiplier")]
  public int corruptedTokenMultiplier;

  [Header("Chance to spawn effect tokens")]
  [Tooltip("Chance is related to non-effect tokens. 100% = just as likely as non-effect tokens")]
  [Range(0f, 1f)]
  public float chanceEffectToken;

  [Header("Transgenic Token chance")]
  [Tooltip("Chance to modify tokens affected by the Transgenic Token")]
  [Range(0f, 1f)]
  public float geneticTokenChance;

  [Header("Health token super multiplier")]
  [Tooltip("superMeterBuild * ch2TokenSuperMeterMultiplier = meter built")]

  public int healthTokenSuperMultiplier;

  [Header("Multipliers for 4+ Tokens")]
  public float match4;
  public float match5;
  public float match6;
  public float match7;
  public float match8;

  [Header("Requirements for victory levels")]
  public int scoreReq1;
  public int scoreReq2;
  public int scoreReq3;

  [Header("Focus color")]
  public Color focusColor;



}

public class GridManager : MonoBehaviour {
  
  public MinigameVariables variables;
  public static GridManager instance;

  [HideInInspector] public int chapter;


  [HideInInspector] public int[] skillsID = new int[3];
	
  [Header("Height (default: 8)")]
  public int sizeX;

  [Header("Width (default: 7)")]
  public int sizeY;

  [Range(1, 50)]
  [Header("Initial moves (default: 15)")]
  public int initialMoves;

  private int victories = 0;
  private int totalSpawnedTokens = 0;

  [Header("Distraction damage (base match of 3)")]
  public int distractionDamage;

  [Header("Points (base match of 3) before bonus")]
  public int pointsPerMatch;

  [Header("Corrupted Token chance")]
  [Range(0f, 1f)]
  public float corruptedTokenChance;

  private int superMeter;
  private int moves;
	
  public float MOVSPEED;

  public GameObject dialogBox;

  public GameObject[] possibleTokens;

  [Header("All Skills")]
  public GameObject[] skillList;

  // Chapter tokens
  public GameObject transToken;
  public GameObject healthToken;
  public GameObject gunToken;
  public GameObject corruptedToken;

  public GameObject gainPointsText;

  [HideInInspector] public GridItem[,] tokens;

  [HideInInspector] public bool canAct = false;
  [HideInInspector] public float scaleDown = 1f;
  [HideInInspector] public int focusLevel = 1;
  [HideInInspector] public bool currentlyDoingCombo = false;
  [HideInInspector] public bool superBlock = false;
  [HideInInspector] public int doingComboLevel = 0;
  [HideInInspector] public bool powerUpActive = false;
  [HideInInspector] public bool brunoPowerUpActive = false;
  [HideInInspector] public bool gameStarted = false;
  [HideInInspector] public bool warnedAboutMovesLeft = false;
  [HideInInspector] public bool melissaLevel1Active = false;
  [HideInInspector] public bool sofiaLevel1Active = false;
  [HideInInspector] public int numberTokensToDestroy = 0;
  [HideInInspector] public bool isTutorialRunning = false;

  private bool emptyFocusOnEndOfMovement;
  private int totalTurnsPlayed = 0;
  private bool isFreeplayMode;

  private List<string> dialogLines;
  private int bookmark;

  [HideInInspector]public bool brunoUltimateActive = false;
  [HideInInspector]public int brunoUltimateTurns = 0;
  [HideInInspector]public bool melissaUltimateActive = false;
  [HideInInspector]public int melissaUltimateSeconds = 0;

  [HideInInspector] public int super = 0;

  public GameObject board;
  public Image bottomPanel;
  public Image rightPanel;
  public Image cross;
  public Image endgamePanel;
  public Image skillTooltip;

  public Text warningText;
  public Text scoreText;
  public Text movesText;
  public Text focusText;
  public Text ultimateText;
  public Text debugText;

  [HideInInspector] int score = 0;

  [HideInInspector] int currentLevel = 0;

  public Slider[] victoryBarSliders;
  public Text victoryLevelText;

  public Slider[] superBarSliders;
  public Text superBarCountText;

  void Awake() {
    if (Persistence.GetInstance().GetVariableValue("isFreeplayMode") == 1){
      isFreeplayMode = true;
    } else{
      isFreeplayMode = false;
    }

    MinigameStatistics.InitializeMinigameStatistics();
    SetMoveCount(initialMoves);
    UpdateSuperBarUI();
    this.chapter = MinigameData.chapter;
    instance = this;
  }


  public static GridManager GetInstance() {
    return instance;
  }


  public void OnMelissaCountdown() {
    if(melissaUltimateActive) {
      melissaUltimateSeconds--;
      ultimateText.text = melissaUltimateSeconds + " segundos";
      if(melissaUltimateSeconds == 0) {
//				MOVSPEED = MOVSPEED * 2f;
        melissaUltimateActive = false;
        ultimateText.text = "";
        GridManager.instance.CancelInvoke("MelissaCountdown");
        //			Camera.main.GetComponent<AudioSource>().pitch = 1f;
      }
    }

  }

  public void StartMelissaCountdown() {
    InvokeRepeating("OnMelissaCountdown", 1f, 1f);
  }

  public void ChangeRandomToken(TokenType t) {
    for(int x = 0; x < sizeX; x++) {
      for(int y = 0; y < sizeY; y++) {
        if(tokens[x, y].tokenType == t) {
          List<TokenType> tempPossibleTokens = new List<TokenType>();
          for(int i = 0; i < possibleTokens.Length; i++) {
            tempPossibleTokens.Add(possibleTokens[i].GetComponent<GridItem>().tokenType);
          }
          tempPossibleTokens.Remove(t);
          ChangeTokenType(tokens[x, y], tempPossibleTokens[Random.Range(0, tempPossibleTokens.Count)], false);
        }
      }
    }

    Invoke("DelayedCheckForMatches", MOVSPEED*0.3f);
  }

  private void DelayedCheckForMatches() {
    ExecuteNewMatches(false);
  }


  public void AddSuperBar(int amount) {
    if(superBlock == false) {
      int maxSuperBar = MinigameData.maxSuperBar;

      super += amount;
      if(super < 0)
        super = 0;
      if(super > maxSuperBar * 100)
        super = maxSuperBar * 100;

      UpdateSuperBarUI();
      RefreshUsableSkills();
    }
  }

  void UpdateSuperBarUI() {
    if(superBlock == false) {
      int maxSuperBar = MinigameData.maxSuperBar;
      int currentLevel = 0;
      int percent;

      for(int i = 0; i < 3; i++) {
        if(super >= (i + 1) * 100) {
          currentLevel = (i + 1);
        }

        percent = (super - 100 * i);
        if(super == maxSuperBar * 100)
          percent = 100;
        superBarSliders[i].DOValue(Mathf.Min(percent, 100), MOVSPEED*0.3f);
      }


      UIManager.instance.SetSkillPoints(currentLevel);

      RefreshUsableSkills();
    }
  }


  void RefreshUsableSkills() {    
    if(isFreeplayMode || Persistence.GetInstance().GetVariableValue("hasSkills") == 1) {
      foreach(Button skillButton in UIManager.instance.skillButtons) {
        MinigameSkillButton skillButtonInfo = skillButton.GetComponent<MinigameSkillButton>();
        if (skillButtonInfo.skill != null){
          if(skillButtonInfo.skill.skillbarCost <= super) {
            if(skillButton.GetComponent<Button>().interactable == false) {
              MinigameAudioController.PlaySkillBarSound();
              skillButton.GetComponent<Button>().interactable = true;
              skillButtonInfo.SetEnabled(true);
            }

          } else {
            skillButton.GetComponent<Button>().interactable = false;
            skillButtonInfo.SetEnabled(false);
          }
        }

      }
    }
  }    

  public void AddFocus(int amount) {
    focusLevel += amount;
    UpdateFocusText();

    if(focusLevel >= 12){
      AchievementsManager.UnlockAchievement("Foco inabalável"); //ACHIEVEMENTNAME
    }
  }

  public void SetFocus(int amount) {
    Debug.Log("Setting focus to " + amount);
    focusLevel = amount;
    UpdateFocusText();
  }

  public void ChangeTokenTypeToRandom(List<GridItem> tokens) {
    Debug.Log("Changing token types to random from a list");
    foreach(GridItem token in tokens) {
      List<TokenType> tempPossibleTokens = new List<TokenType>();
      for(int i = 0; i < possibleTokens.Length; i++) {
        tempPossibleTokens.Add(possibleTokens[i].GetComponent<GridItem>().tokenType);
      }
      tempPossibleTokens.Remove(token.tokenType);
      if(token.tokenType != TokenType.Distraction)
        tempPossibleTokens.Remove(TokenType.Distraction);


      ChangeTokenType(token, tempPossibleTokens[Random.Range(0, tempPossibleTokens.Count)], false);
    }

    //Invoke("DelayedCheckForMatches", 1f);

  }

  public void ChangeTokenType(List<GridItem> tokens, TokenType newType) {
    foreach(GridItem token in tokens) {
      ChangeTokenType(token, newType, false);
    }

    Invoke("DelayedCheckForMatches", 0.6f);

  }



  public void ChangeTokenType(GridItem token, TokenType newType, bool checkMatchesAfter, bool canActAfter = true) {
    float duration = MOVSPEED * 0.25f;
    token.gameObject.transform.DOPunchScale(new Vector3(1.25f, 1.25f), duration, 8, 2f);
    token.gameObject.GetComponent<SpriteRenderer>().material.DOFloat(1f, "_FlashAmount", duration).OnComplete(() => {
      InstantiateToken(token.posX, token.posY, false, newType);
      if (canActAfter){
        SetCanAct(true);
      }
      Destroy(token.gameObject);
    });

    /*
    if(checkMatchesAfter) {
      Invoke("DelayedCheckForMatches", 0.6f);
    }
    */

  }

  void DisplayWarningText(string str) {
    warningText.text = str;
    warningText.transform.position = new Vector3(3.5f, 4f);
    warningText.GetComponent<Text>().color = new Color(1f, 1f, 1f, 0f);
    float speed = MOVSPEED * 1.5f;
    warningText.DOFade(1f, speed);
    warningText.transform.DOMoveY(-1f, speed).SetRelative();
    warningText.DOFade(0f, speed).SetDelay(speed * 2);
    warningText.transform.DOMoveY(-1, speed).SetDelay(speed * 2);
  }


  void Update() {
    if(Application.isEditor && Input.GetKeyDown(KeyCode.R)) {
      RestartGame();
    }
    if(Application.isEditor && Input.GetKeyDown(KeyCode.O)) {
      AddSuperBar(50);
    } else if(Application.isEditor && Input.GetKeyDown(KeyCode.P)) {
      AddScore(200);
    } else if (Application.isEditor && Input.GetKeyDown(KeyCode.I)){
      AddFocus(1);
    } else if (Application.isEditor && Input.GetKeyDown(KeyCode.K)){
      AddMoves(-1);
    }
    if(Application.isEditor)
      debugText.text = "Debug Text\n\nCan Act? " + canAct + "\nComboing? " + currentlyDoingCombo;
  }

  public void RestartGame() {
    Application.LoadLevel(Application.loadedLevel);
  }

  void Start() {
    SetMoveCount(moves);
    scoreText.text = "" + score;
    FillGrid();
		
    GridItem.OnMouseDropItemEventHandler += OnMouseDropItem;
    GridItem.OnMouseDragOverEventHandler += OnMouseDragOver;

    bookmark = 0;
    dialogLines = new List<string>();

    if(Persistence.GetInstance() != null && isFreeplayMode == false) {
      if(chapter == 0) {
        AddMoves(-5);
        FillDialog(0);
      } else if(chapter == 1 && Persistence.GetInstance().GetVariableValue("tutorial_1_1") == 0) {
        FillDialog(1);
        //      MinigameData.hasSkills = false;
      } else if(chapter == 1 && Persistence.GetInstance().GetVariableValue("hasSkills") == 1 && Persistence.GetInstance().GetVariableValue("tutorial_1_2") == 0) {
        FillDialog(2);
      } else if(chapter == 2 && Persistence.GetInstance().GetVariableValue("tutorial_2") == 0) {
        FillDialog(3);
      } else if(chapter == 3 && Persistence.GetInstance().GetVariableValue("tutorial_3") == 0) {
        FillDialog(4);
      } else if(chapter == 4 && Persistence.GetInstance().GetVariableValue("tutorial_4") == 0) {
        FillDialog(5);
      }
    }
    
    Debug.Log("Amount of lines to run: " + dialogLines.Count);

    if(dialogLines.Count > 1) {
      isTutorialRunning = true;
      StartCoroutine("RunDialog");
    }

    FillSkills();

    AudioController.GetInstance().PlayMusic("pesquisa");
  }

  void FillDialog(int tutorial) {
    canAct = false;

    switch(tutorial) {
      case 0:
        dialogLines.Add("O sistema de pesquisa é representado por um minigame de combinar peças.");
        dialogLines.Add("Para avançar com a pesquisa, é necessário combinar 3 ou mais peças adjacentes da mesma cor.");
        dialogLines.Add("Para combiná-las, é possível escolher qualquer peça e arrastá-la na horizontal ou na vertical, quantas casas quiser...");
        dialogLines.Add("...contanto que esse movimento realize alguma combinação.");
        dialogLines.Add("Ao combinar as peças redondas, você ganha pontos, que vão enchendo a <color=orange>Barra de Pesquisa</color> ao lado direito do tabuleiro.");
        dialogLines.Add("Você tem uma quantidade limitada de turnos para encher a <color=orange>Barra de Pesquisa</color>. Quando eles acabarem, a partida termina.");
        dialogLines.Add("Complete a <color=orange>Barra de Pesquisa</color> para obter progresso em seu estudo. É possível até mesmo enchê-la mais de uma vez, para ter grandes progressos!");
        dialogLines.Add("Além das peças redondas, existem mais 3 tipos de peças: as <color=#B59325FF>Peças de Tempo</color>, <color=#E830D2FF>Peças de Foco</color> e <color=#000000FF>Peças de Distração</color>.");
        dialogLines.Add("As <color=#B59325FF>Peças de Tempo</color> são as amarelas. Ao combiná-las, você ganha um turno extra. São bem úteis!");
        dialogLines.Add("As <color=#E830D2FF>Peças de Foco</color> são as rosas. Ao combiná-las, você aumenta o seu <color=#E830D2FF>Multiplicador de Pontos</color>.");
        dialogLines.Add("Seu <color=#E830D2FF>Multiplicador de Pontos</color> fica logo abaixo do contador de turnos.");
        dialogLines.Add("Esse multiplicador afeta a sua próxima pontuação, mas zera logo em seguida. Use as <color=#E830D2FF>Peças de Foco</color> com sabedoria!");
        dialogLines.Add("Por último, há as <color=#000000FF>Peças de Distração</color>, que são as pretas. Tome muito cuidado para não combiná-las, pois elas diminuem bastante o seu progresso na <color=#E87930FF>Barra de Pesquisa</color>.");
        dialogLines.Add("Boa sorte!");
        dialogLines.Add("<stop>");
        break;
      case 1:
        dialogLines.Add("A partir de agora, Alberto está pesquisando sobre um Projeto de Lei.");
        dialogLines.Add("Cada projeto de Lei a ser estudado adicionará uma peça especial diferente ao tabuleiro.");
        dialogLines.Add("Como o PL atual é sobre alimentos transgênicos, a <color=#E830D2FF>Peça Transgênica</color> foi adicionada dessa vez. Ela é a peça roxa, com um símbolo de DNA.");
        dialogLines.Add("Combinar <color=#E830D2FF>Peças Transgênicas</color> dá uma pontuação menor do que as demais, mas elas têm um efeito especial no tabuleiro...");
        dialogLines.Add("Ao combiná-las em uma linha, as demais peças da linha irão se transformar em outras, abrindo novas possibilidades.");
        dialogLines.Add("Ao combiná-las em coluna, as peças da coluna se transformarão.");
        dialogLines.Add("Use seu efeito com sabedoria, e boa sorte em suas pesquisas!");
        dialogLines.Add("<stop>");
        Persistence.GetInstance().SetVariableValue("tutorial_1_1", 1);
        break;
      case 2:
        dialogLines.Add("Alberto possui <color=blue>Habilidades</color> que pode usar durante suas pesquisas, para aumentar sua efetividade.");
        dialogLines.Add("Para usá-las, você precisa encher a <color=blue>Barra de Especial</color>, que fica abaixo do tabuleiro.");
        dialogLines.Add("Essa barra enche naturalmente conforme você vai realizando combinações. Preste atenção quande encher para usar sua <color=blue>Habilidade</color>!");
        dialogLines.Add("A habilidade inicial de Alberto é a <color=blue>Leitura Dinâmica</color>. Ao usá-la, todas as <color=green>Peças de Internet (verdes)</color> são consumidas do tabuleiro, dando uma pequena pontuação por cada peça.");
        dialogLines.Add("Mais habilidades serão desbloqueadas durante o jogo, e você poderá montar suas estratégias.");
        dialogLines.Add("Use suas habilidades com sabedoria!");
        dialogLines.Add("<stop>");
        Persistence.GetInstance().SetVariableValue("tutorial_1_2", 1);
        break;
      case 3:
        dialogLines.Add("Neste PL atual, a <color=#E830D2FF>Peça de Desarmamento</color> está disponível. Ela é a peça roxa, com uma bala cortada.");
        dialogLines.Add("Combinar <color=#E830D2FF>Peças de Desarmamento</color> dá uma pontuação menor do que as demais, mas destrói uma <color=#E83030FF>Peça de Distração</color> e outra peça aleatória no tabuleiro.");
        dialogLines.Add("Use seu efeito com sabedoria!");
        dialogLines.Add("<stop>");
        Persistence.GetInstance().SetVariableValue("tutorial_2", 1);
        break;
      case 4:
        dialogLines.Add("Neste PL atual, a <color=#E830D2FF>Peça de Saúde</color> está disponível. Ela é a peça roxa, com uma cruz.");
        dialogLines.Add("Combinar <color=#E830D2FF>Peças de Saúde</color> não aumenta a sua <color=orange>Barra de Pesquisa</color> nem um pouco, mas aumenta bastante a sua <color=blue>Barra de Especial</color>.");
        dialogLines.Add("Use seu efeito com sabedoria!");
        dialogLines.Add("<stop>");
        Persistence.GetInstance().SetVariableValue("tutorial_3", 1);
        break;
      case 5:
        dialogLines.Add("Neste PL atual, a <color=#E830D2FF>Peça de Corrupção</color> está disponível. Ela é a peça roxa, com um símbolo de dinheiro vazando.");
        dialogLines.Add("Combinar <color=#E830D2FF>Peças de Corrupção</color> aumentam bastante a <color=orange>Barra de Pesquisa</color>, mas causa o surgimento de <color=#E83030FF>Peças de Distração</color> ao redor.");
        dialogLines.Add("Será que o risco de usá-la vale a pena?");
        dialogLines.Add("<stop>");
        Persistence.GetInstance().SetVariableValue("tutorial_4", 1);
        break;
      default:
        dialogLines.Add("<stop>");
        break;
    }
  }

  IEnumerator RunDialog() {
    SetCanAct(false);
    Coroutine playDialogSfx;
    // wait for board to appear completely
    yield return new WaitForSeconds(2.5f);

    dialogBox.SetActive(true);
    GameObject.Find("Character Name Text").GetComponent<Text>().text = "Informação";
    while(true) {
      SetCanAct(false);
      bool completed = false;
      if(dialogLines[bookmark] == "<stop>") {
        bookmark++;
        break;
      }

      playDialogSfx = StartCoroutine(PlayDialogSfx());
      dialogBox.transform.Find("Box").transform.Find("Dialog").GetComponent<Text>().DOText(dialogLines[bookmark], 0.02f * dialogLines[bookmark].Length, true).SetEase(Ease.Linear).OnComplete(() => {
        completed = true;
        StopCoroutine(playDialogSfx);
      });

      do {
        yield return null;
      } while (!HasReceivedInput());

      if(completed) {
        bookmark++;
      } else {
        DOTween.Kill(dialogBox.transform.Find("Box").transform.Find("Dialog").GetComponent<Text>());
        dialogBox.transform.Find("Box").transform.Find("Dialog").GetComponent<Text>().text = dialogLines[bookmark];
        StopCoroutine(playDialogSfx);
        do {
          yield return null;
        } while (!HasReceivedInput());
        bookmark++;
      }

      dialogBox.transform.Find("Box").transform.Find("Dialog").GetComponent<Text>().text = "";
    }

    canAct = true;
    isTutorialRunning = false;
    dialogBox.SetActive(false);
  }

  IEnumerator PlayDialogSfx() {
    while(true) {
      yield return new WaitForSeconds(0.075f);
//      yield return new WaitForSeconds(textWaitTime);
//      for(int i=4; i<4; i++){
      yield return null;
//      }
      AudioController.GetInstance().PlayDialogSound();
    }
  }

  bool HasReceivedInput() {
    if(Input.GetKeyDown(KeyCode.Space) ||
       Input.GetKeyDown(KeyCode.Return) ||
       Input.GetMouseButtonDown(0)) {
      return true;
    }
    return false;
  }

  public void SetCanAct(bool value) {   
    Debug.Log("Can act: " + value);
    canAct = value;
  }


  void FillSkills() {
    if(isFreeplayMode || Persistence.GetInstance().GetVariableValue("hasSkills") == 1) {
      Debug.Log("Skills are enabled");
      GameObject.Find("SkillList").SetActive(true);
      superBarSliders[0].gameObject.SetActive(true);

      if (Persistence.GetInstance().GetVariableValue("isFreeplayMode") == 0){
        skillsID[0] = Persistence.GetInstance().GetVariableValue("skillLevel1", -1);
        if (Persistence.GetInstance().GetVariableValue("unlockedSkillsLevel2") > 0){          
          skillsID[1] = Persistence.GetInstance().GetVariableValue("skillLevel2", -1);
        } else{
          skillsID[1] = -1;
        }
        if (Persistence.GetInstance().GetVariableValue("unlockedSkillsLevel3") > 0){
          skillsID[2] = Persistence.GetInstance().GetVariableValue("skillLevel3", -1);
        } else{
          skillsID[2] = -1;
        }
      }
      else{
        skillsID[0] = PlayerPrefs.GetInt("freeplaySkillLevel1", -1);
        skillsID[1] = PlayerPrefs.GetInt("freeplaySkillLevel2", -1);
        skillsID[2] = PlayerPrefs.GetInt("freeplaySkillLevel3", -1);
      }

      for (int i = 0; i < skillsID.Length; i++) {
        UIManager.instance.AttachSkillToButton(i, (MinigameData.SkillIds) skillsID[i]);
      }

      RefreshUsableSkills();
    } else {
      skillTooltip.gameObject.SetActive(false);
      GameObject.Find("SkillList").SetActive(false);
      superBarSliders[0].gameObject.SetActive(false);
    }
  }



  void OnDestroy() {
    GridItem.OnMouseDropItemEventHandler -= OnMouseDropItem;
    GridItem.OnMouseDragOverEventHandler -= OnMouseDragOver;
  }

  public void AddMoves(int amount) {
    SetMoveCount(moves + amount);
  }

  void SetMoveCount(int amount) {
    moves = amount;
    movesText.text = moves.ToString();
  }

  public void ExecuteNewMatches(bool playerIssued = false) {
    Debug.Log("Calling for ExecuteNewMatches, player issued: " + playerIssued);
    if(brunoUltimateActive && brunoUltimateTurns >= 1) {
      brunoUltimateTurns -= 1;
      ultimateText.text = "" + brunoUltimateTurns;
      if(brunoUltimateTurns == 0) {
        ultimateText.text = "";
        brunoUltimateActive = false;
        ExecuteNewMatches();
      }
      SetCanAct(true);
    } else {
      MinigameMatchChecker checker = new MinigameMatchChecker(tokens);
      List<MinigameMatch> matches = checker.FindAllMatches();

      if(matches.Count > 0) {
        doingComboLevel++;
        TriggerMatches(matches, playerIssued);

      } else {
        Debug.Log("No new matches found. Ending combo of level " + doingComboLevel);
        EndMovement();
      }
    }
      
  }

  private void ShiftRowsDown() {
    int count = 0;
    int emptyLevel = 0;
    for(int x = 0; x < sizeX; x++) {
      for(int y = 0; y < sizeY; y++) {
        count++;
        if(tokens[x, y] != null && tokens[x, y].hasMatched == true) {
          emptyLevel++;
        } else {
          //Debug.Log("Not empty!");
          if(emptyLevel > 0) {
            tokens[x, y].transform.DOMoveY(tokens[x, y].transform.position.y - emptyLevel, MOVSPEED*0.3f);
            tokens[x, y - emptyLevel] = tokens[x, y];
            tokens[x, y - emptyLevel].posY = y - emptyLevel;
          }
        }
      }
      for(int y = sizeY - emptyLevel; y < sizeY; y++) {
        tokens[x, y] = null;
      }
      emptyLevel = 0;
    }

    Invoke("RepopulateTokens", MOVSPEED*0.3f);
  }

  private void RepopulateTokens() {
    // Spawns more tokens to fall down! :)
    // After it happens, we have to re-check to see if new matches were made.

    for(int x = 0; x < sizeX; x++) {
      for(int y = 0; y < sizeY; y++) {
        if(tokens[x, y] == null) {
          tokens[x, y] = InstantiateToken(x, y, false);
        }
      }
    }

    Invoke("DelayedCheckForMatches", MOVSPEED*0.3f);
  }



  public void AddScore(int amount) {
    if((score + amount) < 0)
      amount = 0;
    score += amount;
    float percent = 0f;

    // Update bars
    switch(currentLevel) {
      case 0:
        percent = (float)(score) / (float)(variables.scoreReq1);

        if(percent >= 1) {
          MinigameAudioController.PlayScoreBarSound();
          victories = 1;
          MinigameStatistics.timesFilledResearchMeter += 1;
          victoryBarSliders[0].DOValue(1f, 1f);
          currentLevel++;
          victoryLevelText.text = currentLevel.ToString();
          AddScore(0);
        } else {
          victoryBarSliders[0].DOValue(percent, 1f);
        }
        break;
      case 1:
        percent = (float)(score - variables.scoreReq1) / (float)(variables.scoreReq2 - variables.scoreReq1);

        if(percent >= 1) {
          MinigameAudioController.PlayScoreBarSound();
          victories = 2;
          MinigameStatistics.timesFilledResearchMeter += 1;
          victoryBarSliders[1].DOValue(1f, 1f);
          currentLevel++;
          victoryLevelText.text = currentLevel.ToString();
          AddScore(0);
        } else {
          victoryBarSliders[1].DOValue(percent, 1f);
        }
        break;
      case 2:
        percent = (float)(score - variables.scoreReq2) / (float)(variables.scoreReq3 - variables.scoreReq2);
			
        if(percent >= 1) {
          MinigameAudioController.PlayScoreBarSound();
          victories = 3;
          MinigameStatistics.timesFilledResearchMeter += 1;
          AchievementsManager.UnlockAchievement("Esgotando as fontes");
          victoryBarSliders[2].DOValue(1f, 1f);
          currentLevel++;
          victoryLevelText.text = currentLevel.ToString();
        } else {
          victoryBarSliders[2].DOValue(percent, 1f);
        }
        break;
    }

    scoreText.text = "" + score;

    // Juicy stuff
    if(amount > 0) {
      // Juicy text!
      if(amount > 100)
        scoreText.transform.DOScale(2f, 0.05f);
			else
        scoreText.transform.DOScale(1.5f, 0.05f);
      scoreText.color = new Color32(255, 203, 3, 255);
      scoreText.DOColor(new Color(1f, 1f, 1f), 0.5f).SetDelay(0.3f).SetEase(Ease.InCubic);
      scoreText.transform.DOScale(1f, 0.5f).SetDelay(0.3f).SetEase(Ease.InCubic);


      // Juicy scorebar!
      victoryBarSliders[0].transform.DOScaleX(1.05f, 0.05f);
      victoryBarSliders[0].transform.DOScaleY(1.05f, 0.05f);
      victoryBarSliders[0].transform.DOScaleX(1f, 0.5f).SetDelay(0.3f).SetEase(Ease.InCubic);
      victoryBarSliders[0].transform.DOScaleY(1f, 0.5f).SetDelay(0.3f).SetEase(Ease.InCubic);
    }
  }

  void EndGame() {
    if(moves == 0) {
      SetCanAct(false);
      int r;
      for(int x = 0; x < sizeX; x++) {
        for(int y = 0; y < sizeY; y++) {
          r = Random.Range(1, 3);
          if(r == 1)
            r = -1;
          else
            r = 1;
          tokens[x, y].transform.DOMove(new Vector3(0.7f * r, -0.7f), MOVSPEED * 0.6f).SetRelative().SetEase(Ease.OutCubic).OnComplete(() => {
            //gi.KillItself();
						
          });
          tokens[x, y].GetComponent<SpriteRenderer>().DOFade(0f, MOVSPEED * 0.6f);
          r = Random.Range(1, 3);
          if(r == 1)
            r = -1;
          else
            r = 1;
          tokens[x, y].transform.DORotate(new Vector3(0f, 0f, -40f * r), MOVSPEED * 0.6f).SetRelative();
        }
      }

      endgamePanel.GetComponent<CanvasGroup>().blocksRaycasts = true;
      endgamePanel.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
      endgamePanel.transform.DOScale(1f, 0.75f).SetDelay(1f);
      endgamePanel.GetComponent<CanvasGroup>().DOFade(1f, 0.75f).SetDelay(1f);
      if(victories == 0) {
        endgamePanel.transform.FindChild("Message").GetComponent<Text>().text = "Você não conseguiu avançar em suas pesquisas...";
      } else {
        if(chapter != 0) {
          endgamePanel.transform.FindChild("Message").GetComponent<Text>().text = "Você conseguiu avaliar a veracidade de " + victories + " argumento" +
          (victories > 1 ? "s" : "") + " com suas pesquisas!";
        } else
          endgamePanel.transform.FindChild("Message").GetComponent<Text>().text = "Você conseguiu estudar o regimento da Câmara dos Deputados com sucesso!";
        AudioController.GetInstance().PlayMusic("vencedor");
      }

      MinigameData.lastMinigameVictories = victories;
      MinigameData.lastMinigameScore = score;
    }
  }

  public void LeaveGame() {
    if(moves == 0){      
      MinigameStatistics.UpdateProgressAchievements();
      MinigameData.OnMinigameEndClick();
    }
  }

  void UpdateFocusText() {
    if(brunoPowerUpActive) {
      focusText.text = "x" + focusLevel + " B!";
    } else {
      focusText.text = "x" + focusLevel;
    }
  }

  private List<GridItem> BoardTokensToList() {
    List<GridItem> tokensList = new List<GridItem>();

    for(int x = 0; x < sizeX; x++) {
      for(int y = 0; y < sizeY; y++) {
        tokensList.Add(tokens[x, y]);
      }
    }

    return tokensList;
  }

  public GridItem FindRandomTokenOfType(TokenType tokenType, MinigameQueryType queryType, bool canBeInMatch, bool taintToken) {
    //FIXME put this on a MinigameQuery class or something - can be even more flexible that way

    GridItem returnedToken = null;

    List<GridItem> tokenslist = BoardTokensToList();
    Debug.Log("Finding random token, tokensList count is: " + tokenslist.Count);

    if(queryType == MinigameQueryType.Equal) {
      // You want a tokenType token, so remove everything that is not tokenType
      tokenslist.RemoveAll(token => token.tokenType != tokenType);
    } else if(queryType == MinigameQueryType.NotEqual) {
      // You want a non-tokenType token, so remove everything that is a tokenType
      tokenslist.RemoveAll(token => token.tokenType == tokenType);
    }
    if(canBeInMatch == false) {
      tokenslist.RemoveAll(token => token.isInMatch == true);
    }

    tokenslist.RemoveAll(token => token.hasQueryTaint == true);

    Debug.Log("Potential candidates now: " + tokenslist.Count);
    if(tokenslist.Count == 0) {
      return null;
    } else {
      if(tokenslist.Count == 0)
        return null;
      
      returnedToken = tokenslist[Random.Range(0, tokenslist.Count)];
      returnedToken.hasQueryTaint = true;
      return returnedToken;
    }

  }

  void GunEffect() {
    List<GridItem> tokensToDestroy = new List<GridItem>();
    GridItem distractionToken = null;
    GridItem nonDistractionToken = null;  

    while(true) {
      nonDistractionToken = tokens[Random.Range(0, tokens.GetLength(0)), Random.Range(0, tokens.GetLength(1))];
      if(!nonDistractionToken.hasMatched && nonDistractionToken.tokenType != TokenType.Distraction)
        break;
    }

    for(int i = 0; i < sizeX; i++) {
      for(int j = 0; j < sizeY; j++) {
        if(tokens[i, j].tokenType == TokenType.Distraction) {
          distractionToken = tokens[i, j];
        }
      }
    }

    if(distractionToken != null)
      tokensToDestroy.Add(distractionToken);
    if(nonDistractionToken != null)
      tokensToDestroy.Add(nonDistractionToken);

    foreach(var gridItem in tokensToDestroy) {
      Debug.Log("Gun effect");
      gridItem.transform.DOMoveY(1f, MOVSPEED * 0.6f).SetRelative().SetEase(Ease.OutCubic).OnComplete(() => {
        //gi.KillItself();

      });
      gridItem.GetComponent<SpriteRenderer>().DOFade(0f, MOVSPEED * 0.6f);
      gridItem.hasMatched = true;
    }

  }

  public void RandomlyRemoveTokens(int quantity) {
    for(int i = 0; i < quantity; i++) {
      GridItem gi = GetRandomTokenInBoard();
      Debug.Log("Once, gi was a " + gi.tokenType);
      gi.transform.DOMoveY(1f, MOVSPEED * 0.6f).SetRelative().SetEase(Ease.OutCubic).OnComplete(() => {
        //gi.KillItself();

      });
      gi.GetComponent<SpriteRenderer>().DOFade(0f, MOVSPEED * 0.6f);
      gi.hasMatched = true;
    }

  }

  private void BeginMovement() {
    ExecuteNewMatches(true);
    totalTurnsPlayed++;
  }

  private void EndMovement(){    
    if (doingComboLevel >= 3){
      AchievementsManager.UnlockAchievement("1 é Pouco, 2 é Bom..."); //ACHIEVEMENTNAME
    }

    float nightBackgroundTicksPerMove = (float)1f/this.initialMoves;
    UIManager.instance.SetNightBackgroundPercent(nightBackgroundTicksPerMove * totalTurnsPlayed);

    if(emptyFocusOnEndOfMovement) {
      SetFocus(1);
      emptyFocusOnEndOfMovement = false;
    }
    currentlyDoingCombo = false;
    doingComboLevel = 0;
    if(moves == 0) {
      EndGame();
    } else {
      if(gameStarted) {
        SetCanAct(true);
      }
    }
  }

  public GridItem GetRandomTokenInBoard() {
    GridItem gi = null;
    while(true) {
      gi = tokens[Random.Range(0, tokens.GetLength(0)), Random.Range(0, tokens.GetLength(1))];
      if(!gi.hasMatched)
        break;
    }
    return gi;
  }

  public void TriggerScoreConsumableList(ScoreConsumableList list) {
    if(list.consumableListLength > 0) {
      MinigameAudioController.PlayMatchSound(doingComboLevel);
      list.TriggerEffect();
      Invoke("ShiftRowsDown", MOVSPEED * 0.33f);
    }
  }

  public void TriggerMatches(List<MinigameMatch> matches, bool playerIssued) {
    Debug.Log("*Triggering " + matches.Count + " matches*");
    if (matches.Count >= 3){
      AchievementsManager.UnlockAchievement("3 é demais!"); //ACHIEVEMENTNAME
    }
    bool hasMoveTokens = false;
    bool hasDistractionTokens = false;
    bool hasFocusTokens = false;

    matches.Sort(); // sort implementation is in the MinigameMatch class (IComparable interface)

    foreach(MinigameMatch match in matches){
      if(match.tokenType == TokenType.MoveToken) {        
        hasMoveTokens = true;
      }
      if(match.tokenType == TokenType.Distraction) {        
        hasDistractionTokens = true;
      }
      if(match.tokenType == TokenType.Focus) {        
        hasFocusTokens = true;
      }
    }

    foreach(MinigameMatch match in matches) {      
      match.TriggerEffect();
    }

    if(playerIssued == true) {      
      if (hasFocusTokens == false){
        Debug.Log("TriggerMatches - Player issued and no focus tokens, will empty focus end of turn");
        emptyFocusOnEndOfMovement = true;
      }
    }

    if(hasDistractionTokens) {
      MinigameAudioController.PlayBadMatchSound();
    } else {
      MinigameAudioController.PlayMatchSound(doingComboLevel);
    }

    if(hasMoveTokens == false && playerIssued && melissaUltimateActive == false) {
      AddMoves(-1);
    }

    currentlyDoingCombo = true;
    Invoke("ShiftRowsDown", MOVSPEED * 0.33f);
  }

  bool CheckMatch(GridItem[,] matrix, TokenType type, int checkX, int checkY, bool actualBoard = false) {
    // Check horizontals first!
    if(brunoUltimateActive)
      return true; // A fake "true"! :O (Bruno's ultimate ignores checking)
    int horizontalCount = 0, verticalCount = 0;
    int offset = 0;
    while(true) {
      offset--;
      if(checkX + offset < 0 || checkX + offset >= sizeX)
        break;
      GridItem gi = matrix[checkX + offset, checkY];
      if(gi != null && gi.tokenType == type) {
        horizontalCount++;
      } else {
        break;
      }
    }
    offset = 0;
    while(true) {
      offset++;
      if(checkX + offset < 0 || checkX + offset >= sizeX)
        break;
      GridItem gi = matrix[checkX + offset, checkY];
      if(gi != null && gi.tokenType == type) {
        horizontalCount++;
      } else {
        break;
      }
    }

    offset = 0;
    while(true) {
      offset--;
      if(checkY + offset < 0 || checkY + offset >= sizeY)
        break;
      GridItem gi = matrix[checkX, checkY + offset];
      if(gi != null && gi.tokenType == type) {
        verticalCount++;
      } else {
        break;
      }
    }
    offset = 0;
    while(true) {
      offset++;
      if(checkY + offset < 0 || checkY + offset >= sizeY)
        break;
      GridItem gi = matrix[checkX, checkY + offset];
      if(gi != null && gi.tokenType == type) {
        verticalCount++;
      } else {
        break;
      }
    }


    if(actualBoard) {
      if(horizontalCount >= 2) {
        tokens[checkX, checkY].matchingHorizontally = true;
      }
      if(verticalCount >= 2) {
        tokens[checkX, checkY].matchingVertically = true;
      }
    }

    if(horizontalCount >= 2 || verticalCount >= 2) {			
      
      // Check the sequence!
      if(actualBoard) {
        GridItem checkToken = tokens[checkX, checkY];
        checkToken.verticalSequence.Clear();
        checkToken.horizontalSequence.Clear();
        offset = 1;
        while(offset + checkX < sizeX && tokens[checkX + offset, checkY].tokenType == type && horizontalCount >= 2) {
          checkToken.horizontalSequence.Add(tokens[checkX + offset, checkY]);
          offset++;
        }
        offset = 1;
        while(checkX - offset >= 0 && tokens[checkX - offset, checkY].tokenType == type && horizontalCount >= 2) {
          checkToken.horizontalSequence.Add(tokens[checkX - offset, checkY]);
          offset++;
        }
        offset = 1;
        while(checkY + offset < sizeY && tokens[checkX, checkY + offset].tokenType == type && verticalCount >= 2) {
          checkToken.verticalSequence.Add(tokens[checkX, checkY + offset]);
          offset++;
        }
        offset = 1;
        while(checkY - offset >= 0 && tokens[checkX, checkY - offset].tokenType == type && verticalCount >= 2) {
          checkToken.verticalSequence.Add(tokens[checkX, checkY - offset]);
          offset++;
        }
        if(verticalCount >= 2)
          checkToken.verticalSequence.Add(checkToken);
        if(horizontalCount >= 2)
          checkToken.horizontalSequence.Add(checkToken);
      }
      return true;
    } else {
      if(actualBoard) {
        tokens[checkX, checkY].verticalSequence.Clear();
        tokens[checkX, checkY].horizontalSequence.Clear();
      }
      return false;
    }
  }

  void FillGrid() {
    SetCanAct(false);
    board.transform.localScale = new Vector3(0f, 0f, 0f);
    bottomPanel.transform.position = new Vector3(bottomPanel.transform.position.x, bottomPanel.transform.position.y - 20f);
    bottomPanel.transform.DOMoveY(20f, 0.75f).SetEase(Ease.OutExpo).SetRelative();
    rightPanel.transform.position = new Vector3(rightPanel.transform.position.x + 20, rightPanel.transform.position.y);
    rightPanel.transform.DOMoveX(-20f, 0.75f).SetRelative().SetEase(Ease.OutExpo).OnComplete(() => {
      board.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
      board.GetComponent<CanvasGroup>().alpha = 0;
      board.GetComponent<CanvasGroup>().DOFade(1f, 0.6f);
      board.transform.DOScale(1f, 0.6f).OnComplete(() => {
        tokens = new GridItem[sizeX, sizeY];
				
        for(int x = 0; x < sizeX; x++) {
          for(int y = 0; y < sizeY; y++) {
            // Fill item at position (x,y).
            //MinigameMatchChecker matchChecker = new MinigameMatchChecker(tokens);
            tokens[x, y] = InstantiateToken(x, y);
            //tokens[x,y].gameObject.transform.parent = tokenHolder.transform;
          }
        }
      });
    });



  }

  GridItem InstantiateToken(int x, int y, bool preventMatches = true, TokenType tokenType = TokenType.None) {
    // A "tokenType" of "TokenType.None" means you're not specifying a token type to generate.

    List<GameObject> tempPossibleTokens = new List<GameObject>();
    for(int i = 0; i < possibleTokens.Length; i++) {
      tempPossibleTokens.Add(possibleTokens[i]);
    }
    switch(chapter) {
      case 0:
        break;
      case 1:
        tempPossibleTokens.Add(transToken);
        break;
      case 2:
        tempPossibleTokens.Add(gunToken);
        break;
      case 3:
        tempPossibleTokens.Add(healthToken);
        break;
      case 4:
        tempPossibleTokens.Add(corruptedToken);
        break;
    }
    if(Random.value <= (1 - this.variables.chanceEffectToken) && tokenType == TokenType.None) {
      if(chapter == 0)
        tempPossibleTokens.RemoveRange(4, 3);
      else
        tempPossibleTokens.RemoveRange(4, 4);
    }
    if(tokenType != TokenType.None) {
      GameObject desiredToken = null;
      foreach(GameObject go in tempPossibleTokens) {
        if(go.GetComponent<GridItem>().tokenType == tokenType) {
          desiredToken = go;
          break;
        }
      }
      tempPossibleTokens = new List<GameObject>();
      tempPossibleTokens.Add(desiredToken);
    }

    bool matches = true;
    while(matches) {
      matches = false;
      int randomId = Random.Range(0, tempPossibleTokens.Count);
      GameObject randomToken = tempPossibleTokens[randomId];

      TokenType type;
      if(randomToken == null) {
        //Debug.log("random Token is NULL");
      }
      type = randomToken.GetComponent<GridItem>().tokenType;


      if(preventMatches && tokenType == TokenType.None) {
        matches = CheckMatch(tokens, type, x, y);
      } else {
        matches = false;
      }
      if(brunoUltimateActive)
        matches = true;

      if(matches) {
        tempPossibleTokens.RemoveAt(randomId);
      } else {
        GameObject generatedToken;
        if(tokenType == TokenType.None)
          generatedToken = Instantiate(randomToken, new Vector3(x, y + 20, 0f), Quaternion.identity) as GameObject;
        else
          generatedToken = Instantiate(randomToken, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;
        SpriteRenderer renderer = generatedToken.gameObject.GetComponent<SpriteRenderer>();

        if(tokenType != TokenType.None) {
          generatedToken.transform.localScale = new Vector3(1f, 1f, 1f);
          renderer.color = new Color(renderer.color.r, renderer.color.g, renderer.color.b, 1f);
          renderer.material.SetFloat("_FlashAmount", 1f);
          generatedToken.gameObject.GetComponent<SpriteRenderer>().material.DOFloat(0f, "_FlashAmount", MOVSPEED*0.3f).SetDelay(MOVSPEED*0.3f);
        } else {
          generatedToken.transform.localScale = new Vector3(1f, 1f, 1f);

          if(!gameStarted) {
            generatedToken.transform.DOMoveY(-20f, Mathf.Abs(Random.value + 0.4f)).SetRelative().SetEase(Ease.OutExpo);
          } else {
            if(x % 2 == 1)
              generatedToken.transform.DOMoveY(-20f, MOVSPEED * 0.15f).SetRelative().SetEase(Ease.OutExpo);
            else
              generatedToken.transform.DOMoveY(-20f, MOVSPEED * 0.24f).SetRelative().SetEase(Ease.OutExpo);
          }

        }
        GridItem generatedGridItem = generatedToken.GetComponent<GridItem>();
        generatedGridItem.gridManager = this;
        generatedGridItem.SetPosition(x, y);
        tokens[generatedGridItem.posX, generatedGridItem.posY] = generatedGridItem;
        //if (tokenType != TokenType.None) CheckAllMatches();
        totalSpawnedTokens++;
        if(!gameStarted && totalSpawnedTokens >= sizeX * sizeY) {
          gameStarted = true;
          Invoke("EnableAct", 1f);
        }
        return generatedGridItem;
      }
    }
    return null;		
  }

  void EnableAct() {
    if(chapter != 0 && !isTutorialRunning)
      SetCanAct(true);
  }

  void PrintGrid() {
    // Debugging reasons!

    for(int x = 0; x < sizeX; x++) {
      for(int y = 0; y < sizeY; y++) {
								
        //Debug.log("Token [" + x + "][" + y + "]: " + tokens[x,y].tokenType);
				
      }
    }
  }



  void ResetPreviews() {
    for(int x = 0; x < sizeX; x++) {
      for(int y = 0; y < sizeY; y++) {

				
        GridItem shifteer = tokens[x, y];
        if(shifteer.previewing) {
          shifteer.transform.DOMove(new Vector3(shifteer.posX, shifteer.posY, shifteer.transform.position.z), MOVSPEED * 0.3f);
          shifteer.previewing = false;
        }
				
      }
    }
  }

  void OnMouseDragOver(GridItem item) {		
    if(moves <= 0) {
      SetCanAct(false);
      item.gameObject.transform.position = new Vector3(item.posX, item.posY, item.gameObject.transform.position.z);
    }
    if(canAct) {
      int offset;
      GameObject go = item.gameObject;
			
      int dropX, dropY;
      dropX = Mathf.RoundToInt(go.transform.position.x);
      dropY = Mathf.RoundToInt(go.transform.position.y);
			
      if(dropY == item.posY && dropX != item.posX) {
        // Horizontal preview shift
        for(int i = 0; i < sizeX; i++) {
          GridItem shifteer = tokens[i, dropY];
			
          if(shifteer != item) {
            int a, b, c = 0;
            a = item.posX;
            b = dropX;
			
			
			
            if(a > b) {
              // Swap, so a < b always
              c = a;
              a = b;
              b = c;
              offset = 1;
            } else {
              offset = -1;
            }
			
			
            if(shifteer.posX <= b && shifteer.posX >= a && !shifteer.previewing) {
              shifteer.previewing = true;
              shifteer.previewX = shifteer.posX + offset;
              shifteer.previewY = shifteer.posY;
              shifteer.transform.DOMove(new Vector3(shifteer.posX + offset, shifteer.transform.position.y, shifteer.transform.position.z), MOVSPEED *0.3f);
            } else if(shifteer.previewing && shifteer.posX > b || shifteer.posX < a) {					
              shifteer.transform.DOMove(new Vector3(shifteer.posX, shifteer.posY, shifteer.transform.position.z), MOVSPEED *0.3f);
              shifteer.previewing = false;
              //ResetPreviews();
            }
          }
        }
      } else if(dropX == item.posX && dropY != item.posY) {
        // Vertical preview shift
        for(int i = 0; i < sizeY; i++) {
          GridItem shifteer = tokens[dropX, i];
					
          if(shifteer != item) {
            int a, b, c = 0;
            a = item.posY;
            b = dropY;
			
						
						
            if(a > b) {
              // Swap, so a < b always
              c = a;
              a = b;
              b = c;
              offset = 1;
            } else {
              offset = -1;
            }
						
						
            if(shifteer.posY <= b && shifteer.posY >= a && !shifteer.previewing) {
              shifteer.previewX = shifteer.posX;
              shifteer.previewY = shifteer.posY + offset;
              shifteer.previewing = true;
              shifteer.transform.DOMove(new Vector3(shifteer.transform.position.x, shifteer.posY + offset, shifteer.transform.position.z), MOVSPEED*0.3f);
            } else if(shifteer.previewing && shifteer.posY > b || shifteer.posY < a) {
              shifteer.transform.DOMove(new Vector3(shifteer.posX, shifteer.posY, shifteer.transform.position.z), MOVSPEED*0.3f);
              shifteer.previewing = false;
              //ResetPreviews();
            }
          }
        }
      } else {
        // Impossible diagonal shift! Return everything to its original (x,y) position.
        ResetPreviews();
			
      }
			
      if(dropX < 0 || dropX >= sizeX || dropY < 0 || dropY >= sizeY) {
        // Impossible out of bounds shift! Return everything to its original (x,y) position.
        ResetPreviews();
      }
    }

  }

  void OnMouseDropItem(GridItem item) {		
    int newX, newY;

    GameObject go = item.gameObject;
    go.GetComponent<SpriteRenderer>().sortingOrder = 1;
    if(canAct) {
      SetCanAct(false);
      int dropX, dropY;
      dropX = Mathf.RoundToInt(go.transform.position.x);
      dropY = Mathf.RoundToInt(go.transform.position.y);

      if(dropX != item.posX && dropY != item.posY) {
        // Invalid position (moving diagonally somewhere), so we reset the position
        go.transform.DOMove(new Vector3(item.posX, item.posY, go.transform.position.z), MOVSPEED*0.3f).OnComplete(() => {
          SetCanAct(true);
        });
        //go.transform.position = new Vector3(item.posX, item.posY, go.transform.position.z);
      } else {
        if((dropX >= 0 && dropX < sizeX) && (dropY >= 0 && dropY < sizeY)) {

          // Moving inside the grid, so it's good!


          // First, we make a copy of the "tokens" matrix, to check the "what if" scenario of the new grid.
          // This will be useful to invalidate the move if it doesn't match any tokens.

          GridItem[,] copyTokens = new GridItem[sizeX, sizeY];

          for(int x = 0; x < sizeX; x++) {
            for(int y = 0; y < sizeY; y++) {							
              copyTokens[x, y] = tokens[x, y];
            }
          }

          // All items that need to "update" their locations. First in the copyTokens, then in the tokens matrix
          // (if it's a legit move)
          List<GridItem> toUpdate = new List<GridItem>();
          int count = 0;
          for(int x = 0; x < sizeX; x++) {
            for(int y = 0; y < sizeY; y++) {	
              if(tokens[x, y].previewing || tokens[x, y] == item) {
                toUpdate.Add(tokens[x, y]);
              }
            }
          }
          foreach(GridItem gi in toUpdate) {
            int tempX, tempY;
            if(gi.previewing || gi == item) {
              if(gi == item) {
                tempX = Mathf.RoundToInt(gi.transform.position.x);
                tempY = Mathf.RoundToInt(gi.transform.position.y);
              } else {
                tempX = gi.previewX;
                tempY = gi.previewY;
              }

              copyTokens[tempX, tempY] = gi;
            }
          }

          bool checkValid = false;

          for(int x = 0; x < sizeX; x++) {
            for(int y = 0; y < sizeY; y++) {
              if(CheckMatch(copyTokens, copyTokens[x, y].tokenType, x, y)) {
                checkValid = true;
                break;
              }
            }
          }


          if(checkValid) {            
            foreach(GridItem gi in toUpdate) {
								
              GridItem shifteer = gi;
						
              if(shifteer.previewing || shifteer == item) {
                count++;

                if(shifteer == item) {
                  newX = Mathf.RoundToInt(shifteer.transform.position.x);
                  newY = Mathf.RoundToInt(shifteer.transform.position.y);
                } else {
                  newX = shifteer.previewX;
                  newY = shifteer.previewY;
                }
						
                if(shifteer.previewing) {
                  shifteer.previewing = false;
                  shifteer.previewX = 0;
                  shifteer.previewY = 0;
                }
                shifteer.posX = newX;
                shifteer.posY = newY;
						
                tokens[newX, newY] = shifteer;
						
						
              }	
            }

            item.transform.position = new Vector3(dropX, dropY, item.transform.position.z);

            bool check = CheckMatch(tokens, item.tokenType, item.posX, item.posY);
            if(check) {              
              BeginMovement();
              if(moves == 3 && !warnedAboutMovesLeft) {
                warnedAboutMovesLeft = true;
                DisplayWarningText("Últimas " + moves + " jogadas!");
              }
            } else {
              Debug.Log("Not move, invalid");
              if(!melissaUltimateActive)
                AddMoves(-1);
              ExecuteNewMatches();
            }

//						AudioController.GetInstance().PlaySfx("puzzle/peca_soltar");
            ResetPreviews();
          } else {
            go.transform.DOMove(new Vector3(item.posX, item.posY, go.transform.position.z), MOVSPEED*0.3f).OnComplete(() => {
              SetCanAct(true);
            });
            MinigameAudioController.PlayCancelSound();
            ResetPreviews();
          }

        } else {
          // Tried to move to x,y coordinates outside of the grid, so we reset the position.
          // Could also mean an invalid move.
          MinigameAudioController.PlayCancelSound();

          go.transform.DOMove(new Vector3(item.posX, item.posY, go.transform.position.z), MOVSPEED*0.3f).OnComplete(() => {
            SetCanAct(true);
          });
          ResetPreviews();

        }
      }



    } else {

      go.transform.position = new Vector3(item.posX, item.posY, go.transform.position.z);
    }

  }
	

}
