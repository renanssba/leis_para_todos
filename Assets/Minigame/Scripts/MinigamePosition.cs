﻿using System;

public class MinigamePosition {
  public int x;
  public int y;

  public MinigamePosition(int x, int y){
    this.x = x;
    this.y = y;
  }

  public MinigamePosition(GridItem token){
    this.x = token.posX;
    this.y = token.posY;
  }
}

