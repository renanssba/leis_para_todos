﻿using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class MinigameData : MonoBehaviour {

  public static MinigameData instance;

  public static int lastMinigameScore;

  public enum SkillIds{
    NoSkill = -1,
    AlbertoLv1 = 0,
    BrunoLv1 = 1,
    MelissaLv1 = 2,

    AlbertoLv2 = 4,
    BrunoLv2 = 5,
    MelissaLv2 = 6,

    AlbertoLv3 = 8,
    BrunoLv3 = 9,
    MelissaLv3 = 10,
  }

	/* Currently played chapter. Changes the 8th token.
	 * 1 = Transgenic Token
	 * 2 = -
	 * 3 = -
	 * 4 = Corrupted Token
	 * 
	 * Might also affect the required score for each victory level (0 to 3).
	 */
	[HideInInspector] public static int chapter = 1;


	/* Number of victories (0 to 3) in the last minigame played.
	 * Updates after each minigame.
	 */ 
	[HideInInspector] public static int lastMinigameVictories = 0;

	[HideInInspector] public static int maxSuperBar = 3;

	// Use this for initialization
	void Awake () {
    if(instance == null) {
      instance = this;
      DontDestroyOnLoad(this.gameObject);
    } else if(instance!= this){
      Destroy(gameObject);
    }

	}

	public static void OnMinigameEndClick(){

    // Update the leaderboard
    LeaderboardsManager.TryUpdateScore(GridManager.instance.chapter, lastMinigameScore);

    if (Persistence.GetInstance().GetVariableValue("isFreeplayMode") == 0){
      // Story mode, normal flow
      Analytics.CustomEvent("minigameVictory", new Dictionary<string, object>{{"victories", lastMinigameVictories}});
      Persistence.matchVictories = lastMinigameVictories;
      SceneManager.LoadScene("Chapter");
    } else{
      // Freeplay mode, altered flow
      // TODO: Send highscore to leaderboard

      SceneManager.LoadScene("Freeplay");
    }
	}
}
