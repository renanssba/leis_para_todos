public enum TokenType{
	None, Internet, Conversation, Statistics, Reading, MoveToken, Distraction, Focus, GeneticFood, GunToken, HealthToken, CorruptionToken,
	NumberOfTokenTypes
}