﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FreeplayManager : MonoBehaviour {

  public Button equipSkillsButton;
  public Button returnToMainMenuButton;
  public Button showAchievementsButton;
  public Button showLeaderboardsButton;
  public GameObject skillsScreenPanel;
  public GameObject chap1Panel;
  public Button chap1Button;
  public GameObject chap2Panel;
  public Button chap2Button;
  public GameObject chap3Panel;
  public Button chap3Button;
  public GameObject chap4Panel;
  public Button chap4Button;

  void Awake(){
    Persistence.GetInstance().SetVariableValue("isFreeplayMode", 1);

    AudioController.GetInstance().PlayMusic("irmao");

    equipSkillsButton.onClick.AddListener(this.OnEquipSkillButtonClick);
    chap1Button.onClick.AddListener(()=>{OnChapterButtonClick(1);});
    chap2Button.onClick.AddListener(()=>{OnChapterButtonClick(2);});
    chap3Button.onClick.AddListener(()=>{OnChapterButtonClick(3);});
    chap4Button.onClick.AddListener(()=>{OnChapterButtonClick(4);});

    showAchievementsButton.onClick.AddListener(this.ShowAchievements);
    showLeaderboardsButton.onClick.AddListener(this.ShowLeaderboards);

    returnToMainMenuButton.onClick.AddListener(()=>{
      AudioController.GetInstance().PlayClickSound();
      Persistence.GetInstance().SetVariableValue("isFreeplayMode", 0);
      PlayerPrefs.Save();
      SceneManager.LoadScene("TitleScreen");
    });

    EnableChaptersByProgress();
      
  } 

  void EnableChaptersByProgress() {
    if (PlayerPrefs.GetInt("permanentUnlockedSkillsLevel2") > 0){
      chap2Panel.SetActive(true);
    }

    if (PlayerPrefs.GetInt("permanentUnlockedSkillsLevel3") > 0){
      chap3Panel.SetActive(true);
    }

    if (PlayerPrefs.GetInt("permanentUnlockedSkillsLevel3") >= 3){
      chap4Panel.SetActive(true);
    }
  }

  void OnEquipSkillButtonClick() {
    AudioController.GetInstance().PlayPageSound();
    Debug.Log("Opening equip skill panel");
    skillsScreenPanel.SetActive(true);
    skillsScreenPanel.GetComponent<SkillsScreen>().Initialize();
//    skillsScreenPanel.GetComponent<MenuController>().OpenSkillsScreen();
  }


  public void OnCloseSkillsScreenButtonClick(){
    AudioController.GetInstance().PlayPageSound();
    skillsScreenPanel.SetActive(false);
  }



  void OnChapterButtonClick(int chapter){
    Debug.Log("Going to chapter " + chapter);
    MinigameData.chapter = chapter;
    Persistence.GetInstance().SetVariableValue("hasSkills", 1);

    SceneManager.LoadScene("Minigame");
  }


  void Update(){
    if (Application.isEditor && Input.GetKeyDown(KeyCode.P)){
      Debug.Log("Deleting all your PlayerPrefs for debugging reasons...");
      PlayerPrefs.DeleteAll();
    }

    if (Application.isEditor && Input.GetKeyDown(KeyCode.A)){
      ShowAchievements();
    }

    if (Application.isEditor && Input.GetKeyDown(KeyCode.O)){
      PlayerPrefs.SetInt("permanentUnlockedSkillsLevel1", 3);
      PlayerPrefs.SetInt("permanentUnlockedSkillsLevel2", 3);
      PlayerPrefs.SetInt("permanentUnlockedSkillsLevel3", 3);

      EnableChaptersByProgress();
    }
  }

  public void ShowAchievements(){
    AudioController.GetInstance().PlayClickSound();

    Social.ShowAchievementsUI();
  }



  public void ShowLeaderboards(){
    AudioController.GetInstance().PlayClickSound();

    Social.ShowLeaderboardUI();
  }
    
}
