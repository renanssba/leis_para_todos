﻿using UnityEngine;
using UnityEditor;

class PreProcessTexturesToUI : AssetPostprocessor {

  void OnPreprocessTexture () {
		TextureImporter textureImporter = (TextureImporter)assetImporter;

    if(textureImporter.assetPath.Contains("Resources")){
      textureImporter.textureType = TextureImporterType.Sprite;
    }
	}
}