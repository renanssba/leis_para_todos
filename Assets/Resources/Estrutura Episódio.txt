﻿// [0] - INTRODUÇÃO DO CAPÍTULO/JOGO

*Introdução sobre quem é Alberto Colina*
*Introdução sobre a história do jogo*
*Introdução sobre o Processo Legislativo*


// [1] - INTRODUÇÃO DO PL

*Durante a sessão plenária, o deputado Fernando Netuno apresenta seu PLC, 3/16*
*Alberto recebe uma cópia do documento*

*É explicado para o jogador o que o Projeto de Lei prevê *



// [2] - FLASHBACK RELACIONADO À PL

*no barzinho, Alberto encontra Bruno e Melissa*

*os três conversam sobre o Projeto de Lei e todos dão sua opinião sobre o tema*



// [3] - OPINIÃO DOS CONSELHEIROS

*Alberto decide precisa realizar pesquisas acerca do tema*

*o jogo oferece opções de conselheiros para Alberto consultar, sendo eles: Bruno, Melissa, Sofia*

*ao consultar Bruno, ele fala sobre os benefícios econômicos dos transgênicos e da possível segurança de seu consumo*

*ao consultar Melissa, ela fala sobre os direitos do consumidor envolvidos no PL*

*ao consultar Sofia Carcará, ela fala sobre a suspeita de inconstitucionalidade do PL*



// [4] - PESQUISA DE DADOS COM ESPECIALISTAS

*Alberto consegue dados para embasar sua decisão sobre a PL em debate*



// [5] - DISCURSO DE ALBERTO

*Alberto apresenta seus argumentos a favor ou contra o PL.*

*Rivaldo apresenta questionamentos a Alberto, que deve defender seus argumentos com dados coletados*



// [6] - VOTAÇÃO DO PL

*O jogo considera todos os fatores envolvidos no resultado da votação, como: relacionamento de Alberto com os demais partidos políticos, a validade do discurso e o debate com o Rivaldo*

*apresenta-se o resultado da votação*

*se Alberto vencer a votação, o jogo prossegue*

*se Albero perder a votação, o segmento de discurso reinicia*



// [7] - DESFECHO CAPÍTULO

*timelapse*

*são mostrados os efeitos da aprovação/reprovação da lei no país*

*são mostrados os efeitos da votação na carreira de Alberto*

*apresentação de gancho para o próximo capítulo*
