﻿using UnityEngine;
using System.Collections;

public class Animate {

	public Transform character { get; private set;}
	public float speed { get; private set;}
	public Vector2 origin { get; private set;}
	public Vector2 destination { get; private set;}

	public Animate(Transform character, float speed, Vector2 origin, Vector2 destination){
		this.character = character;
		this.speed = speed;
		this.origin = origin;
		this.destination = destination;
	}
}
