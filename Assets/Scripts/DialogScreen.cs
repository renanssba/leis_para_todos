﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class DialogScreen : MonoBehaviour {
	
	public float defaultMusicVolume = 0.3f;
	private ScreenLayout layout = ScreenLayout.Dialog;
	public GameObject emoticonPrefab;
	public GameObject choices;
	public GameObject[] calendar;
  public GameObject character_name_box;
  public GameObject arrow;

  public DialogBox dialogBox;
  public QuestionBox questionBox;
  public CharacterList charList;
  public Image bg;
  public Image fg;
  public CanvasGroup hypeBarCanvasGroup;
  public Slider hypeBarSlider;
	public GameController gameController;

  public const int maxCharsInScreen = 6;
	
	public enum ScreenLayout{
		Dialog,
		Question
	}

	public void ChangeScreenLayout(ScreenLayout new_layout){
		layout = new_layout;

		switch(layout) {
		case ScreenLayout.Dialog:
			dialogBox.gameObject.SetActive(true);
			questionBox.gameObject.SetActive(false);
			choices.SetActive(false);
			break;
		case ScreenLayout.Question:
			dialogBox.gameObject.SetActive(true);
      questionBox.gameObject.SetActive(false);
//			character_name_box.SetActive(false);
      arrow.SetActive(false);
			choices.SetActive(true);
			break;
		}
	}
	
	public void SetDialog(string dialog){
		ChangeScreenLayout(ScreenLayout.Dialog);
		dialogBox.Say(dialog);
	}
	
	public void SetDialog(string characterName, string dialog){
    SetCharacterName(characterName);
		SetDialog(dialog);
	}

  public void EndDialogEffect(){
    if( layout == ScreenLayout.Question ){
      if( !dialogBox.talking && questionBox.gameObject.activeSelf == false ){
        questionBox.UpdateChoicesText();
        questionBox.gameObject.SetActive(true);
      }
    }
  }

  public void SetArrowPosition(string characterName){
    float pos_x = charList.FindPositionByName(characterName);
    
    if(pos_x == -99999f){
      pos_x = -10f;
    }
    arrow.transform.position = new Vector3(pos_x,
                                           arrow.transform.position.y,
                                           arrow.transform.position.z);
    if(pos_x<0){
      arrow.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
    }else{
      arrow.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
    }
  }

	public void Screenshake(int amount = 3){
		//Camera.main.DOShakePosition(2, amount*10, 80, 90f);
		//Camera.main.transform.DOMoveY(300f, 2f).SetRelative();
		GameObject.Find("GameCanvas").transform.Find("UI Elements").DOShakePosition(0.5f, amount*10, 80, 90f);
	}

	public string GetTalkingCharName(){
		return dialogBox.GetCharacterName();
	}
	
	public void SetSfxInterval(int interval){
		dialogBox.sfxInterval = interval;
	}
	
	public void SetCollect(int id, int backWp, int[] collectibleWp){
		// deprecated
	}



  public void SetCharacter(int id, string charName, float position){
    Character character = charList.characters[id];

    if(character.GetBaseSprite() != null) {
      Resources.UnloadAsset(character.GetBaseSprite());
    }
    character.SetBaseSprite(Resources.Load<Sprite>("Characters/" + charName));
    character.SetMood(charName);
    character.SetName(charName);
    character.SetAlpha(1f);

    if(charName != "none") {
      character.SetPositionX(position);
      character.SetFacing(1f);
    }
  }
	
  public void SetCharacterMood(int charId, string moodName){
		Character character = charList.characters[charId];
    character.SetMood(moodName);
  }

  public void SetCharacterBaseSprite(string[] character_sprite){
    for(int i=0; i<maxCharsInScreen; i++){
      if(character_sprite[i] != "none"){
        Character character = charList.characters[i];
        character.SetBaseSprite( Resources.Load<Sprite>("Characters/" + character_sprite[i]) );
      }
    }
  }
	
	void ClearCharacters(){
    foreach(Character character in charList.characters){
      Image charImg = character.GetComponent<Image>();
      charImg.sprite = null;
		}
	}
	
	public void MirrorCharacter(int charIndex){
    charList.characters[charIndex].Mirror();
  }
  
  public void SetMovement_x(int charIndex, float anim_time, float destination){
		ChangeScreenLayout(ScreenLayout.Dialog);
		charList.Move_x(charIndex, anim_time, destination);
	}
	
	public void SetMovement_y(int charIndex, float anim_time, float destination){
		ChangeScreenLayout(ScreenLayout.Dialog);
		charList.Move_y(charIndex, anim_time, destination);
	}
	
	public void StopAnimation(){
		charList.StopAnimation();
	}

	public void AnimateCharacterAlpha(int charIndex, float animTime, float endAlpha){
		charList.AnimateAlpha(charIndex, animTime, endAlpha);
	}

	public void AnimateCharacterScale(int charIndex, float anim_time, float scale_y){
		charList.AnimateScale(charIndex, anim_time, scale_y);
	}
	
	public void SetQuestion(string text){
    gameController.SetQuestion(text);
		dialogBox.Say(text);
	}

	public void SetQuestion(string characterName, string text){
    SetCharacterName(characterName);
    SetQuestion(text);
	}

  public void SetCharacterName(string name){
    dialogBox.SetCharacterName(name);

    // set position of character name box
    if(name == ""){
      character_name_box.SetActive(false);
      arrow.SetActive(false);
    }else{
      character_name_box.SetActive(true);
      arrow.SetActive(true);

      SetArrowPosition(name);
    }
  }
	
  public void SetChoices(string[] text, int[] wp){
		gameController.SetQuestionState(wp);
    questionBox.SetChoicesText(text);
    ChangeScreenLayout(DialogScreen.ScreenLayout.Question);
//    questionBox.UpdateChoicesText();
	}
	
  public void SetBg(string path){
    if(path.ToLower() !="none"){
      bg.enabled = true;
      bg.sprite = Resources.Load<Sprite>("Bg/" + path);
    }else{
      bg.enabled = false;
    }
  }

  public void SetFg(string path){
    if(path.ToLower() !="none"){
      fg.enabled = true;
      fg.sprite = Resources.Load<Sprite>("Bg/" + path);
    }else{
      fg.enabled = false;
    }
  }
	
	public void EnableDialogBox(bool value){
		dialogBox.EnableBox(value);
	}
	
	public void ShowCalendar(bool value){
		foreach(GameObject obj in calendar){
			obj.SetActive(value);
		}
	}

	public void ChangeCalendar(string first, string second){
		calendar[0].GetComponent<Text>().text = first;
		calendar[1].GetComponent<Text>().text = second;
	}
	
	public void ShowEmoticon(string file, Vector2 position, float lifespan){
		
		float fadeTime =(0.1f) * lifespan;
		Sprite sprite = Resources.Load<Sprite>("Emoticons/" + file);
		GameObject emote =(GameObject)Instantiate(emoticonPrefab);
		Emoticon emoticon = emote.GetComponent<Emoticon>();
		emoticon.Create(lifespan, fadeTime, position, sprite);
	}

	public void UpdateHypeBar(){
    int hypeLevels = Persistence.GetInstance().GetVariableValue("hype_discurso");;

		float percent = (float)(hypeLevels)/100f;
    hypeBarSlider.DOValue(percent, 1f);
	}

	public void ShowHype(){
		hypeBarCanvasGroup.DOFade(1f, 0.5f);
	}

	public void HideHype(){
		hypeBarCanvasGroup.DOFade(0f, 0.5f);
	}
}
