﻿using System;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using UnityEngine.SocialPlatforms.GameCenter;

public class GameCenterConnector : GenericSocialConnector{

  void GenericSocialConnector.Connect() {
    #if (UNITY_IOS)
    Debug.Log("<color=blue>Trying to connect to Game Center...</color>");

    Social.localUser.Authenticate((bool success) => {
      // handle success or failure
      if (success){
        Debug.Log("<color=green>Successfully logged to Game Center Services</color>");
        AchievementsManager.CommitLocalAchievements();
        LeaderboardsManager.CommitPendingHighscores();
      } else{
        Debug.Log("<color=red>Error: couldn't log into Game Center Services</color>");
      }
    });
    #endif
  }

}

