﻿using System;

public class Achievement {

  public enum Status{
    LOCKED=0,
    PENDING=1,
    COMMITED=2
  }

  public string achievementName;
  public string googlePlayId;
  public string gameCenterId;
  public int progress;
  public int target;
  public bool isPlatinum;
  public bool isProgress;
  public Status status;

  /// <summary>
  /// Initializes a new instance of the <see cref="Achievement"/> class.
  /// </summary>
  /// <param name="googlePlayId">Google play identifier.</param>
  /// <param name="achievementName">Achievement name.</param>
  /// <param name="target">Target goal for progression (e.g: "Kill 10 enemies" would have a target of 10). If not progress, default set to 1.</param>
  /// <param name="isPlatinum">If set to <c>true</c>, it's a platinum achievement (unlocks automatically when all other achievements unlock.</param>
  public Achievement(string googlePlayId, int gameCenterId, string achievementName, int target = 1, bool isPlatinum=false) {
    this.achievementName = achievementName;
    this.googlePlayId = googlePlayId;
    this.gameCenterId = gameCenterId.ToString();
    this.progress = 0;
    this.target = target;
    if (this.target > 1){
      this.isProgress = true;
    } else{
      this.isProgress = false;
    }
    this.status = Status.LOCKED;
    this.isPlatinum = isPlatinum;
  }    


}