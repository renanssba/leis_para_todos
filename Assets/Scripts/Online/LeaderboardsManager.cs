﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeaderboardsManager {

  // user's highscores representation here
  // save also whether each highscore is updated in Google Play or not

  public static void TryUpdateScore(int chapter, int newScore){
    
    if (chapter >= 1 && chapter <= 4){
      int scoreBefore = Persistence.GetInstance().GetLeaderboardHighscore(chapter);
      if (newScore > scoreBefore){
        Debug.Log("<color=blue>New highscore for chapter " + chapter + ". Updating from " + scoreBefore + " to " + newScore + ".</color>");
        UpdateHighscore(chapter, newScore);
      } else{
        Debug.Log("<color=blue>Newest score (" + newScore + ") is lower than the score before " + scoreBefore + ". Won't update new highscore.</color>");
      }
    } else{
      Debug.Log("Error - invalid leaderboard chapter: " + chapter);
    }      
      
  }

  private static void UpdateHighscore(int chapter, int newHighScore){    
    Persistence.GetInstance().SaveLeaderboardHighscore(chapter, newHighScore);
    CommitPendingHighscores();
  }


  public static void CommitPendingHighscores(){
    // try to commit the pending highscores

    #if (UNITY_ANDROID || UNITY_IOS)
    Debug.Log("<color=blue>Checking and commiting any new highscores...</color>");
    for (int i = 1 ; i <= 4 ; i++){
      int chapterNumber = i;
      string chapterCode = GetLeaderboardCodeByChapter(chapterNumber);

      int highScore = Persistence.GetInstance().GetLeaderboardHighscore(chapterNumber);
      if (highScore > 0){
        Social.ReportScore(highScore, chapterCode, (bool success) => {
          if (success){
            Debug.Log("<color=green>Successfully commited highscore of " + highScore + " (chapter " + chapterNumber + ")</color>");
          } else{
            Debug.Log("<color=red>Error: couldn't commit highscore for chapter " + chapterNumber + ".</color>");
          }
        });
      }
    }
    #endif
  }


  public static void ShowLeaderboard(string leaderboardCode){
    Social.ShowAchievementsUI();
  }

  private static string GetLeaderboardCodeByChapter(int chapter){
    if (Application.platform == RuntimePlatform.Android){
      switch(chapter){
        case 1:
          return GPGSIds.leaderboard_placar_transgnicos;
        case 2:
          return GPGSIds.leaderboard_placar_desarmamento;
        case 3:
          return GPGSIds.leaderboard_placar_sade;
        case 4:
          return GPGSIds.leaderboard_placar_corrupo;
        default:
          return "";
      }
    } else if (Application.platform == RuntimePlatform.IPhonePlayer){
      switch(chapter){
        // FIXME get correct ids!
        case 1:
          return "1";
        case 2:
          return "2";
        case 3:
          return "3";
        case 4:
          return "4";
        default:
          return "";
      }
    }
    else{
      return "";
    }
  }
}
