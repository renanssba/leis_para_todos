﻿using System;
using System.Collections.Generic;

public interface GenericSocialAchievementCommitter {
  void Commit(List<Achievement> listAchievements);
}

