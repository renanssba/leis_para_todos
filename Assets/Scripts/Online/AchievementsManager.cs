﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;
using GooglePlayGames.BasicApi;

public class AchievementsManager {

  // achievements representation here
  static List<Achievement> listAchievements;


  public static void Initialize() {
    // Initialization, automatically calls "static" constructor
  }

  static AchievementsManager() {
    //Fill achievement names/codes
    AchievementsManager.FillAchievements();

    AchievementsManager.LoadAchievementsFromLocal();  
    
  }

  private static void LoadAchievementsFromLocal() {
    foreach(Achievement achievement in listAchievements) {
      Persistence.GetInstance().LoadLocalAchievementProgress(achievement);
    }
  }

  private static void FillAchievements() {
    listAchievements = new List<Achievement>();

    // VisualNovel Progress
    AchievementsManager.listAchievements.Add(new Achievement(GPGSIds.achievement_a_culpa__do_mordomo, 1, "A culpa é do mordomo!"));
    AchievementsManager.listAchievements.Add(new Achievement(GPGSIds.achievement_nova_mensagem_recebida, 2, "Nova mensagem recebida"));
    AchievementsManager.listAchievements.Add(new Achievement(GPGSIds.achievement_o_ltimo_discurso, 3, "O último discurso"));
    AchievementsManager.listAchievements.Add(new Achievement(GPGSIds.achievement_at_mais_colina, 4, "Até mais, Colina"));
    AchievementsManager.listAchievements.Add(new Achievement(GPGSIds.achievement_deputado_preguioso, 5, "Deputado Preguiçoso"));

    // Speech
    AchievementsManager.listAchievements.Add(new Achievement(GPGSIds.achievement_discursos_impecveis, 6, "Discursos Impecáveis"));

    // Relationships
    AchievementsManager.listAchievements.Add(new Achievement(GPGSIds.achievement_diplomacia, 7, "Diplomacia"));

    // Research   
    AchievementsManager.listAchievements.Add(new Achievement(GPGSIds.achievement_encontrou_a_verdade, 8, "Encontrou a Verdade"));

    // Date
    AchievementsManager.listAchievements.Add(new Achievement(GPGSIds.achievement_romance_de_infncia, 9, "Romance de Infância"));
    AchievementsManager.listAchievements.Add(new Achievement(GPGSIds.achievement_deputado_plays, 10, "Deputado Plays"));
    AchievementsManager.listAchievements.Add(new Achievement(GPGSIds.achievement_sada_pra_danar, 11, "Saída pra Dançar"));

    // Minigame (Progress)
    AchievementsManager.listAchievements.Add(new Achievement(GPGSIds.achievement_habilidoso, 12, "Habilidoso", 50, false));
    AchievementsManager.listAchievements.Add(new Achievement(GPGSIds.achievement_quanto_mais_melhor, 13, "Quanto mais, melhor", 20, false));
    AchievementsManager.listAchievements.Add(new Achievement(GPGSIds.achievement_pesquisador, 14, "Pesquisador", 50, false));
    AchievementsManager.listAchievements.Add(new Achievement(GPGSIds.achievement_mudando_o_jogo, 15, "Mudando o jogo", 100, false));
    AchievementsManager.listAchievements.Add(new Achievement(GPGSIds.achievement_desarmando, 16, "Desarmando", 50, false));
    AchievementsManager.listAchievements.Add(new Achievement(GPGSIds.achievement_sade_de_ferro, 17, "Saúde de Ferro", 500, false));
    AchievementsManager.listAchievements.Add(new Achievement(GPGSIds.achievement_corrupo, 18, "Corrupção", 3000, false));

    // Minigame (Non-progress)
    AchievementsManager.listAchievements.Add(new Achievement(GPGSIds.achievement_esgotando_as_fontes, 19, "Esgotando as fontes"));
    AchievementsManager.listAchievements.Add(new Achievement(GPGSIds.achievement_1__pouco_2__bom, 20, "1 é Pouco, 2 é Bom..."));
    AchievementsManager.listAchievements.Add(new Achievement(GPGSIds.achievement_3__demais, 21, "3 é demais!"));
    AchievementsManager.listAchievements.Add(new Achievement(GPGSIds.achievement_sede_de_conhecimento, 22, "Sede de conhecimento"));
    AchievementsManager.listAchievements.Add(new Achievement(GPGSIds.achievement_foco_inabalvel, 23, "Foco inabalável"));
    AchievementsManager.listAchievements.Add(new Achievement(GPGSIds.achievement_mudando_o_jogo, 24, "Jogada de mestre"));

    // Social
    AchievementsManager.listAchievements.Add(new Achievement(GPGSIds.achievement_f_da_supernova, 25, "Fã da Supernova"));

    // Platinum
    AchievementsManager.listAchievements.Add(new Achievement(GPGSIds.achievement_presidente_da_colina, 26, "Presidente da Colina", 1, true));
  }


  public static void IncrementAchievement(string achievementName, int amount) {
    Achievement relatedAchievement = listAchievements.Find(achievement => achievement.achievementName == achievementName);

    if(relatedAchievement != null) {
      if(relatedAchievement.status == Achievement.Status.LOCKED) {
        relatedAchievement.progress += amount;
//        Debug.Log("<color=blue>Incrementing achievement " + relatedAchievement.achievementName
//                  + " by " + amount + " - progress: " + relatedAchievement.progress + "/" + relatedAchievement.target + ".</color>");

        if(relatedAchievement.progress >= relatedAchievement.target) {
          UnlockAchievement(relatedAchievement.achievementName);
        } else {
          Persistence.GetInstance().SaveLocalAchievementProgress(relatedAchievement);
        }
      } else {
//        Debug.Log("Tried to increment an already unlocked achievement: " + relatedAchievement.achievementName);
      }
    } else {
//      Debug.Log("<color=red>Achievement " + achievementName + " was not found in local list. Is it in the AchievementsManager list of achievements?</color>");
    }
  }

  public static void UnlockAchievement(string achievementName) {
    // save this achievement as pending (that needs committing)
    // if connected to internet and (GPS) Google Play Services, commit achievements      

    Achievement relatedAchievement = listAchievements.Find(achievement => achievement.achievementName == achievementName);

    if(relatedAchievement != null) {
      if(relatedAchievement.status == Achievement.Status.LOCKED) {
        relatedAchievement.status = Achievement.Status.PENDING;
        relatedAchievement.progress = relatedAchievement.target;
        Persistence.GetInstance().SaveLocalAchievementProgress(relatedAchievement);

        Debug.Log("<color=#00FF00>UNLOCKED ACHIEVEMENT: " + relatedAchievement.achievementName + "</color>");

        CommitLocalAchievements();
      } else {
        Debug.Log("Tried to unlock an already unlocked achievement: " + relatedAchievement.achievementName);
      }

    } else {
      Debug.Log("Achievement " + achievementName + " was not found in local list.");
    }
      
    if(HasAllNonPlatinumAchievements() && HasPlatinum() == false) {
      Debug.Log("Will unlock platinum trophy");
      UnlockPlatinum();
    } 
  }

  private static void UnlockPlatinum() {
    Achievement platinumAchievement = listAchievements.Find(achievement => achievement.isPlatinum);

    if(platinumAchievement != null) {
      UnlockAchievement(platinumAchievement.achievementName);
    }
  }

  private static bool HasPlatinum() {
    Achievement platinumAchievement = listAchievements.Find(achievement => achievement.isPlatinum);

    if(platinumAchievement != null) {
      if(platinumAchievement.status == Achievement.Status.LOCKED) {
        return false;
      } else {
        return true;
      }
    }

    return false;
  }

  private static bool HasAllNonPlatinumAchievements() {
    
    foreach(Achievement achievement in listAchievements) {
      // If there's still one achievement locked that isn't the platinum, then return false
      if(achievement.isPlatinum == false && achievement.status == Achievement.Status.LOCKED) {
        return false;
      }
    }
    return true;

  }



  public static void CommitLocalAchievements() {
    // try to commit the pending/partial achievements
    GenericSocialAchievementCommitter committer = null;

    if(Application.platform == RuntimePlatform.Android) {
      committer = new GooglePlayAchievementCommitter();
    } else if(Application.platform == RuntimePlatform.IPhonePlayer) {
      committer = new GameCenterAchievementCommitter();
    }

    if(committer != null) {
      committer.Commit(listAchievements);
    }   

  }


  public static void ShowAchievements() {
    // show Google Play Services default list of achievements
    Social.ShowAchievementsUI();
  }

}

