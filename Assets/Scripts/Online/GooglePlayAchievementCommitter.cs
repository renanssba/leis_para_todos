﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;
using GooglePlayGames.BasicApi;

public class GooglePlayAchievementCommitter : GenericSocialAchievementCommitter {

  void GenericSocialAchievementCommitter.Commit(List<Achievement> listAchievements) {
    #if (UNITY_ANDROID)
    Debug.Log("Commiting current progress for achievements...");

    foreach(Achievement achievement in listAchievements) {
      if(achievement.status == Achievement.Status.PENDING) {
        // Complete achievement, pending commit.

        if(achievement.isProgress) {
          // Progress met the target value
          PlayGamesPlatform.Instance.IncrementAchievement(achievement.googlePlayId, achievement.progress, (bool success) => {
            if(success) {
              achievement.status = Achievement.Status.COMMITED;
              Persistence.GetInstance().SaveLocalAchievementProgress(achievement);
              Debug.Log("<color=green>Committed " + achievement.achievementName + " to Google Play - " + achievement.progress + "/" + achievement.target + "</color>");
            } else {
              Debug.Log("<color=red>Failed to commit " + achievement.achievementName + " to Google Play (Progress achievement).</color>");
            }
          });
        } else {
          Social.ReportProgress(achievement.googlePlayId, 100f, (bool success) => {
            if(success) {
              achievement.status = Achievement.Status.COMMITED;
              Persistence.GetInstance().SaveLocalAchievementProgress(achievement);
              Debug.Log("<color=green>Committed " + achievement.achievementName + " to Google Play - " + achievement.progress + "/" + achievement.target + "</color>");
            } else {
              Debug.Log("<color=red>Failed to commit " + achievement.achievementName + " to Google Play (Non-progress achievement).</color>");
            }
          });
        }

      } else if(achievement.status == Achievement.Status.LOCKED && achievement.isProgress) {
        // Achievement still locked, but will update progress

        PlayGamesPlatform.Instance.IncrementAchievement(achievement.googlePlayId, achievement.progress, (bool success) => {
          if(success) {
            Debug.Log("<color=green>Committed " + achievement.achievementName + " to Google Play - " + achievement.progress + "/" + achievement.target + "</color>");
          } else {
            Debug.Log("<color=red>Failed to commit " + achievement.achievementName + " to Google Play (Locked progress achievement).</color>");
          }
        });
      }
    }

    #else
    Debug.Log("Tried commiting android achievements in a non-Android platform");
    #endif
  }

}

