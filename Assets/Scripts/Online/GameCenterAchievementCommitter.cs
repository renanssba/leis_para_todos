﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using UnityEngine.SocialPlatforms.GameCenter;

public class GameCenterAchievementCommitter : GenericSocialAchievementCommitter {

  void GenericSocialAchievementCommitter.Commit(List<Achievement> listAchievements) {
    #if (UNITY_IOS)
    Debug.Log("Commiting current progress for achievements...");

    foreach(Achievement achievement in listAchievements) {
      if(achievement.status == Achievement.Status.PENDING) {
        // Complete achievement, pending commit.

        Social.ReportProgress(achievement.gameCenterId, 100f, ((bool success) => {
          if(success) {
            achievement.status = Achievement.Status.COMMITED;
            Persistence.GetInstance().SaveLocalAchievementProgress(achievement);
            Debug.Log("<color=green>Committed " + achievement.achievementName + " to Game Center - " + achievement.progress + "/" + achievement.target + "</color>");
          } else {
            Debug.Log("<color=red>Failed to commit " + achievement.achievementName + " to Game Center (Progress achievement).</color>");
          }
        }
        ));


      } else if(achievement.status == Achievement.Status.LOCKED && achievement.isProgress) {
        // Achievement still locked, but will update progress
        float progressPercentage = ((float)achievement.progress / achievement.target) * 100f;
        Social.ReportProgress(achievement.gameCenterId, progressPercentage, (bool success) => {
          if(success) {
            Debug.Log("<color=green>Committed " + achievement.achievementName + " to Game Center - " + achievement.progress + "/" + achievement.target + "</color>");
          } else {
            Debug.Log("<color=red>Failed to commit " + achievement.achievementName + " to Game Center (Locked progress achievement).</color>");
          }
        }); 
      }
    }

    #else
    Debug.Log("Tried commiting iOS achievements in a non-iOS platform");
    #endif
  }



}

