﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using GooglePlayGames;
using GooglePlayGames.BasicApi;

public class GooglePlayConnector : GenericSocialConnector {

  void GenericSocialConnector.Connect() {
    #if (UNITY_ANDROID)
    Debug.Log("<color=blue>Trying to connect to Google Play...</color>");
    PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();

    PlayGamesPlatform.InitializeInstance(config);
    // recommended for debugging:
    PlayGamesPlatform.DebugLogEnabled = true;
    // Activate the Google Play Games platform
    PlayGamesPlatform.Activate();

    Social.localUser.Authenticate((bool success) => {
      // handle success or failure
      if (success){
        Debug.Log("<color=green>Successfully logged to Google Play Services</color>");
        AchievementsManager.CommitLocalAchievements();
        LeaderboardsManager.CommitPendingHighscores();
      } else{
        Debug.Log("<color=red>Error: couldn't log into Google Play Services</color>");
      }
    });

    #endif
  }

}
