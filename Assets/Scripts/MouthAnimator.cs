﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MouthAnimator : MonoBehaviour {
	
	public Sprite[] anim;
	public float clock;
	public int index;
	public bool is_animating;
  public Image mouthImage;
	private float mouthFrameTime = 0.13f;
	
	void Start () {
		ResetAnim();
	}

	void Update () {
		if( is_animating ){
			clock += Time.deltaTime;

			if(clock > mouthFrameTime){
				index+=1;
        index = CapInt(index, anim.Length);
        SetSprite(index);
				clock -= mouthFrameTime;
			}
		}
	}

  void SetSprite(int index){
    mouthImage.sprite = anim[index];
//    Debug.Log("Y size: " + anim[index].rect.yMax);
    float dy = anim[index].rect.yMax - 512f;
    mouthImage.gameObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, dy);
//    mouthImage.SetNativeSize();
  }

	public void SetAnim(Sprite[] new_anim){
//		ResetAnim();
		anim = new_anim;

		if(anim != null){
      SetSprite(0);
			gameObject.SetActive(true);
		}else{
			gameObject.SetActive(false);
		}
	}

	public void StartAnim(){
		//character.mouthObject.SetActive(true);

		ResetAnim();

		if(anim!=null){
			is_animating = true;
      SetSprite(0);
		}else{
			is_animating = false;
		}
	}

	public void StopAnim(){
		//character.mouthObject.SetActive(false);

		if(is_animating){
      SetSprite(0);
			is_animating = false;
		}else{
			// do nothing
		}
	}

	void ResetAnim(){
		index = 0;
		clock = 0f;
	}

	int CapInt(int num, int capAt){
		if(num >= capAt)
			return 0;
		else
			return num;
	}

}
