using UnityEngine;
using UnityEngine.Analytics;
using System.Collections;
using System.IO;
using System.Collections.Generic;

public class GameController : MonoBehaviour {
	
  public static GameController instance;

  public GameState gameState;
  public TextAsset[] scriptList = new TextAsset[4];
  public int chapterId;
  public int lineStarted;
  public int checkpointStarted;
  public int currentLine = -1;
  public int lineCount;

  public Sprite bgTemp { get; set; }

  public Quit quit;
  public CharacterList charList;

  public DialogBox dialogBox;
  public DialogScreen screen;
  public ScriptReader reader;

  private int[] choices;
  private int testimonyNextWp;
  private string[] labelNames;
  private string questionName;

  void Awake() {
    instance = this;
  }

  public static GameController GetInstance() {
    return instance;
  }



  public enum GameState {
    PlayingScript,
    Dialog,
    Question,
    Collection,
    Waiting,
    Inventory,
    ChooseTopic
  }

	
  void Start() {
    dialogBox.gameObject.SetActive(false);


    /// LOAD correct chapter
    if(Persistence.chapter_to_load > 0) { // if value is valid
      chapterId = Persistence.chapter_to_load;
    }		
    reader.SetCurrentScript(scriptList[chapterId - 1]);

    reader.LoadScript(checkpointStarted);


    // LOAD the game from a savepoint
    if(Persistence.GetInstance().isLoadingGame == true) {	
      string savepointName = Persistence.GetLastSavepoint();
      Debug.Log("savepoint to load: " + savepointName);
			
      if(savepointName.Length > 0) {				
        ScriptReader.GetInstance().GoToSavepoint(savepointName);
      }
      Persistence.GetInstance().isLoadingGame = false;
    
    // LOAD the game from a checkpoint
    } else {

      if(Persistence.checkpoint_to_load >= 0){ // if value is valid
        checkpointStarted = Persistence.checkpoint_to_load;
        Debug.Log("checkpoint to start: " + checkpointStarted);
      }

      Debug.LogWarning("Checkpoint to load: " + checkpointStarted);
      Debug.LogWarning("Line to load: " + reader.checkpoints[checkpointStarted]);

      lineStarted = reader.checkpoints[checkpointStarted];
      ScriptReader.GetInstance().GoToLine(lineStarted);
    }


    DebugMode.gameController = this;

    // load the character animations here
    Persistence.DeleteCharacterAnims();
    Persistence.GetInstance().CreateCharacterAnims();

		
    gameState = GameState.PlayingScript;
  }

  void Update() {
//    if(Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return))
//      ClickedScreen();
		
    CheckToPlayScript();

		if(Input.GetKey(KeyCode.S) && (Persistence.debugMode)){
      ClickedScreen();
    }   
  }



  public void StartVSNScript(string scriptToLoad){
    StartVSNScript(scriptToLoad, 0);
  }

  public void StartVSNScript(string scriptToLoad, string waypointToStart){
    StartVSNScript(scriptToLoad, 0);
    int lineToStart = Command.GetInstance().waypoints[waypointToStart];
    ScriptReader.GetInstance().GoToLine(lineToStart);
  }

  public void StartVSNScript(string scriptToLoad, int lineToLoad){

  }

  public void ResumeVSN() {
    gameState = GameState.PlayingScript;
    screen.gameObject.SetActive(true);
  }

  public void PauseVSN() {
    gameState = GameState.Waiting;
    screen.gameObject.SetActive(false);
  }



  public void ClickedScreen() {
    Debug.Log("clicked screen!");
    switch(gameState) {
      case GameState.Dialog:
        AdvanceDialog();
        break;
      case GameState.Question:
        SkipDialog();
        break;
    }
  }

  void CheckToPlayScript() {
    if(gameState == GameState.PlayingScript) {
      PlayScript();
    }
  }

  void PlayScript() {
    reader.ReadScript();
  }

  void AdvanceDialog() {
    if(SkipDialog())
      return;
    if(NextDialog())
      return;
  }

  public void ChooseOption(int value) {
    AudioController.GetInstance().PlayClickSound();
    SkipDialog();
    AnswerQuestion(value);

    Analytics.CustomEvent("choiceEvent", new Dictionary<string, object>{ { questionName, labelNames[value] } });
  }

  public void SetQuestion(string question) {
    questionName = question;
  }

  public void SetLabelNames(string[] names) {
    labelNames = names;
  }

  void AnswerQuestion(int choiceIndex) {
    ScriptReader.GetInstance().GoToLine(choices[choiceIndex]);
    gameState = GameState.PlayingScript;
  }

  public void GoToSelectedWaypoint(int wp) {
    ScriptReader.GetInstance().GoToLine(wp);
    gameState = GameState.PlayingScript;
  }

  public bool SkipDialog() {
    if(dialogBox.talking) {
      dialogBox.SkipDialog();
      return true;
    }
    return false;
  }

  public bool NextDialog() {
    if(!dialogBox.talking) {
      //screen.StopAnimation();
      dialogBox.ShowIndicatorArrow(false);
      gameState = GameState.PlayingScript;
      return true;
    }
    return false;
  }

  public void SetQuestionState(int[] choicesindex) {
    choices = choicesindex;
    gameState = GameState.Question;
  }

  public void SetDialogState() {
    gameState = GameState.Dialog;
  }

  public void SetCollectionState() {
    gameState = GameState.Collection;
  }

  public void SetChooseTopicState() {
    gameState = GameState.ChooseTopic;
  }

  public void WaitForItem() {
    gameState = GameState.Waiting;
  }

  public void WaitSeconds(float time) {
    StartCoroutine( Wait(time) );
  }

  IEnumerator Wait(float time) {
    gameState = GameState.Waiting;
    if(Input.GetKey(KeyCode.S) && (Persistence.debugMode)) time/=4f; // speed mode
    yield return new WaitForSeconds(time);
    gameState = GameState.PlayingScript;
  }

  public void State_Inventory() {
    gameState = GameState.Inventory;
  }

  public void ReloadLevel() {
    Persistence.Load(chapterId, 0);
  }

  public void AdvanceChapter() {
    if(chapterId < 4) {
      Persistence.SetChapterAccess(chapterId + 1, 1);
      Persistence.ReachCheckpoint(chapterId + 1, 0);
      Persistence.Load(chapterId + 1, 0);
      Persistence.ResetTopics();
    } else { // if playing the last chapter, go back to title screen
      quit.ToTitleScreen();
    }
  }

  public void ExitGame() {
    Application.Quit();		
  }
}
