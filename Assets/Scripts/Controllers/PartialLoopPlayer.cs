﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Plays an intro clip only once, and then loops another one. /// </summary>
public class PartialLoopPlayer : MonoBehaviour {

	public AudioSource introSource;
	public AudioSource loopSource;

	void Start(){
		StartSplaying();
	}
	
	public void SetMusic(AudioClip intro, AudioClip loop){
		introSource.Stop();
		loopSource.Stop();
		
		introSource.clip = intro;
		loopSource.clip = loop;
		
		StartSplaying();
	}

	public void StartSplaying(){
		if( introSource.clip!=null ){
			introSource.Play();
			if( introSource.clip!=null ){
				loopSource.PlayDelayed(introSource.clip.length);
			}
		}else if( loopSource.clip!=null ){
			loopSource.Play();
		}
	}

}
