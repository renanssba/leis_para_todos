﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using FMODUnity;
using FMOD.Studio;

public class AudioController : MonoBehaviour {

  private static AudioController instance;
	private bool fadingMusic;

  public bool supportsAudio = false;

  FMOD.Studio.EventInstance musicEvent = null;
  FMOD.Studio.EventInstance ambienceEvent = null;

  [FMODUnity.BankRef]
  public string masterBank;
  [FMODUnity.BankRef]
  public string sfxBank;
  [FMODUnity.BankRef]
  public string uiBank;
	[FMODUnity.BankRef]
	public string puzzleBank;
  [FMODUnity.BankRef]
  public string[] musicBanks;

  void Awake(){
    if(instance == null){
      instance = this;
      DontDestroyOnLoad(transform.gameObject);

      UpdateMusicVolume(1f);
      UpdateSfxVolume();

      FMODUnity.RuntimeManager.LoadBank(masterBank);
      FMODUnity.RuntimeManager.LoadBank(sfxBank);
      FMODUnity.RuntimeManager.LoadBank(uiBank);
//	    FMODUnity.RuntimeManager.LoadBank(puzzleBank);
      foreach(string bank in musicBanks)
        FMODUnity.RuntimeManager.LoadBank(bank);
      supportsAudio = true;
    }else if( instance!=this ){
      Destroy(gameObject);
      return;
    }
  }

  public static AudioController GetInstance(){
    if(instance == null){
      instance = GameObject.FindWithTag("AudioControl").GetComponent<AudioController>();
    }
    return instance;
  }

	public void FadeMusic(float anim_time){
		if(anim_time == 0){
      StopMusic();
      // stop quickly
		}else{
      StartCoroutine(FadeOutMusic(anim_time));
			fadingMusic = true;
		}
	}

	public void StopFading(){
		StopCoroutine(FadeOutMusic(1f));
    StopMusic();
	}
	
  IEnumerator FadeOutMusic(float fadeTime){
    if(!supportsAudio) yield break;

    if(Input.GetKey(KeyCode.S) && (Persistence.debugMode)) fadeTime/=4f; // speed mode
    float currentTime = fadeTime;

    if(musicEvent!=null){
      while(currentTime > 0){
        UpdateMusicVolume(currentTime / fadeTime);
        currentTime -= Time.deltaTime;
  			yield return new WaitForEndOfFrame();
  		}
      StopMusic();
    }
	}
	
  public void PlayMusic(string name){
    if(!supportsAudio) return;

    if(musicEvent!=null){
      FMOD.Studio.EventDescription description;
      string eventName;

      musicEvent.getDescription(out description);
      if(description != null){
        description.getPath(out eventName);
        if(eventName == "event:/musicas/"+name){
          // dont stop and replay music
          //Debug.log("spare playing music");
          return;
        }
      }

      StopMusic();
    }

    if(name != "none"){
      musicEvent = FMODUnity.RuntimeManager.CreateInstance("event:/musicas/"+name);
      UpdateMusicVolume(1f);
      musicEvent.start();
    }
	}

  public void SetMusicArgument(float argument){
    if(!supportsAudio) return;

    musicEvent.setParameterValue("discurso", argument);
  }

  public void StopMusic(){
    if(!supportsAudio) return;

    musicEvent.stop(FMOD.Studio.STOP_MODE.IMMEDIATE); 
    musicEvent.release();
    fadingMusic = false;
  }
	
	public void PlayClickSound(){
		PlaySfx("ui/opcao3");
	}
	
	public void PlayCancelSound(){
    PlaySfx("ui/cancelar1");
	}
	
	public void PlayConfirmSound(){
    PlaySfx("ui/evidencia2");
	}
	
	public void PlaySlideSound(){
    PlaySfx("ui/opcao1");
	}
	
	public void PlayPageSound(){
    PlaySfx("ui/pagina1");
  }
	
	public void PlayItemSound(){
    PlaySfx("ui/evidencia2");
	}
	
	public void PlayDialogSound(){
    PlaySfx("dialogo/dialogo1");
  }


  public void PlayAmbience(string name, float argument){
    if(!supportsAudio) return;

    if(ambienceEvent!=null){
      StopAmbience();
    }

    if(name != "none"){
      ambienceEvent = FMODUnity.RuntimeManager.CreateInstance("event:/ambiencia/"+name);
      ambienceEvent.setParameterValue("Hype", argument);
      ambienceEvent.setVolume(Options.GetSfxVolume()*1f *0.5f);
      ambienceEvent.start();
    }
  }

  public void StopAmbience(){
    if(!supportsAudio) return;

    ambienceEvent.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
    ambienceEvent.release();
  }



  public void PlaySfx(string name){
    if(!supportsAudio) return;

    string fullPath = "event:/sfx/"+name;

    FMODUnity.RuntimeManager.PlayOneShot(fullPath, new Vector3(0f, 0f, 0f));
  }

  public void UpdateMusicVolume(float multiplier){
    if(!supportsAudio) return;

    if(musicEvent != null){
      musicEvent.setVolume( Options.GetMusicVolume()/4f * multiplier );
    }
  }

  public void UpdateSfxVolume(){
    if(!supportsAudio) return;

    FMOD.Studio.VCA sfxVCA =  FMODUnity.RuntimeManager.GetVCA("vca:/sfx");
    if(sfxVCA != null){
      sfxVCA.setFaderLevel( Options.GetSfxVolume()/4f );
    }
  }
	
	float GetCharacterPitch(string characterName){
    return 1f;
	}
}
