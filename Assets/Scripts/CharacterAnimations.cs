﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public static class CharacterAnimations {

	private static Dictionary<string, Sprite[]> mouthAnimations = new Dictionary<string, Sprite[]>();
  private static Dictionary<string, Sprite[]> eyeAnimations = new Dictionary<string, Sprite[]>();
  private static Dictionary<string, MoodAnim> moods = new Dictionary<string, MoodAnim>();


  public class MoodAnim{
    public string name;
    public string mouthAnim;
    public string eyesAnim;
  }


	public static void DeleteAnimations(){

		foreach(string anim in mouthAnimations.Keys){
      UnloadAnimation(mouthAnimations[anim]);
		}

		foreach(string anim in eyeAnimations.Keys ){
      UnloadAnimation(eyeAnimations[anim]);
		}

		mouthAnimations.Clear();
		eyeAnimations.Clear();
    moods.Clear();
		
		Resources.UnloadUnusedAssets();
		mouthAnimations = new Dictionary<string, Sprite[]>();
		eyeAnimations = new Dictionary<string, Sprite[]>();
    moods = new Dictionary<string, MoodAnim>();
	}


  public static void UnloadAnimation(Sprite[] animation){
    for(int i=0; i<animation.Length; i++){
      for(int j=i+1; j<animation.Length; j++){
        if(animation[i] == animation[j]) {
          animation[j] = null;
        }
      }
    }

    for(int i = 0; i < animation.Length; i++) {
      if(animation[i] != null) {
        Resources.UnloadAsset( animation[i] );
      }
    }
  }


	public static void CreateMoodAnimation(string moodName, string mouthAnim, string eyesAnim){

    if(moods.ContainsKey(moodName)){
//      Debug.LogWarning("Trying to create same mood: " + moodName);
      return;
    }

    MoodAnim newMood = new MoodAnim();
    CreateMouthAnimation(mouthAnim);
    CreateEyeAnimation(eyesAnim);

    newMood.name = moodName;
    newMood.mouthAnim = mouthAnim;
    newMood.eyesAnim = eyesAnim;

    moods[moodName] = newMood;
  }

  public static MoodAnim GetMood(string moodName){
    return moods[moodName];
  }


	public static void CreateMouthAnimation(string anim_name){

		string mouth_path = "Characters/Mouth/" + anim_name;
		Sprite[] sprites = new Sprite[6];

		if(mouthAnimations.ContainsKey(anim_name)){
//      Debug.LogWarning("Trying to create same anim: "+anim_name);
			return;
		}

		sprites[0] = Resources.Load<Sprite>(mouth_path + "_0");
		sprites[1] = Resources.Load<Sprite>(mouth_path + "_1");
		sprites[2] = Resources.Load<Sprite>(mouth_path + "_2");
		sprites[3] = sprites[1];
		sprites[4] = sprites[2];
		sprites[5] = sprites[1];
    //Debug.log("Animation: "+anim_name+" created with success!");

    if(sprites[1] == null) {
      Sprite aux = sprites[0];
      sprites = new Sprite[1];
      sprites[0] = aux;
    }

		// memorize the new animation in the dictionary
		mouthAnimations[anim_name] = sprites;
	}
	
	public static void CreateEyeAnimation(string anim_name){

		string eyes_path = "Characters/Eyes/" + anim_name;
    Sprite[] sprites = new Sprite[2];
    
    if(eyeAnimations.ContainsKey(anim_name)){
//      Debug.LogWarning("Trying to create same anim: "+anim_name);
      return;
    }

    sprites[0] = Resources.Load<Sprite>(eyes_path + "_0");
    sprites[1] = Resources.Load<Sprite>(eyes_path + "_1");

    if(sprites[1] == null) {
      sprites[1] = sprites[0];
    }
    
    // memorize the new animation in the dictionary
    eyeAnimations[anim_name] = sprites;
	}

	public static Sprite[] GetMouthAnimation(string anim_name){
		if(mouthAnimations.ContainsKey(anim_name)) {
			return mouthAnimations [anim_name];
		}else{
			return null;
		}
	}

	public static Sprite[] GetEyeAnimation(string name){

		if(eyeAnimations.ContainsKey(name)) {
			return eyeAnimations [name];
		}else{
			return null;
		}
	}
}
