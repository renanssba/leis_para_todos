using UnityEngine;
using System.Collections;

public class ResourceManager : MonoBehaviour {
	
	public static ResourceManager rman;
	public Sprite[] characters;
	public Sprite[] backgrounds;


	void Start () {
		rman = this;
	}

	public Sprite GetCharacter(string spr_name){

		if(spr_name.ToLower()=="none"){
//			//Debug.log("tried loading: "+spr_name+", but could not. setting character to none");
			return null;
		}

		foreach(Sprite spr in characters){
			if( spr.name == spr_name ){
//				//Debug.log("setting character to: "+spr_name);
				return spr;
			}
		}
//		//Debug.log("no sprite named: "+spr_name+"found. setting character to none");
		return null;
	}

	public Sprite GetBackground(string bg_name){

		if(bg_name.ToLower()=="none"){
//			//Debug.log("tried loading: "+bg_name+", but could not. setting bg to none");
			return null;
		}
		
		foreach(Sprite spr in backgrounds){
			if( spr.name == bg_name ){
//				//Debug.log("setting bg to: "+bg_name);
				return spr;
			}
		}
//		//Debug.log("no sprite named: "+bg_name+"found. setting bg to none");
		return null;
	}
}
