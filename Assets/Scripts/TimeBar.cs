using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TimeBar : MonoBehaviour {

	public int waypoint { get; set;}
	public IEnumerator timer;
	private Image fillImage;
	private GameController game;
	
	void Start() {
		fillImage = transform.Find("Fill").Find("Fill").GetComponent<Image>();
		game = GameObject.FindWithTag("GameController").GetComponent<GameController>();
	}

	public void StartTimer(float length, int waypoint){
		fillImage.fillAmount = 1;
		this.waypoint = waypoint;
		timer = Timer(length);
		StartCoroutine(timer);
	}

	public void timerEnded(){
		game.GoToSelectedWaypoint(waypoint);
		this.gameObject.SetActive(false);
	}

	IEnumerator Timer(float length){

		float elapsedTime = 0;
		float time = length;
		while(elapsedTime < time){
			float lerp =(Mathf.Lerp(100, 0, elapsedTime / time)) / 100;
			fillImage.fillAmount = lerp;
			elapsedTime += Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}
		timerEnded();
	}
}
