﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;
using System.Collections;
using UnityEngine.SceneManagement;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;
using GooglePlayGames.BasicApi;

public class TitleScreen : MonoBehaviour {

  public GameObject confirmNewGameScreen;
  public Button continueButton;
  public Button freeplayButton;
  public Fade fade;

  public GameObject optionsWindow;
  public Options options;


	void Awake(){		
    Persistence.GetInstance().SetVariableValue("isFreeplayMode", 0);
    AchievementsManager.Initialize();

		// initialize buttons
		CheckButtonsActive();
    StartCoroutine(WaitAndPlayMusic());

    ConnectToGameService();
	}

  void ConnectToGameService() {
    GenericSocialConnector socialConnector = null;
    if (Application.platform == RuntimePlatform.Android){
      socialConnector = new GooglePlayConnector();
    } else if (Application.platform == RuntimePlatform.IPhonePlayer){
      socialConnector = new GameCenterConnector();
    }

    if (socialConnector != null) {
      socialConnector.Connect();
    }
  }

  public IEnumerator WaitAndPlayMusic(){
    yield return new WaitForSeconds(0.1f);
    AudioController.GetInstance().PlayMusic("infancia");
  }
	
	void CheckButtonsActive(){

		if( Persistence.GetLastPlayedChapter() == -1 ){
      continueButton.interactable = false;
		}else{
      continueButton.interactable = true;
		}

    if(PlayerPrefs.GetInt("isFreeplayUnlocked", 0) == 1){
      freeplayButton.interactable = true;
    }else{
      freeplayButton.interactable = false;
    }
	}

	void Update(){
		if (Input.GetKeyDown(KeyCode.P) && Persistence.debugMode){
			PlayerPrefs.DeleteAll();
			Debug.Log("Deleted all playerprefs");
			Persistence.SetDebugMode(false);
		}
	}

	public void NewGame(){
    if( Persistence.GetLastPlayedChapter() == -1 ){
      AudioController.GetInstance().PlayConfirmSound();
      StartNewGame();
    }else{
      AudioController.GetInstance().PlayClickSound();
      OpenNewGameConfirmationScreen();
    }
	}

  public void StartNewGame(){
    AudioController.GetInstance().PlayConfirmSound();
    IEnumerator fadeThenLoad = FadeThenLoad(1, 0);
    Persistence.SetLastPlayed(-1, -1);
    Persistence.ResetTopics();
    StartCoroutine(fadeThenLoad);
  }

  public void OpenNewGameConfirmationScreen(){
    confirmNewGameScreen.SetActive(true);
  }


	public void LoadGame(){
		if(Persistence.GetInstance().LoadGame()){	
      AudioController.GetInstance().PlayConfirmSound();
			int chapter = Persistence.chapter_to_load;
			IEnumerator fadeThenLoad = FadeThenLoad(chapter, 0);
      Persistence.GetInstance().isLoadingGame = true;

			StartCoroutine(fadeThenLoad);
		}
	}

  public void FreeplayMode(){
    AudioController.GetInstance().PlayClickSound();
    Persistence.GetInstance().SetVariableValue("isFreeplayMode", 1);
    SceneManager.LoadScene("Freeplay");
  }


  public void Options(){
    AudioController.GetInstance().PlayMusic("infancia");
    AudioController.GetInstance().PlayPageSound();
    optionsWindow.gameObject.SetActive(true);
    options.Open();
  }

  public void CloseOptions(){
    AudioController.GetInstance().PlayPageSound();
    optionsWindow.gameObject.SetActive(false);
  }


	IEnumerator FadeThenLoad(int chapter, int checkpoint){
		int fadeTime = 1;
    AudioController.GetInstance().FadeMusic(fadeTime);
		fade.FadeOut(fadeTime);
		yield return new WaitForSeconds(fadeTime + 1.0f);
		Persistence.Load(chapter, checkpoint);
	}

	public void OpenSupernovaSite(){
		//Application.OpenURL("http://supernova-indiegames.com/");
    Analytics.CustomEvent("openForm", null);
    Application.OpenURL("https://docs.google.com/forms/d/1fsFf8gHuZ9kN1ani4wbehXsqqwLZxoc17PDwKPXm35U/viewform");
	}


}
