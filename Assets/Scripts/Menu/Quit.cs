﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Quit : MonoBehaviour {
	public Fade fade;

	public void QuitGame(){
		Application.Quit();
	}

	public void ToTitleScreen(){
    AudioController.GetInstance().PlayCancelSound();
    StartCoroutine(FadeThenLoad());
	}
	
	IEnumerator FadeThenLoad(){
    Debug.Log("Called Fade Then Load");

		float fadeTime = 1f;
//    AudioController.GetInstance().FadeMusic(fadeTime);
		fade.FadeOut(fadeTime);
		yield return new WaitForSeconds(fadeTime + 0.5f);
		
		CharacterAnimations.DeleteAnimations();
    SceneManager.LoadScene("TitleScreen");
	}
}