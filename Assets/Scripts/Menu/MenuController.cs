﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class MenuController : MonoBehaviour {

  public StatusScreen statusScreen;
  public StatusScreen plScreen;
  public GameObject detailsScreen;
  public StatusScreen skillsScreen;
  public StatusScreen optionsScreen;

  public Text detailsTitleText;
  public Text detailsDescriptionText;

  public Image grayBg;
  public Button menuButton;
  public Button[] menuTabButtons;

  public enum MenuTab {
    status = 0,
    pl = 1,
    skills = 2,
    options = 3
  };


  public void OpenMenu(){
    transform.DOMoveY(0.25f,0f).SetRelative().OnComplete(()=>{
      transform.DOMoveY(-0.25f, 0.25f).SetRelative();
    });
    grayBg.DOFade(0.8f, 0.2f);

    OpenPlScreen();
    if(menuButton != null){
      menuButton.gameObject.SetActive(false);
    }
    gameObject.SetActive(true);
  }


  public void CloseMenu(){
    AudioController.GetInstance().PlayPageSound();

    if (!DOTween.IsTweening(GetComponent<CanvasGroup>())){
      GetComponent<CanvasGroup>().DOFade(0f, 0.25f);
      transform.DOMoveY(-0.25f,0.25f).SetRelative().OnComplete(()=>{
        transform.DOMoveY(0.25f, 0f).SetRelative();
        gameObject.SetActive(false);

      });
    }
    grayBg.DOFade(0f, 0.2f);

    gameObject.SetActive(false);

    if (menuButton != null){
      menuButton.gameObject.SetActive(true);
    }
  }


  public void OpenStatusScreen(){
    CloseAllScreens();
    statusScreen.OpenStatusScreen();
    SetSelectedTab(MenuTab.status);
  }

  public void OpenPlScreen(){
    CloseAllScreens();
    plScreen.OpenStatusScreen();
    SetSelectedTab(MenuTab.pl);
  }

  public void OpenDetailsScreen(){
    AudioController.GetInstance().PlayClickSound();

    detailsTitleText.text = GetPlName();
    detailsDescriptionText.text = GetPlDescription();
    detailsScreen.SetActive(true);
  }

  public void OpenSkillsScreen(){
    // while skills are not implemented
//    AudioController.GetInstance().PlayCancelSound();
//    return;

    CloseAllScreens();
    skillsScreen.OpenStatusScreen();
    SetSelectedTab(MenuTab.skills);
  }

  public void OpenOptionsScreen(){
    CloseAllScreens();
    optionsScreen.OpenStatusScreen();
    SetSelectedTab(MenuTab.options);
  }

  public void CloseDetailsScreen(){
    AudioController.GetInstance().PlayClickSound();
    detailsScreen.SetActive(false);
  }



  public static string GetPlName(){
    switch(Persistence.GetInstance().GetVariableValue("current_chapter") ){
      case 1:
        return "PL da Rotulação dos Transgênicos";
      case 2:
        return "PL do Desarmamento";
      case 3:
        return "PL da Interrupção da Gravidez";
      case 4:
        return "PEC da Polícia Legislativa";
    }
    return "";
  }


  public static string GetPlDescription(){
    switch(Persistence.GetInstance().GetVariableValue("current_chapter") ){
      case 1:
        return "Este Projeto de Lei altera a lei que determina a rotulação de alimentos que contém transgênicos.\nAtualmente, alimentos que possuam qualquer quantidade de transgênicos em seus componentes são rotulados com um símbolo T dentro de um triângulo amarelo. Esta rotulação ocorre mediante a declaração dos produtores destes alimentos, anterior ao processamento dos alimentos.\n\nO projeto de lei implica em 3 mudanças:\n1. Os alimentos só receberão um indicativo em seus rótulos de que contém transgênicos após testes realizados em sua forma final, ou seja, depois do seu processamento.\n2. Somente serão rotulados alimentos que apresentarem mais de 1% de DNA transgênico nos testes.\n3. O símbolo T dentro do triângulo amarelo será removido completamente, e substituído pela frase \"Contém produto transgênico\".";
      case 2:
        return "Este Projeto de Lei visa alterar o Estatuto do Desarmamento, que proíbe a comercialização, posse e porte de armas de fogo para a maior parte da população civil.\n\nO projeto de lei implica em 2 mudanças:\n1. Permitir o porte de armas de fogo pelo cidadão comum, de modo a garantir o direito à legítima defesa.\n2. Redução na burocracia para a aquisição de armas de fogo pelos cidadãos.";
      case 3:
        return "Este Projeto de Lei trata da modificação da legislação quanto à descriminalização da interrupção da gravidez.\nA legislação atual só permite a interrupção voluntária da gravidez em hipóteses extremas, como, por exemplo, quando a gravidez apresenta risco comprovado à saúde da gestante. Nos demais casos, a conduta é punível com um a três anos de detenção.\n\nO projeto visa a descriminalizar a interrupção da gravidez, sendo permitida até a décima segunda semana de gestação, por manifestação de vontade exclusiva da gestante.";
      case 4:
        return "Este Projeto de Emenda Constitucional visa fortalecer a Polícia Legislativa, aumentando a autonomia do Poder Legislativo em relação ao Executivo e Judiciário.\n\nA PEC implica em 2 mudanças:\n1. A competência para investigar e realizar prisões relativas a crimes praticados por deputados será exclusivamente da Polícia Legislativa.\n2. A Polícia Legislativa responderá à Presidência da Câmara dos Deputados e o julgamento criminal de qualquer conduta ilícita praticada por deputados seria de competência de Comissão constituída pela própria casa legislativa.";
    }
    return "";
  }



  void SetSelectedTab(MenuTab menuTab){
    foreach(Button b in menuTabButtons){
      b.interactable = true;
    }
    menuTabButtons[(int)menuTab].interactable = false;
  }

  public void CloseAllScreens(){
    statusScreen.gameObject.SetActive(false);
    plScreen.gameObject.SetActive(false);
    detailsScreen.gameObject.SetActive(false);
    skillsScreen.gameObject.SetActive(false);
    optionsScreen.gameObject.SetActive(false);
  }
}
