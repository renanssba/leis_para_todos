﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillsScreen : MonoBehaviour {

  public Button[] skillButtonsLv1;
  public Button[] skillButtonsLv2;
  public Button[] skillButtonsLv3;

  public GameObject[] equippedIconLv1;
  public GameObject[] equippedIconLv2;
  public GameObject[] equippedIconLv3;

  public Text[] skillLevelTexts;
  public Text skillDescription;
  public Button equipSkillButton;


  private int selectedSkill = -1;


  public void Initialize() {
    selectedSkill = -1;
    UpdateSkillButtons();
    UpdateEquipSkillButton();

    UpdateSkillNames();
  }

  void UpdateSkillButtons() {
    
    for(int i = 0; i < 3; i++) {

      // skill level text
      if(GetSkillsUnlockedInLevel(i + 1) > 0) {
        skillLevelTexts[i].gameObject.SetActive(true);
      } else {
        skillLevelTexts[i].gameObject.SetActive(false);
      }

      // skills level 1 buttons
      if(i < GetSkillsUnlockedInLevel(1)) {
        skillButtonsLv1[i].gameObject.SetActive(true);
        if(GetSkill(1) == i) {
          equippedIconLv1[i].SetActive(true);
        } else {          
          equippedIconLv1[i].SetActive(false);
        }
      } else {
        skillButtonsLv1[i].gameObject.SetActive(false);
      }


      // skills level 2 buttons
      if(i < GetSkillsUnlockedInLevel(2)) {
        skillButtonsLv2[i].gameObject.SetActive(true);
        if(GetSkill(2) - 4 == i) {
          equippedIconLv2[i].SetActive(true);
        } else {
          equippedIconLv2[i].SetActive(false);
        }
      } else {
        skillButtonsLv2[i].gameObject.SetActive(false);
      }


      // skills level 3 buttons
      if(i < GetSkillsUnlockedInLevel(3)) {
        skillButtonsLv3[i].gameObject.SetActive(true);
        if(GetSkill(3) - 8 == i) {
          equippedIconLv3[i].SetActive(true);
        } else {
          equippedIconLv3[i].SetActive(false);
        }
      } else {
        skillButtonsLv3[i].gameObject.SetActive(false);
      }
    }
  }


  int GetSkillsUnlockedInLevel(int level) {
    if(Persistence.GetInstance().GetVariableValue("isFreeplayMode") == 0) {
      // For regular visual novel play, number of skills comes from current save file variable "unlockedSkillsLevelX"
      return Persistence.GetInstance().GetVariableValue("unlockedSkillsLevel" + level.ToString());
    } else {
      // For freeplay mode, number of skills unlocked comes from an always-increasing, multi-save variable "permanentUnlockedSkillsLevelX"
      return PlayerPrefs.GetInt("permanentUnlockedSkillsLevel" + level.ToString());
    }
  }



  public void SelectSkill(int skillId) {
    selectedSkill = skillId;

    UnselectSkillButtons();

    if(skillId < 4) {
      skillButtonsLv1[skillId].interactable = false;
    } else if(skillId < 8) {
      skillButtonsLv2[skillId - 4].interactable = false;
    } else if(skillId < 12) {
      skillButtonsLv3[skillId - 8].interactable = false;
    }

    ShowSkillDescription();

    UpdateEquipSkillButton();
  }

  public void UpdateEquipSkillButton() {
    if(selectedSkill == -1) {
      equipSkillButton.gameObject.SetActive(false);
    } else {
      equipSkillButton.gameObject.SetActive(true);
    }

    if(IsSkillEquipped(selectedSkill)) {
      equipSkillButton.interactable = false;
      equipSkillButton.GetComponentInChildren<Text>().text = "Equipada";
    } else {
      equipSkillButton.interactable = true;
      equipSkillButton.GetComponentInChildren<Text>().text = "Equipar";
    }
  }

  public bool IsSkillEquipped(int skillId) {

    string skillVariable = GetSkillVariableString();

    if(skillId < 4) {
      return (GetSkill(1) == skillId);
    } else if(skillId < 8) {
      return (GetSkill(2) == skillId);
    }
    return (GetSkill(3) == skillId);
  }

  public void ShowSkillDescription() {
    skillDescription.text = GetSkillDescription(selectedSkill);
  }

  public void UpdateSkillNames(){
    int skillId = 0;
    foreach (Button button in skillButtonsLv1){
      button.GetComponentInChildren<Text>().text = GetSkillName(skillId);
      skillId++;
    }
    skillId++;
    foreach (Button button in skillButtonsLv2){
      button.GetComponentInChildren<Text>().text = GetSkillName(skillId);
      skillId++;
    }
    skillId++;
    foreach (Button button in skillButtonsLv3){
      button.GetComponentInChildren<Text>().text = GetSkillName(skillId);
      skillId++;
    }
  }


  public static string GetSkillDescription(int skillId) {
    switch(skillId) {
      case 0:
        return "Consome todas as Peças de um tipo aleatório (que vale pontos)";
      case 1:
        return "Sobe o Multiplicador de Foco em 2x";
      case 2:
        return "Transforma o seu Multiplicador de Foco em Turnos";

      case 4:
        return "Consome todas as Peças de Distração ganhando pontos";
      case 5:
        return "Transforma todas as Peças de Distração em Peças de Foco";
      case 6:
        return "Transforma todas as Peças de Distração em Peças de Turno";

      case 8:
        return "Consome todas as Peças de Pontuação";
      case 9:
        return "Permite realizar 5 jogadas sem gastar turno ou desencadear combinações";
      case 10:
        return "Permite realizar combinações sem gastar turno por 20 segundos, e distrações não causam dano";
    }
    return "";
  }

  public static string GetSkillName(int skillId) {
    switch(skillId) {
      case 0:
        return "Leitura Dinâmica";
      case 1:
        return "Concentração";
      case 2:
        return "Hora Extra";

      case 4:
        return "Zerar Distração";
      case 5:
        return "Mudança de Foco";
      case 6:
        return "Pesquisa de Campo";

      case 8:
        return "Ouvindo o Povo";
      case 9:
        return "Xeque-Mate";
      case 10:
        return "Combinações Intensas";
    }
    return "";
  }

  public void EquipSkill() {
    if (selectedSkill != -1){
      AudioController.GetInstance().PlayClickSound();

      if(selectedSkill < 4) {
        SetSkill(1, selectedSkill);
      } else if(selectedSkill < 8) {
        SetSkill(2, selectedSkill);
      } else if(selectedSkill < 12) {
        SetSkill(3, selectedSkill);
      }

      UpdateEquipSkillButton();
      UpdateSkillButtons();
    }


  }


  public void UnselectSkillButtons() {
    for(int i = 0; i < skillButtonsLv1.Length; i++) {
      skillButtonsLv1[i].interactable = true;
      skillButtonsLv2[i].interactable = true;
      skillButtonsLv3[i].interactable = true;
    }
  }

  private void SetSkill(int slot, int skillId){

    string skillVariable = GetSkillVariableString();

    if(Persistence.GetInstance().GetVariableValue("isFreeplayMode") == 0){
      Persistence.GetInstance().SetVariableValue(skillVariable + slot.ToString(), skillId);
    }else{
      PlayerPrefs.SetInt(skillVariable + slot.ToString(), skillId);
      PlayerPrefs.Save();
    }

  } 

  private int GetSkill(int slot){
    string skillVariable = GetSkillVariableString();

    if(Persistence.GetInstance().GetVariableValue("isFreeplayMode") == 0){
      return Persistence.GetInstance().GetVariableValue(skillVariable + slot.ToString());
    }else{
      string key = skillVariable + slot.ToString();
      if (PlayerPrefs.HasKey(key)){
        return PlayerPrefs.GetInt(key);
      } else{
        return -666;
      }
    }
  }

  private string GetSkillVariableString(){
    string skillVariable;
    if(Persistence.GetInstance().GetVariableValue("isFreeplayMode") == 0){
      skillVariable = "skillLevel"; // + some number from 1 to 3
    } else{
      skillVariable = "freeplaySkillLevel"; // + some number from 1 to 3
    }

    return skillVariable;
  }
    
}
