using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CheckpointBtn : MonoBehaviour {

	public int chapterIndex;
	public int checkpointIndex;
	public Sprite lockedSprite;
	public Sprite unlockedSprite;
	private bool locked = true;
	private Fade fade;

	void Start(){
		fade = GameObject.FindWithTag("Fade").GetComponent<Fade>();
		Feed();
	}

	void Feed(){

		string parentName = transform.parent.parent.name;
		chapterIndex = int.Parse(transform.parent.parent.name.Substring(parentName.Length - 1, 1));
		checkpointIndex = transform.GetSiblingIndex();
		Lock();
	}

	void Lock(){

		int chapterProgress = Persistence.GetChapterProgress(chapterIndex);
		if( Persistence.IsChapterAccessible(chapterIndex)==0 )
			Locked = true;
		else if(checkpointIndex > chapterProgress)
			Locked = true;
		else
			Locked = false;
	}

	public bool Locked {

		get {
			return locked;
		}
		set {
			locked = value;
			if(locked){
				transform.Find("Lock").GetComponent<Image>().enabled = true;
				transform.Find("Text").GetComponent<Text>().enabled = false;
			}
			else {
				transform.Find("Lock").GetComponent<Image>().enabled = false;
				transform.Find("Text").GetComponent<Text>().enabled = true;
			}
		}
	}

	public void Click(){
		if(!locked){
			IEnumerator fadeThenLoad = FadeThenLoad(chapterIndex, checkpointIndex);
			StartCoroutine(fadeThenLoad);
		}
	}

	IEnumerator FadeThenLoad(int chapter, int checkpoint){
		int fadeTime = 1;
		fade.FadeOut(fadeTime);
		yield return new WaitForSeconds(fadeTime + 0.5f);
		Persistence.Load(chapter, checkpoint);
	}
}
