﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Options : MonoBehaviour {

  public Slider musicSlider;
  public Slider sfxSlider;

  public bool canPlaySliderSound = false;

  public void Open() {
    LoadOptions();
  }

  public void LoadOptions() {
    musicSlider.value = GetMusicVolume();
    sfxSlider.value = GetSfxVolume();
    canPlaySliderSound = true;
  }



  public void SetMusicVolume(){
    if(canPlaySliderSound) {
      AudioController.GetInstance().PlaySlideSound();
    }

    PlayerPrefs.SetFloat("musicVolume", musicSlider.value);
    AudioController.GetInstance().UpdateMusicVolume(1f);
  }

  public void SetSfxVolume(){
    PlayerPrefs.SetFloat("sfxVolume", sfxSlider.value);
    AudioController.GetInstance().UpdateSfxVolume();
    if(canPlaySliderSound) {
      AudioController.GetInstance().PlaySlideSound();
    }
  }

  public void SetTextSpeed(){
    if(canPlaySliderSound) {
      AudioController.GetInstance().PlaySlideSound();
    }

    if(DialogBox.instance != null){
      DialogBox.instance.UpdateTextSpeed();
    }
  }

  public void ClickAchievementsButton(){
    AudioController.GetInstance().PlayClickSound();
    Social.ShowAchievementsUI();
  }


  public void ClickLeaderboardsButton(){
    AudioController.GetInstance().PlayClickSound();
    Social.ShowLeaderboardUI();
  }


  public static float GetMusicVolume(){
    return PlayerPrefs.GetFloat("musicVolume", 3f);
  }

  public static float GetSfxVolume(){
    return PlayerPrefs.GetFloat("sfxVolume", 3f);
  }
}
