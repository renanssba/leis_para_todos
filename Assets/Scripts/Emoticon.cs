﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Emoticon : MonoBehaviour {

	public bool autoFadeOut = true;
	private float fadeTime;
	private Image image;
	private Fade fade;

	public void Create(float lifespan, float fadeTime, Vector2 position, Sprite sprite){

		image = GetComponentInChildren<Image>();
		fade = GetComponent<Fade>();
		RectTransform rectTrans = GetComponent<RectTransform>();

		this.fadeTime = fadeTime;
		this.transform.SetParent(GameObject.FindWithTag("MainCanvas").transform);
		rectTrans.anchoredPosition = Camera.main.ViewportToWorldPoint(position);
		image.sprite = sprite;
		image.SetNativeSize();

		fade.FadeIn(fadeTime);
		if(autoFadeOut)
			Invoke("FadeOut", lifespan - fadeTime);
		print(lifespan);
		Destroy(this.gameObject, lifespan);
	}

	public void FadeOut(){
		fade.FadeOut(fadeTime);
	}
}
