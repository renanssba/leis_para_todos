﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DetailWindow : MonoBehaviour {

	public Text itemName;
	public Text description;
	public Text summary;
	public Image itemImage;

	public void SetDetailWindow(string new_name, string new_description, string[] summary, Sprite img){
		description.text = new_description;
		itemName.text = new_name;
		this.summary.text = "";
		for(int i = 0; i < summary.Length; i++) {
			if(i == 0){
				this.summary.text += summary[i];
			}
			else{
				this.summary.text += "\n" + summary[i];
			}

		}
		this.itemImage.sprite = img;
	}
}
