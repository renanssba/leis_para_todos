﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class Slot : MonoBehaviour {

	public Item item;
	public int index;
	public int wp = 0;
	private Image itemIcon;
	private Inventory inventory;
	public Vector3 scale;
	public Slot slot;
//	private EventTrigger eventTrigger = null;

	void Awake(){
		scale = GetComponent<RectTransform>().localScale;
	}
	void Start() {
		itemIcon = transform.Find("ItemIcon").GetComponent<Image>();
		inventory = GameObject.FindWithTag("MainCanvas").transform.Find("Inventory").GetComponent<Inventory>();
		UnityAction click =() => { OpenItem();};
		GetComponent<Button>().onClick.AddListener(click);
		slot = GetComponent<Slot> ();
	}

	void Update() {
		scale = GetComponent<RectTransform>().localScale;
		UpdateItemSprite();
		wp = ItemWaypoint;
	}
	
	public void OpenItem(){

		if(ItemName != null){
      AudioController.GetInstance().PlayClickSound();
			inventory.ShowDetailWindow(ItemName, ItemDescription, ItemSummary, ItemImage);
			inventory.current_item = index;
		}else{
      AudioController.GetInstance().PlayCancelSound();
		}
	}

	void UpdateItemSprite(){

		if(item != null){
			itemIcon.sprite = item.image;
			itemIcon.enabled = true;
		}
		else{
			itemIcon.enabled = false;
		}
	}

	public string ItemName{
		get {
			return item!=null ? item.name : null;
		}
	}

	public string ItemDescription{
		get {
			return item!=null ? item.description : null;
		}
	}
	public string[] ItemSummary{
		get {
			return item!=null ? item.summary : null;
		}
	}

	public Sprite ItemImage{
		get {
			return item!=null ? item.image : null;
		}
	}

	public int ItemWaypoint{
		get {
			return item!=null ? item.waypoint : 0;
		}
	}
}
