using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Inventory : MonoBehaviour {

	public int current_item;
	public IList<Item> itemList = new List<Item>();
	public bool inventoryActive;
	public Vector2 dimensions;
	public Vector2 slotSpacing;
	public GameObject slotPrefab;
	public GameObject slotsObj;
	public GameObject inventoryWindow;
	public DetailWindow detailWindow;
	private IList<Slot> slotList = new List<Slot>();
	private GameObject optionsOpen;

	void Awake() {
//		optionsOpen = transform.Find("OpenConfigButton").gameObject;
		CreateSlots();
	}

	public void ArrowLeft(){
		Item i;

		if(current_item!=0){
			current_item -=1;
		}else{
			current_item = itemList.Count-1;
		}
		i = itemList[current_item];

		ShowDetailWindow(i.name, i.description, i.summary, i.image);
	}
	
	public void ArrowRight(){
		Item i;
		
		if(current_item!=itemList.Count-1){
			i = itemList[current_item+1];
			current_item +=1;
		}else{
			i = itemList[0];
			current_item = 0;
		}
		
		ShowDetailWindow(i.name, i.description, i.summary, i.image);
	}

	public bool IsActive(){
		if(inventoryWindow.activeSelf || detailWindow.gameObject.activeSelf){
			inventoryActive = true;
			return true;
		}
		else {
			inventoryActive = false;
			return false;
		}
	}

	public bool AddItem(Item item){

		int itemCount = itemList.Count;
		int slotCount = slotList.Count;

		if(itemCount <= slotCount){
			if( !HasItem(item.name) ){
				itemList.Insert(itemCount, item);
				slotList[itemCount].item = item;
				slotList[itemCount].index = itemCount;
				return true;
			}
			else{
//				//Debug.log("ERRO: Item '" + item.name + "' ja existe!");
				return false;
			}
		}
		else{
//			//Debug.log("ERRO: Inventario Cheio!");
			return false;
		}
	}

	public void RemoveItem(Item item){

		int i = itemList.IndexOf(item);
		itemList.Remove(item);
		slotList[i].item = null;
	}

	public void RemoveAllItems(){

		itemList.Clear();
		for(int i = 0; i < slotList.Count; i++){
			slotList[i].item = null;
		}
	}

	public bool HasItem(string newItem){

		foreach(Item item in itemList) {
			if( newItem == item.name )
				return true;
		}
		return false;
	}

	public void ShowDetailWindow(string name, string description, string[] summary, Sprite img){
		detailWindow.SetDetailWindow(name, description, summary, img);
		detailWindow.gameObject.SetActive(true);
		HideInventoryWindow();
	}

	public void HideDetailWindow(){
		ShowInventoryWindow();
		detailWindow.gameObject.SetActive(false);
	}

	public void ShowInventoryWindow(){
//		optionsOpen.SetActive(true);
		inventoryWindow.SetActive(true);
	}

	public void HideInventoryWindow(){
//		optionsOpen.SetActive(false);
		inventoryWindow.SetActive(false);
	}

	void CreateSlots(){
		for(int i = 0; i < dimensions.y; i++){
			for(int j = 0; j < dimensions.x; j++){
				GameObject newSlot = (GameObject)Instantiate(slotPrefab);
				newSlot.transform.SetParent(slotsObj.transform, false);
				newSlot.transform.localPosition = SlotPosition(i,j);
				newSlot.name = "Slot " + i + "." + j;
			}
		}

		foreach(Transform child in slotsObj.transform){
			slotList.Add(child.GetComponent<Slot>());
		}
	}

	public Vector3 SlotPosition(int i, int j){

		Vector3 initialPos = new Vector3(-355f, 224.4f, 0);
		Vector3 offsetX = new Vector3(slotSpacing.x, 0, 0);
		Vector3 offsetY = new Vector3(0, -(slotSpacing.y), 0);
		Vector3 pos;
		pos = initialPos + offsetX * j + offsetY * i;
		return pos;
	}
}
