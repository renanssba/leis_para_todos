﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TitleBgScroller : MonoBehaviour {

	public Sprite[] Bg;
	public float animate_time =3f;
	public float change_bg_time =3f;
	public float fade_time =2f;
	public Fade fade;
	public float dx = 56;
	private bool fading =false;
	private int index;
	private float clock;
	private Image img;

	void Start () {
		index=0;
		img = GetComponent<Image>();
		StartCoroutine("AnimateBg");
		clock =0f;

		// initialize bg position
		ResetPosition();
	}

	void ResetPosition(){
		img.rectTransform.anchoredPosition = new Vector2(dx, img.rectTransform.anchoredPosition.y);
	}

	void Update () {
//		//Debug.log("anchored rect x: "+img.rectTransform.anchoredPosition.x);
//		//Debug.log("transform x: "+transform.position.x);

		if(clock<animate_time){
			clock +=Time.deltaTime;
			img.rectTransform.anchoredPosition = new Vector2(img.rectTransform.anchoredPosition.x -dx*2*Time.deltaTime/animate_time,
			                                                 img.rectTransform.anchoredPosition.y);
		}else{
			img.rectTransform.anchoredPosition = new Vector2(-dx, img.rectTransform.anchoredPosition.y);
		}

		if(clock>=animate_time-fade_time && !fading){
			fade.FadeOut(fade_time);
			fading = true;
		}
	}

	IEnumerator AnimateBg(){
		while(true){
			yield return new WaitForSeconds(change_bg_time);
			index+=1;
			if(index>=Bg.Length){
				index=0;
			}

			img.sprite = Bg[index];
			ResetPosition();
			clock=0;
			fade.FadeIn(fade_time);
			fading = false;
		}
	}
}
