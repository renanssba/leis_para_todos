using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class ButtonClick : MonoBehaviour {

	public int wpToGo;
	public GameController gameController;

	void Start(){
		UnityAction click =() => { GoToLine();};
		GetComponent<Button>().onClick.AddListener(click);
	}

	void GoToLine(){
		if(wpToGo > 0){
			if(gameController.SkipDialog())
				return;
			else {
        ScriptReader.GetInstance().GoToLine(wpToGo);
        gameController.gameState = GameController.GameState.PlayingScript;
			}
		}
	}
}