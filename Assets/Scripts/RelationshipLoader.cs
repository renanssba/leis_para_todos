﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RelationshipLoader : MonoBehaviour {

  public Slider[] relationshipSlider;
  public Image[] relationshipSliderFill;
  public Text[] levelText;

	public void LoadRelationships(){
		string[] strings = new string[4];

		strings[0] = "PDN";
		strings[1] = "PN";
		strings[2] = "PPC";
		strings[3] = "PSH";

    for(int i=0; i<relationshipSlider.Length; i++){
			int val = Persistence.GetInstance().GetVariableValue(strings[i]);
      SetBar(i, levelText[i], val);
		}
	}

  void SetBar(int relationId, Text levelText, int value){
    int levelcost = 40;
    int currentLevel = 0;

    while(value >= levelcost){
      currentLevel++;
      value -= levelcost;
    };

    if(currentLevel < 4){
      relationshipSlider[relationId].value = (float)value/(float)levelcost;
      if(currentLevel == 0) {
        levelText.text = "+0%";
      }else{
        levelText.text = "+" + currentLevel.ToString() + "0%";
      }
    }else{
      relationshipSlider[relationId].value = 1f;
      levelText.text = "+50%";
    }

    if(value == 0) {
      relationshipSliderFill[relationId].gameObject.SetActive(false);
    } else {
      relationshipSliderFill[relationId].gameObject.SetActive(true);
    }
	}
}
