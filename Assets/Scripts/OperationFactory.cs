﻿using UnityEngine;
using System.Collections;

public class OperationFactory {

	public bool Run(int operand1, string operatorVal, int operand2){

		switch(operatorVal) {
			case "==": return operand1 == operand2;
			case ">": return operand1 > operand2;
			case "<": return operand1 < operand2;
			case "<=": return operand1 <= operand2;
			case ">=": return operand1 >= operand2;
			case "!=": return operand1 != operand2;
			default: throw new UnityException("Wrong Operator");
		}
	}
}
