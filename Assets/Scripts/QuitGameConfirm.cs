﻿using UnityEngine;
using System.Collections;

public class QuitGameConfirm : MonoBehaviour {

	public GameObject confirmWindow;

	public void Open(){
    AudioController.GetInstance().PlayClickSound();
		confirmWindow.SetActive(true);
	}
	
	public 	void Close(){
		confirmWindow.SetActive(false);
	}
}
