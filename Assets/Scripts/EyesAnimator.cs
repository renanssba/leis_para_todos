﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EyesAnimator : MonoBehaviour {

  public Sprite[] anim;
  public float clock;
  
  void Start () {
    ResetAnim();
  }
  
  void Update () {

    if(clock > 0)
      clock -= Time.deltaTime;
    else {
      clock = 100f; // to wait the blink function execution
      StartCoroutine(Blink());
    }
  }
  
  IEnumerator Blink(){
    GetComponent<Image>().sprite = anim[0];
    yield return new WaitForSeconds(GetBlinkTime());
    GetComponent<Image>().sprite = anim[1];

    if(Random.Range(0, 100) < 15){
      yield return new WaitForSeconds(GetBlinkTime());
      GetComponent<Image>().sprite = anim[0];
      yield return new WaitForSeconds(GetBlinkTime());
      GetComponent<Image>().sprite = anim[1];
    }

    ResetAnim();
  }

  float GetBlinkTime(){
//    return 0.15f;
    return Random.Range(0.08f, 0.14f);
  }

  
  public void SetAnim(Sprite[] new_anim){
    anim = new_anim;

    if(anim != null){
      GetComponent<Image>().sprite = anim[1];
      gameObject.SetActive(true);
    }else{
      gameObject.SetActive(false);
    }
  }
  
  public void StartAnim(){
    
    ResetAnim();
    
    if(anim!=null){
      GetComponent<Image>().sprite = anim[1];
      //      GetComponent<Image>().SetNativeSize();
    }
  }
  
  public void StopAnim(){
    
    if(anim!=null){
      GetComponent<Image>().sprite = anim[1];
//      GetComponent<Image>().SetNativeSize();
    }
  }
  
  void ResetAnim(){
    clock = Random.Range(3f, 5f);
  }
}
