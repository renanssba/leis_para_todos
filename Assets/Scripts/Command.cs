﻿using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using FMODUnity;
using System.Text.RegularExpressions;
using DG.Tweening;

public class Command : MonoBehaviour {

  public static Command instance;

  public Dictionary<string, int> waypoints = new Dictionary<string, int>();
  public int lineCount;

  public GameController game;
  public DialogScreen screen;
  public Inventory inventory;
  public Fade fade;
  public GameObject inventoryButton;
  public TopicLists topics;
  public Text letreiroText;
  public Image flashImage;

  void Awake() {
    game.lineCount = lineCount;
    instance = this;
  }

  public static Command GetInstance() {
    return instance;
  }


  public bool CommandBreaksReading(string line) {
    string command = GetCommand(line);

    if(command == "wait" || command == "say" || command == "choices" ||
      command == "collect" || command == "fade_out" || command == "fade_in" || command == "minigame_start" ||
      command == "to_next_chapter" || command == "to_start_menu" || command == "lettering" ||
       command == "topic_new" || command == "add_relationship" || command == "open_url" )
      return true;
    else
      return false;
  }

  public void CheckCommand(string line, int lineNumber) {
		
    //Debug.log("line " + lineNumber+": "+line );
    string command = GetCommand(line);
    string[] param = GetParams(line);
    ScriptReader.GetInstance().GoToLine(lineNumber);
		
    switch(command) {
      case "analytics_event":
        Analytics_Event_Command(param);
        break;
      case "player_prefs":
        Player_Prefs_Command(param);
        break;
      case "wait":
        Wait_Command(param);
        break;
      case "character":
        Character_Command(param);
        break;
      case "character_reset_all":
        Character_Reset_All_Command();
        break;
      case "char_mood":
        Char_Mood_Command(param);
        break;
      case "char_base":
        Char_Base_Command(param);
        break;
      case "bg":
        Bg_Command(param);
        break;
      case "fg":
        Fg_Command(param);
        break;
      case "move_x":
        Move_x_Command(param);
        break;
      case "move_y":
        Move_y_Command(param);
        break;
      case "anim_alpha":
        Anim_Alpha_Command(param);
        break;
      case "anim_scale":
        Anim_Scale_Command(param);
        break;
      case "music":
        Music_Command(param);
        break;
      case "music_arg":
        Music_Arg_Command(param);
        break;
      case "ambience":
        Ambience_Command(param);
        break;
      case "fade_out_music":
        Fade_Out_Music_Command(param);
        break;
      case "sfx":
        Sfx_Command(param);
        break;
      case "minigame_start":
        Minigame_Start_Command(param);
        break;
      case "minigame_enable_skill":
        Minigame_Enable_Skill_Command();
        break;
      case "say":
        Say_Command(param);
        break;
      case "say_sfx":
        Say_Sfx_Command(param);
        break;
      case "say_pitch":
        Say_Pitch_Command(param);
        break;
      case "question":
        Question_Command(param);
        break;
      case "choices":
        Choices_Command(param);
        break;
      case "item":
        Item_Command(param);
        break;
      case "topic_new":
        Topic_New_Command(param);
        break;
      case "topic_relevance":
        Topic_Relevance_Command(param);
        break;
      case "topic_title":
        Topic_Title_Command(param);
        break;
      case "topic_debug":
        topics.DebugPrint();
        break;
      case "inventory":
        Inventory_Command(param);
        break;
      case "inventory_clean":
        Inventory_Clean_Command();
        break;
      case "goto":
        Goto_Command(param);
        break;
      case "waypoint":
        break;
      case "evidence":
        Evidence_Command(param);
        break;
      case "collect":
        Collect_Command(param);
        break;
      case "screenshake":
        Screenshake_Command(param);
        break;
      case "hype_show":
        Hype_Show_Command(param);
        break;
      case "hype_increase":
        Hype_Increase_Command(param);
        break;
      case "calendar_show":
        Calendar_Show_Command(param);
        break;
      case "calendar_change":
        Calendar_Change_Command(param);
        break;
      case "fade_in":
        Fade_In_Command(param);
        break;
      case "fade_out":
        Fade_Out_Command(param);
        break;
      case "mirror":
        Mirror_Command(param);
        break;
      case "inventory_btn":
        Inventory_Btn_Command(param);
        break;
      case "emoticon":
        Emoticon_Command(param);
        break;
      case "add_relationship":
        AddRelationship_Command(param);
        break;
      case "add_var":
        Add_Var_Command(param);
        break;
      case "if":
        If_Command(param);
        break;
      case "set_var":
        Set_Var_Command(param);
        break;
      case "savepoint":
        SavePoint_Command(param);
        break;
      case "set_number_victories":
        Set_Number_Victories(param);
        break;
      case "checkpoint":
        Checkpoint_Command(param);
        break;
      case "to_next_chapter":
        To_Next_Chapter_Command();
        break;
      case "to_start_menu":
        To_Start_Menu_Command();
        break;
      case "lettering":
        Lettering_Command(param);
        break;
      case "flash":
        Flash_Command(param);
        break;
      case "unlock_achievement":
        Unlock_Achievement(param);
        break;
      case "else":
        Else_Command();
        break;
      case "permanent_unlock_skills":
        Permanent_Unlock_Skills(param);
        break;
      case "open_url":
        OpenUrl_Command(param);
        break;
      case "open_store":
        OpenStore_Command();
        break;
      case "unlock_freeplay":
        Unlock_Freeplay_Command();
        break;
      case "unlock_skills":
        Unlock_Skills_Command(param);
        break;
      default:
			//Debug.log("WRONG COMMAND: " + command);
        break;
    }
  }

  void Flash_Command(string[] param) {
    float flashDuration = 0.5f;

    if(param.Length == 1) {
      flashDuration = float.Parse(param[0]);
    }

    flashImage.GetComponent<CanvasGroup>().alpha = 0.7f;
    flashImage.GetComponent<CanvasGroup>().DOFade(0f, flashDuration);
  }

  void Lettering_Command(string[] param) {
    // 0 - Text
    // 1 - Delay Time
    // 2 - Fade Time
    float fadeTime = 1.5f;
    float delayTime = 2f;

    if(param.Length == 3) {
      delayTime = float.Parse(param[1]);
      fadeTime = float.Parse(param[2]);
    } else if(param.Length == 2) {
      delayTime = float.Parse(param[1]);
    }

    string letteringText = param[0];

    letreiroText.text = letteringText;
    letreiroText.GetComponent<CanvasGroup>().DOFade(1f, fadeTime);
    letreiroText.GetComponent<CanvasGroup>().DOFade(0f, fadeTime).SetDelay(delayTime + fadeTime);

    string[] waitCommandArguments = new string[1];
    float totalWaitTime = fadeTime * 2 + delayTime;
    waitCommandArguments[0] = totalWaitTime.ToString();

    //FIXME DOESN'T SEEM TO WORK?
    Debug.Log("CALL WAIT COMMAND FOR " + waitCommandArguments[0]);
    Wait_Command(waitCommandArguments);
  }

  public static void Analytics_Event_Command(string[] param) {
    if(param.Length < 2) {
      //Debug.log("Error! Sent too few arguments to analytics command");
      return;
    }

    Analytics.CustomEvent("commandEvent", new Dictionary<string, object>{ { param[0], param[1] } });
  }

  public static void Player_Prefs_Command(string[] param) {
    PlayerPrefs.SetString(param[0], param[1]);
  }

  public void SavePoint_Command(string[] param) {		
		if (param.Length == 1){
			string savepointName = param[0];

			Persistence.SetLastSavepoint(savepointName);
    	Persistence.GetInstance().SaveGame();
			Debug.Log("Game SAVED");
		} else{
			Debug.LogError("Wrong use of savepoint, should've  been: savepoint {savepointName");
		}
  }

  public static void CheckAnimCommand(string line, int lineNumber) {
		
    string command = GetStaticCommand(line);
    string[] param = GetParams(line);
		
    switch(command) {
      case "create_mood":
        Create_Mood_Command(param);
        break;
      default:
        break;
    }
  }

  public void Unlock_Achievement(string[] args){
    string achievementName = args[0];

    AchievementsManager.UnlockAchievement(achievementName);
  }
    
  public void Permanent_Unlock_Skills(string[] args) {
    // Unlocks 1, 2 or all 3 skills from a given level. First argument is the level, second argument is the number
    // This differs from a simple set_var command because it can only go up.
    // So if you unlock 3 skills for level 2, even if you call this again to unlock only 2 skills from level 2,
    // it WON'T lower the number of skills from  3 to 2.
    int level = int.Parse(args[0]);
    int numberOfSkillsUnlocked = int.Parse(args[1]);

    string prefix = "permanentUnlockedSkillsLevel";

    int skillsUnlockedBefore = PlayerPrefs.GetInt(prefix + level.ToString());
    if (numberOfSkillsUnlocked >= skillsUnlockedBefore){
      Debug.Log("Unlocking " + numberOfSkillsUnlocked + " skills (from " + skillsUnlockedBefore + ") to level " + level);
      PlayerPrefs.SetInt(prefix + level.ToString(), numberOfSkillsUnlocked);
    }

  }

  static void OpenUrl_Command(string[] args){
    if(args[0] == "facebook"){
      Application.OpenURL("http://www.facebook.com/supernovaIndieGames");
      AchievementsManager.UnlockAchievement("Fã da Supernova");
    }else if (args[0] == "twitter"){
      Application.OpenURL("http://www.twitter.com/supernovaIG");
    }
  }

  void OpenStore_Command(){
    if(Application.platform == RuntimePlatform.Android) {
      Application.OpenURL("https://play.google.com/store/apps/details?id=com.SupernovaGames.LeisParaTodos");
    } else if(Application.platform == RuntimePlatform.IPhonePlayer) {
      Application.OpenURL("https://appsto.re/br/0ccrgb.i");
    } else {
      Application.OpenURL("https://supernova.games");
    }
  }

  void Unlock_Freeplay_Command(){
    PlayerPrefs.SetInt("isFreeplayUnlocked", 1);
  }

  void Unlock_Skills_Command(string[] param){
    int skillLevel = int.Parse(param[0]);
    int numberOfSkillsToUnlock = int.Parse(param[1]);

    /// unlock skills
    Persistence.GetInstance().SetVariableValue("hasSkills", 1);

    // setar a skill desse nivel para a default
    if( Persistence.GetInstance().GetVariableValue("unlockedSkillsLevel"+skillLevel) == 0 ){
      Persistence.GetInstance().SetVariableValue("skillLevel"+skillLevel, (skillLevel-1)*4);
    }
    Persistence.GetInstance().SetVariableValue("unlockedSkillsLevel"+skillLevel, numberOfSkillsToUnlock);


    /// unlock skill for freeplay mode
    if(PlayerPrefs.GetInt("permanentUnlockedSkillsLevel" + skillLevel) < numberOfSkillsToUnlock){
      // setar a skill desse nivel para a default
      if(PlayerPrefs.GetInt("permanentUnlockedSkillsLevel" + skillLevel) == 0){
        PlayerPrefs.SetInt("freeplaySkillLevel" + skillLevel, (skillLevel-1)*4);
      }
      PlayerPrefs.SetInt("permanentUnlockedSkillsLevel" + skillLevel, numberOfSkillsToUnlock);
    }
  }


  void Wait_Command(string[] args) {
    float time = float.Parse(args[0]);
    Debug.Log ("Wait called for " + time + " seconds");
    screen.EnableDialogBox(false);
    screen.choices.SetActive(false);
    game.WaitSeconds(time);
  }

  void Music_Command(string[] args) {
    AudioController.GetInstance().PlayMusic(args[0]);
  }

  void Music_Arg_Command(string[] args) {
    AudioController.GetInstance().SetMusicArgument(float.Parse(args[0]));
  }

  void Ambience_Command(string[] args) {
    AudioController.GetInstance().PlayAmbience(args[0], float.Parse(args[1]));
  }

  void Fade_Out_Music_Command(string[] args) {
    AudioController.GetInstance().FadeMusic(float.Parse(args[0]));
  }

  void Sfx_Command(string[] args) {
    AudioController.GetInstance().PlaySfx(args[0]);
  }

  void Minigame_Start_Command(string[] args) {
    /*
     * arg0 = chapter
     * arg1 = checkpoint_to_load
     * arg2 = hasSkills
     */
    //Analytics.CustomEvent("startMinigame", null);
    //Persistence.chapter_to_load = 1;
    MinigameData.chapter = int.Parse(args[0]);
    Persistence.checkpoint_to_load = int.Parse(args[1]);   

    SceneManager.LoadScene("Minigame");
  }

  void Minigame_Enable_Skill_Command() {
//    Debug.Log("%%%%Skills have been enabled%%%%");
    Persistence.GetInstance().SetVariableValue("hasSkills", 1);
  }

  void Bg_Command(string[] args) {
    screen.SetBg(args[0]);
    if(args.Length >= 2) {
      screen.SetFg(args[1]);
    }
  }

  void Fg_Command(string[] args) {
    screen.SetFg(args[0]);
  }

  void Character_Command(string[] args) {
    for(int i = 1; i < args.Length; i += 3) {
      int index = (int.Parse(args[i-1])) - 1;
      string name = args[i];
      float posY = float.Parse(args[i + 1]);

      screen.SetCharacter(index, args[i], posY );
    } 
  }

  void Character_Reset_All_Command() {
    string[] args = {"1", "none", "0",
      "2", "none", "0",
      "3", "none", "0",
      "4", "none", "0",
      "5", "none", "0",
      "6", "none", "0",
    };
    Character_Command(args);
  }

  void Char_Mood_Command(string[] param) {
    int charId = game.charList.GetCharIdByParam(param[0]);
    string moodName = param[1];

    screen.SetCharacterMood(charId, moodName);
  }

  void Char_Base_Command(string[] param) {
    int convertedInt = 0;
    string[] chars = new string[6];
    chars[0] = "none";
    chars[1] = "none";
    chars[2] = "none";
    chars[3] = "none";
    chars[4] = "none";
    chars[5] = "none";
    for(int i = 0; i < param.Length; i++) {
      if(i % 2 != 0) {
        convertedInt = game.charList.GetCharIdByParam(param[0]);
        chars[convertedInt] = param[i];
      } 
    } 
    screen.SetCharacterBaseSprite(chars);
  }

  void Say_Command(string[] args) {

    // apply special characters
    for(int i=0; i<args.Length; i++){
      args[i] = InterpretSpecialStrings(args[i]);
    }
		
    game.SetDialogState();
    if(args.Length > 1) {
      screen.SetDialog(args[0], args[1]);

      if((args[1])[0] != '(' && (args[1])[0] != '<')
        game.charList.MakeCharTalk(args[0]);
    } else {
      screen.SetDialog(args[0]);

      if((args[0])[0] != '(' && (args[0])[0] != '<')
        game.charList.MakeCharTalk(screen.GetTalkingCharName());
    }
  }

  string InterpretSpecialStrings(string initialString){
    string currentString = initialString;
    int i;

    if(!initialString.Contains("\\")){
      return initialString;
    }

    currentString = InterpretSpecialString(currentString, "\\n", "\n");
    if(Application.platform == RuntimePlatform.Android) {
      currentString = InterpretSpecialString(currentString, "\\store", "Google Play Store");
    } else if(Application.platform == RuntimePlatform.IPhonePlayer){
      currentString = InterpretSpecialString(currentString, "\\store", "App Store");
    } else {
      currentString = InterpretSpecialString(currentString, "\\store", "Store");
    }
    return currentString;
  }

  string InterpretSpecialString(string initialString, string specialString, string convertTo){
    while(true){
      int index = initialString.IndexOf(specialString);

      if(index == -1){
        break;
      }else{
        int stringSize = initialString.Length;

        if(stringSize > index + specialString.Length){
          initialString = initialString.Substring(0, index) + convertTo + initialString.Substring(index + specialString.Length, stringSize-index- specialString.Length);
        }else{
          initialString = initialString.Substring(0, index) + convertTo;
        }
      }
    };
    return initialString;
  }


  void Say_Sfx_Command(string[] param) {
//    string name;
//    int interval;
//
//    if(param.Length > 1) {
//      name = param[0];
//      interval = int.Parse(param[1]);
//      screen.SetDialogSfx(name);
//      screen.SetSfxInterval(interval);
//      return;
//    } else {
//      bool result = int.TryParse(param[0], out interval);
//      if(result) {
//        screen.SetSfxInterval(interval);
//        return;
//      } else {
//        name = param[0];
//        screen.SetDialogSfx(name);
//        return;
//      }
//    }
  }

  void Say_Pitch_Command(string[] param) {		
    for(int i = 0; i < param.Length; i += 2) {
			
      string name = param[i];
      float pitch = float.Parse(param[i + 1]);
    }
  }

  void Move_x_Command(string[] param) {
		
    int char_index = game.charList.GetCharIdByParam(param[0]);
    float anim_time = float.Parse(param[1]);
    float destination_x = float.Parse(param[2]);
		
    screen.SetMovement_x(char_index, anim_time, destination_x);
  }

  void Move_y_Command(string[] param) {
		
    int char_index = game.charList.GetCharIdByParam(param[0]);
    float anim_time = float.Parse(param[1]);
    float destination_y = float.Parse(param[2]);

    screen.SetMovement_y(char_index, anim_time, destination_y);
  }



  static void Create_Mood_Command(string[] param) {
    string moodName = param[0];
    string mouthAnim = param[1];
    string eyesAnim = param[2];
    string[] sprite_names = new string[param.Length - 1];
    int j = 1;

    for(int i = 0; i < sprite_names.Length; i++) {
      sprite_names[i] = param[j];
      j++;
    }
    CharacterAnimations.CreateMoodAnimation(moodName, mouthAnim, eyesAnim);
  }



  void Anim_Alpha_Command(string[] param) {

    int charIndex = game.charList.GetCharIdByParam(param[0]);
    float animTime = float.Parse(param[1]);
    float alpha = float.Parse(param[2]);
		
    screen.AnimateCharacterAlpha(charIndex, animTime, alpha);
  }

  void Anim_Scale_Command(string[] param) {

    int charIndex = game.charList.GetCharIdByParam(param[0]);
    float speed = float.Parse(param[1]);
    float scaleY = float.Parse(param[2]);

    screen.AnimateCharacterScale(charIndex, speed, scaleY);
  }

  void Question_Command(string[] args) {

    // apply special characters
    for(int i=0; i<args.Length; i++){
      args[i] = InterpretSpecialStrings(args[i]);
    }

		
    if(args.Length > 1) {
      screen.SetQuestion(args[0], args[1]);
			
      if((args[1])[0] != '(')
        game.charList.MakeCharTalk(screen.GetTalkingCharName());
    } else {
      screen.SetQuestion(args[0]);

      if(args[0] == null)
        return;
			
      if((args[0])[0] != '(')
        game.charList.MakeCharTalk(screen.GetTalkingCharName());
    }
  }

  void Choices_Command(string[] param) {
		
    string[] choiceText = new string[param.Length / 2];
    int[] choiceWaypoint = new int[param.Length / 2];
    string[] labelNames = new string[param.Length / 2];
    int tempIndex = 0;
		
    for(int i = 0; i < param.Length; i++) {
      if(i % 2 == 0) {
        choiceText[tempIndex] = param[i];
      } else {
        labelNames[tempIndex] = param[i];
        if(waypoints.ContainsKey(param[i])) {
          choiceWaypoint[tempIndex] = waypoints[param[i]];
        } else {
          //Debug.log("ERROR: " + "'" + param[i] + "'" + " is not a valid Waypoint. Choices_Command");
          choiceWaypoint[tempIndex] = 0;
        }
        tempIndex++;
      }
    }
    game.SetLabelNames(labelNames);
    screen.SetChoices(choiceText, choiceWaypoint);
  }

  void Item_Command(string[] param) {
		
    string name = param[0];
    string description = param[1];
    Sprite sprite = Resources.Load<Sprite>("Items/" + param[2]);
    int wp = 0;
    string[] summary = new string[param.Length - 3];
    int j = 3;

    for(int i = 0; i < summary.Length; i++) {
      summary[i] = param[j];
      j++;
    }

    if(inventory.AddItem(new Item(name, description, summary, sprite, wp)) && !Persistence.fastRead) {
      // sound effect
      AudioController.GetInstance().PlayItemSound();

      // informative message
      string msg = "<color=#836900FF>[Voce recebeu </color><color=#aa1100ff>" + name + "</color> <color=#836900FF>.]</color>";
      string[] info_msg = { "", msg };
      Say_Command(info_msg);
    }
  }

  void Inventory_Command(string[] param) {
		
    if(param[0].ToLower() == "true") {
      GameObject presentBtn = inventory.transform.Find("DetailWindow").Find("PresentBtn").gameObject;
      presentBtn.SetActive(true);
      game.WaitForItem();
      inventory.transform.Find("InventoryWindow").gameObject.SetActive(true);
    }
		
    int defaultWp;
    if(waypoints.ContainsKey(param[1])) {
      defaultWp = waypoints[param[1]];
    } else {
//			//Debug.log("ERROR: " + "'" + param[1] + "'" + " is not a valid Waypoint. Inventory_Command");
      return;
    }
		
    foreach(Item item in inventory.itemList) {
			
      item.waypoint = defaultWp;
			
      for(int i = 2; i < param.Length; i += 2) {
        if(param[i] == item.name) {
          if(waypoints.ContainsKey(param[i + 1])) {
            item.waypoint = waypoints[param[i + 1]];
          } else {
//						//Debug.log("ERROR: " + "'" + param[i+1] + "'" + " is not a valid Waypoint. Inventory_Command");
          }
        }
      }
    }
  }

  void Topic_New_Command(string[] param) {
    if(topics.AddTopicToList(param)) {
      // sound effect
      AudioController.GetInstance().PlayConfirmSound();
      
      // informative message
      string msg;
      if(param[0] == "favor") {
        msg = "<color=#836900FF>[NOVO ARGUMENTO! </color><color=#be1300ff>" + param[2] + "</color> <color=#836900FF>foi adicionado à lista de argumentos </color><color=#be1300ff>A FAVOR</color><color=#836900FF> do PL.]</color>";
      } else {
        msg = "<color=#836900FF>[NOVO ARGUMENTO! </color><color=#be1300ff>" + param[2] + "</color> <color=#836900FF>foi adicionado à lista de argumentos </color><color=#be1300ff>CONTRA</color><color=#836900FF> o PL.]</color>";
      }
      string[] info_msg = { "", msg };
      Say_Command(info_msg);
    }
//    else{
//      //Debug.logError("Error setting this new topic");
//    }
  }

  void Topic_Title_Command(string[] param) {
    topics.SetTopicsTitle(param[0]);
  }

  void Topic_Relevance_Command(string[] param) {
    if(param.Length < 3) {
      //Debug.logError("Script Error! Insuficient number of parameters");
      return;
    }

    if(param[0] != "favor" && param[0] != "contrario") {
      //Debug.logError("Script Error! "+param[0]+" is not a valid topic list");
      return;
    }

    topics.SetTopicRelevance(param);
  }

  void Inventory_Clean_Command() {
    inventory.RemoveAllItems();
  }

  void Inventory_Btn_Command(string[] param) {
		
    string value = param[0].ToLower();
		
    if(value == "on") {
      inventoryButton.SetActive(true);
    } else if(value == "off") {
      inventoryButton.SetActive(false);
    }
  }

  void GotoNextElse() {
    int lineNumber = ScriptReader.GetInstance().GetNextElseLine();
    ScriptReader.GetInstance().GoToLine(lineNumber);
  }

  void GotoNextEndif() {
    int lineNumber = ScriptReader.GetInstance().GetNextEndifLine();
    ScriptReader.GetInstance().GoToLine(lineNumber + 1);
  }


  public void Goto_Command(string[] label) {
    if(waypoints.ContainsKey(label[0])) {
      int lineNumber = waypoints[label[0]];
//      //Debug.log("The label " + label[0] + " has value: "+lineNumber);
      ScriptReader.GetInstance().GoToLine(lineNumber);
    }
  }

  void Evidence_Command(string[] param) {
    GameObject presentBtn = inventory.transform.Find("DetailWindow").Find("PresentBtn").gameObject;
		
    if(param[0].ToLower() == "on") {
      presentBtn.SetActive(true);
    } else if(param[0].ToLower() == "off") {
      presentBtn.SetActive(false);
    }

  }

  void Collect_Command(string[] param) {
    int id = int.Parse(param[0]);
    int backWp = waypoints[param[1]];
    int[] collectibleWp = new int[param.Length - 2];
		
    int k = 0;
    for(int i = 2; i < param.Length; i++) {
      collectibleWp[k] = waypoints[param[i]];
      k++;
    }
    game.SetCollectionState();
    screen.SetCollect(id, backWp, collectibleWp);
  }

  void Screenshake_Command(string[] param) {
    Debug.Log("Screenshake happening");
		
    if(param.Length != 1)
      Debug.Log("Incorrect use of Screenshake. Use Screenshake {x, where x is the intensity from 1 to 10");
    else {
      int amount = int.Parse(param[0]);
      if(amount >= 1 && amount <= 10)
        screen.Screenshake(amount);
    }
  }

  void Hype_Show_Command(string[] param) {
    string show = param[0].ToLower();
    if(show == "on") {
      Persistence.GetInstance().SetVariableValue("hype_discurso", 0);
      screen.UpdateHypeBar();
      screen.ShowHype();
    } else if(show == "off") {
      screen.HideHype();
//      Persistence.GetInstance().SetVariableValue("hype_discurso", Persistence.GetInstance().GetVariableValue("influencia_politica") * 5);
    }
  }

  void Hype_Increase_Command(string[] param) {
    int addedValue = int.Parse(param[0]);
    int previousValue = Persistence.GetInstance().GetVariableValue("hype_discurso");

    Persistence.GetInstance().SetVariableValue("hype_discurso", previousValue + addedValue);

    screen.UpdateHypeBar();
    UpdateMusicArgument();
  }

  void UpdateMusicArgument() {
    string[] musicArgs = new string[1];
    string[] ambienceArgs = new string[2];
    int hypeLevels = Persistence.GetInstance().GetVariableValue("hype_discurso");
    ambienceArgs[0] = "congresso_amb";

    if(hypeLevels >= 70) {
      musicArgs[0] = "3";
    } else if(hypeLevels >= 40) {
      musicArgs[0] = "2";
    } else {
      musicArgs[0] = "1";
    }
    ambienceArgs[1] = (Mathf.Max((float)hypeLevels - 20f, 0f) / 100f).ToString();
    //Debug.log("hype level: " + screen.hypeLevels + ", ambience value: " + ambienceArgs[1]);

    Music_Arg_Command(musicArgs);
    Ambience_Command(ambienceArgs);
  }

  void Calendar_Show_Command(string[] param) {
    string show = param[0].ToLower();
    if(show == "on") {
      screen.ShowCalendar(true);
    } else if(show == "off") {
      screen.ShowCalendar(false);
    }
  }

  void Calendar_Change_Command(string[] param) {
//		int first = int.Parse(param [0]);
//		int second = int.Parse(param [1]);
    screen.ChangeCalendar(param[0], param[1]);
  }

  public void Fade_In_Command(string[] anim_time) {
    fade.FadeIn(float.Parse(anim_time[0]));
    Wait_Command(anim_time);
  }

  public void Fade_Out_Command(string[] anim_time) {
    fade.FadeOut(float.Parse(anim_time[0]));
    Wait_Command(anim_time);
  }

  void Mirror_Command(string[] param) {
    int char_index = game.charList.GetCharIdByParam(param[0]);
    screen.MirrorCharacter(char_index);
  }

  void Emoticon_Command(string[] param) {
    string filename = param[0];
    Vector2 position = new Vector2(float.Parse(param[1]), float.Parse(param[2]));
    float lifespan = float.Parse(param[3]);
    screen.ShowEmoticon(filename, position, lifespan);
  }

  void If_Command(string[] param) {
    int firstOperand = GetValueFromParameter(param[0]);
    string operatorName = param[1];
    int secondOperand = GetValueFromParameter(param[2]);

    if (param.Length >= 4){
      LegacyVsnIf(param);
      return;
		}


    OperationFactory operation = new OperationFactory();
    if(operation.Run(firstOperand, operatorName, secondOperand) == false) {				
			// New VSN If
			if (param.Length <= 3){
				GotoNextElse();
			}
		}
  }


  void LegacyVsnIf(string[] param){
    int firstOperand = GetValueFromParameter(param[0]);
    string operatorName = param[1];
    int secondOperand = GetValueFromParameter(param[2]);

    string[] waypointName = new string[1];
    waypointName[0] = param[3];


    OperationFactory operation = new OperationFactory();
    if(operation.Run(firstOperand, operatorName, secondOperand) == true) {       
      Goto_Command(waypointName);
    }
  }  


  void Else_Command() {
    GotoNextEndif();
  }

  void AddRelationship_Command(string[] param) {
    string partyName = param[0];
    int valueToAdd = int.Parse(param[1]);
    string[] say_args = new string[2];

		switch(partyName.ToUpper()) {
      case "PDN":
      case "PN":
      case "PPC":
			case "PSH":
				break;
      default:
        Debug.LogError("Wrong use of command add_relationship. Use the arguments PDN, PN, PPC or PHS");
        return;
    }

    Persistence.GetInstance().SetVariableValue(partyName, Persistence.GetInstance().GetVariableValue(partyName) + valueToAdd);

    AudioController.GetInstance().PlayConfirmSound();
    say_args[0] = "";
    if(valueToAdd >= 20) {
      say_args[1] = "<color=#836900FF>[O seu relacionamento com o </color><color=#aa1100ff>" + partyName + "</color><color=#836900FF> melhorou bastante!]</color>";
    } else {
      say_args[1] = "<color=#836900FF>[O seu relacionamento com o </color><color=#aa1100ff>" + partyName + "</color><color=#836900FF> melhorou um pouco.]</color>";
    }
    CheckToGetRelationshipAchievement();
    Say_Command(say_args);
  }

  void CheckToGetRelationshipAchievement(){
    int relationshipSum =0;
    relationshipSum += Persistence.GetInstance().GetVariableValue("PDN");
    relationshipSum += Persistence.GetInstance().GetVariableValue("PN");
    relationshipSum += Persistence.GetInstance().GetVariableValue("PPC");
    relationshipSum += Persistence.GetInstance().GetVariableValue("PSH");

    if(relationshipSum >= 640){
      AchievementsManager.UnlockAchievement("Diplomacia");
    }
  }



  void Set_Number_Victories(string[] param) {
    string key = param[0];

    Persistence.GetInstance().SetVariableValue(key, Persistence.matchVictories);
  }

  void Set_Var_Command(string[] param) {
    string key = param[0];

    Persistence.GetInstance().SetVariableValue(key, GetValueFromParameter(param[1]));
  }

  void Add_Var_Command(string[] param) {
    string key = param[0];

    Persistence.GetInstance().SetVariableValue(key, Persistence.GetInstance().GetVariableValue(key) + GetValueFromParameter(param[1]));
  }

  int GetValueFromParameter(string param){
    int value = 0;

    if(int.TryParse(param, out value)) {
      return value;
    } else if(param[0] == '#'){
      return InterpretSpecialOperand(param);
    } else {
      return Persistence.GetInstance().GetVariableValue(param);
    }
    return 0;
  }

  int InterpretSpecialOperand(string param){
    switch(param){
      case "#isFreeplayUnlocked":
        return PlayerPrefs.GetInt("isFreeplayUnlocked", 0);
    }
    return 0;
  }

  void Checkpoint_Command(string[] param) {
    int chapter = game.chapterId;
    int checkpoint = int.Parse(param[0]);
    string msg;

    Resources.UnloadUnusedAssets();

    // sound effect
    AudioController.GetInstance().PlayConfirmSound();

    // informative message
    if(Persistence.ReachCheckpoint(chapter, checkpoint)) {
      msg = "<color=#836900FF>[</color><color=#aa1100ff>O SEU PROGRESSO ESTÁ SALVO!</color><color=#836900FF> Você pode continuar o jogo a partir deste ponto através do Menu Inicial.]</color>";
    } else {
      msg = "<color=#836900FF>[</color><color=#aa1100ff>PONTO DE RETORNO ALCANÇADO!</color><color=#836900FF> Você pode continuar o jogo a partir deste ponto através do Menu Inicial.]</color>";
    }
    string[] info_msg = { "", msg };
    Say_Command(info_msg);
  }

  void To_Next_Chapter_Command() {
    game.AdvanceChapter();
    game.WaitSeconds(3f);
  }

  void To_Start_Menu_Command() {
//    Analytics.CustomEvent("finishedGame", null);
    game.quit.ToTitleScreen();
    game.WaitSeconds(3f);
  }

  public static string GetStaticCommand(string line) {
    int start = line.IndexOf("@");
    int end = line.IndexOf(" ", start + 1);
    if(end <= 0)
      end = line.Length - 1;
    else
      end--;
    string command = line.Substring(start + 1, end - start);
    return command.ToLower();
  }

  public static string GetCommand(string line) {    

    int end = line.IndexOf(" ", 0);
    line = Regex.Match(line, "[a-zA-Z_]+").Value;
    //Debug.Log(line);
    if(end <= 0) {
      end = line.Length;
    }
    return line.ToLower();

//    int end = line.IndexOf(" ", 0);
//    if(end <= 0) {
//      end = line.Length;
//    }
//    return line.Substring(0, end).ToLower();

  }

  public static string[] GetParams(string line) {

    List<string> arrayStrings = new List<string>();

    Match match = Regex.Match(line, @"({[^{]*)+");


    foreach(Capture c in match.Groups[1].Captures) {
      string value = c.Value;

      value = Regex.Replace(value, "^{", "");
      value = value.Trim();


      arrayStrings.Add(value);
    }
      




    return arrayStrings.ToArray();
//    
//    IList<int> foundIndexes = new List<int>();
//		
//    for(int i = line.IndexOf("{"); i > -1; i = line.IndexOf("{", i + 1)) {
//      foundIndexes.Add(i);
//    }
//    string[] paramArray = new string[foundIndexes.Count];
//		
//    int j = 0;
//    char[] tempChars = new char[2];
//    tempChars[0] = '>';
//    tempChars[1] = '<';
//    foreach(int i in foundIndexes) {
//      string param = "";
//      string thisChar = line.Substring(i, 1);
//			
//      if(thisChar == "{") {
//        int textStart = i;
//        int textEnd = line.IndexOf('{', textStart + 1);
//        if(textEnd <= 0)
//          textEnd = line.Length - 1;
//        else
//          textEnd -= 2;
//        param = line.Substring(textStart + 1, textEnd - textStart);
//      } else if(thisChar == "#") {
//        int paramStart = i;
//        int paramEnd = line.IndexOf(" ", paramStart + 1);
//        if(paramEnd <= 0)
//          paramEnd = line.Length - 1;
//        else
//          paramEnd -= 2;
//        param = line.Substring(paramStart + 1, paramEnd - paramStart);
//      }
//      paramArray[j] = param;
//      j++;
//    }
//    return paramArray;

  }
}
