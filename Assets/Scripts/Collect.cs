using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Collect : MonoBehaviour {

	public GameObject[] collectList = new GameObject[1];
	public int id;
	public Sprite bg;
	public int backWp { get; set;}
	private GameObject collectibleList;
	private GameController game;

	public void Create(int id, int backWp, int[] collectibleWp, Transform parent){

		transform.SetParent(parent, false);
		game = GameObject.FindWithTag("GameController").GetComponent<GameController>();
		collectibleList = transform.Find("CollectibleList").gameObject;
		this.backWp = backWp;
		this.id = id;
		name = "Collect_" + id;
		AssignWaypoints(collectibleWp);
	}

	void AssignWaypoints(int[] wp){	

		IList<Collectible> collectible = new List<Collectible>();
		foreach(Transform child in collectibleList.transform) {
			collectible.Add(child.GetComponent<Collectible>());
		}
		for(int i = 0; i < collectible.Count; i++){
			collectible[i].waypoint = wp[i];
		}
	}

	public void BackBtn(){
		game.GoToSelectedWaypoint(backWp);
	}
}
