using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.UI;


public class Collectible : MonoBehaviour {

	public int waypoint = -1;
	public GameController game;

	void Awake() {
		game = GameObject.FindWithTag("GameController").GetComponent<GameController>();
		UnityAction click =() => { PickThis();};
		GetComponent<Button>().onClick.AddListener(click);
	}

	void PickThis(){
		if(waypoint > -1)
			SelectObject(waypoint);
	}
	public void SelectObject(int wp){
		game.GoToSelectedWaypoint(wp);
	}
}
