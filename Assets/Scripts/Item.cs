﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Item {

	public string name;
	public string description;
	public string[] summary;
	public Sprite image;
	public int waypoint;

	public Item(string name, string description, string[] summary, Sprite image, int waypoint){
		this.name = name;
		this.description = description;
		this.summary = summary;
		this.image = image;
		this.waypoint = waypoint;
	}
}