﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System.Reflection;

public class ScriptReader : MonoBehaviour {
	
  private static ScriptReader instance;

	public Command commandController;
	public string[] script;
	public int[] checkpoints = new int[14];
  public int currentLine = -1;

  private TextAsset currentScript;
  private int totalLines;

  public void Awake(){
    instance = this;
  }

  public static ScriptReader GetInstance(){
    return instance;
  }

	public void GoToSavepoint(string savepointName){
    for(int i = 0; i < script.Length; i++){
			string line = script[i];
			
      if(line != null){
				if(line.Contains("savepoint") && line.Contains("{" + savepointName)){
					GoToLine(i+1);
					return;
				}
			}
		}
	}

	public void SetCurrentScript(TextAsset newScript){
		currentScript = newScript;
	}

	public void LoadScript(int startingCheckpoint){
    //Debug.log("Load Script");
    //Debug.log("Chapter: "+Persistence.chapter_to_load+", Checkpoint: " +Persistence.checkpoint_to_load);

		Persistence.fastRead = true;
   	commandController.waypoints = StoreWaypointsAndLoadInventoryAndCountLines(startingCheckpoint);
    CreateScriptArray();
    Persistence.fastRead = false;
		
    // initialize character animations
		//CreateCharacterAnims();
  }

  public void GoToLine(int line) {
    currentLine = line;
  }

  public int GetNextElseLine(){
    for (int i = currentLine+1 ; i < script.Length ; i++){
      string line = script [i];
      if (Command.GetCommand(line) == "else" ||
          Command.GetCommand(line) == "endif"){
        return i;
      }
    }

    return -1;
  }

  public int GetNextEndifLine(){
    for (int i = currentLine+1 ; i < script.Length ; i++){
      string line = script [i];
      if (Command.GetCommand(line) == "endif"){
        return i;
      }   
    }

    return -1;
  }

	public void ReadScript(){

//    Debug.Log("Called ReadScript with line: " + startingLine);
    currentLine++;

    while(currentLine < script.Length){
      string line = script[currentLine];

      if( line.Length == 0 ){
        goto nextIteration;
      }

      if( line[0] == '/' || line[0] == '*' ){
        goto nextIteration;
      }

      commandController.CheckCommand(line, currentLine);
      if(commandController.CommandBreaksReading(line) == true){
        return;
      }

      nextIteration:
      currentLine++;
		}
	}

	void CreateScriptArray(){

		TextReader reader;
    if(Persistence.debugMode){
			FileStream scriptFile = new FileStream("script_debug.txt", FileMode.Open, FileAccess.Read);
			reader = new StreamReader(scriptFile);
		}else{
			reader = new StringReader(currentScript.text);
		}

    if(reader==null){
			Debug.Log("Error loading Script file");
    }


		script = new string[totalLines + 1];
		for(int i = 1; i < script.Length; i++){
      string line = reader.ReadLine ();
      if (line != null && line.StartsWith("\t"))
        line = line.Trim ();
      
      script [i] = line;
		}

		reader.Close();
	}

	Dictionary<string, int> StoreWaypointsAndLoadInventoryAndCountLines(int startingCheckpoint){
		Dictionary<string, int> waypoints = new Dictionary<string, int>();
		int lineCount = 1;
		string line = null;
		TextReader reader;

    if(Persistence.debugMode){
			FileStream scriptFile = new FileStream("script_debug.txt", FileMode.Open, FileAccess.Read);
			reader = new StreamReader(scriptFile);
		}else{
			reader = new StringReader(currentScript.text);
		}

		while( (line = reader.ReadLine()) != null ){
      if(line.Length <= 1){
        lineCount++;
        continue;
      }

      if( Command.GetCommand(line) == "waypoint" ){
        int start = line.IndexOf("{");
        int end = line.IndexOf(" ", start + 1);
        if(end <= 0)
          end = line.Length - 1;
        else
          end--;
        string wp = line.Substring(start + 1, end - start);
        //Debug.Log (wp);
        waypoints.Add(wp, lineCount);
      }

      if( Command.GetCommand(line) == "checkpoint" ){
        string[] param = Command.GetParams(line);
        int i = int.Parse(param[0]);
        checkpoints[i] = lineCount;
      }

      if( Command.GetCommand(line) == "mouth_anim" ){
        commandController.CheckCommand(line, lineCount);
      }
			
      if( Command.GetCommand(line) == "eye_blink_anim" ){
        commandController.CheckCommand(line, lineCount);
      }

			lineCount++;
		}
		totalLines = lineCount;
		
		reader.Close();
		return waypoints;
	}


  public static bool LineStartsWith(string line, string startsWith){
    int length = startsWith.Length;

    if( line.Length >= length ){        
      if( line.Substring(0, length).ToLower() == startsWith ){
        return true;
      }
    }
    return false;
  }


	public string PathForDocumentsFile(string filename) {
		if(Application.platform == RuntimePlatform.IPhonePlayer){
			string path = Application.dataPath.Substring( 0, Application.dataPath.Length - 5 );
			path = path.Substring( 0, path.LastIndexOf( '/' ) );
			return Path.Combine( Path.Combine( path, "Documents" ), filename );
		}
		
		else if(Application.platform == RuntimePlatform.Android){
			string resources = Path.Combine(Application.persistentDataPath, "Resources");
			string path = Path.Combine(resources, filename);
			return path;
		}
		else {
			string path = Application.dataPath + "/../" + filename;
			return path;
		}
	}
}
