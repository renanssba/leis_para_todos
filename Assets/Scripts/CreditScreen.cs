﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class CreditScreen : MonoBehaviour {

	public float screenTime = 3;
	public float fadeTime = 1;
	public string nextScene;
	public bool autoFadeOut = true;
	public Fade fade;

	void Start() {
		if( Persistence.debugMode && autoFadeOut ){
      SceneManager.LoadScene(nextScene);
		}
		fade = GameObject.FindWithTag("Fade").GetComponent<Fade>();
		fade.FadeIn(fadeTime);
		if( autoFadeOut )
			Invoke("FadeOutAdvanceScene", screenTime + fadeTime);
	}

	public void FadeOutAdvanceScene(){
		fade.FadeOut(fadeTime);
		StartCoroutine("LoadNextScene", fadeTime);
	}

	IEnumerator LoadNextScene(float time){
		yield return new WaitForSeconds(time);
    SceneManager.LoadScene(nextScene);
	}
}