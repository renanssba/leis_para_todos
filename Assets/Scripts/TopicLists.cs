﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class TopicLists : MonoBehaviour {

  public Text topicsTitle;
  public Topic[] favorableTopics;
  public Topic[] contraryTopics;
  public Sprite okSymbol;
  public Sprite cancelSymbol;


  public void SetTopicsTitle(string title){
    Persistence.SetTopicsTitle(title);
    LoadTopicTitle();
  }

  public void LoadTopicTitle(){
    topicsTitle.text = MenuController.GetPlName();
  }


  public bool AddTopicToList(string[] param){
    bool result = Persistence.SetNewTopic(param);

    UpdateVisibleTopics();
    return result;
  }


  public void UpdateVisibleTopics(){
    Topic topicToSet;

    for(int i=0; i<favorableTopics.Length; i++){
      if( Persistence.IsTopicActive("favor", i) ){
        favorableTopics[i].gameObject.SetActive(true);
        topicToSet = favorableTopics[i];
        topicToSet.text.text = Persistence.GetTopicName("favor", i);
        topicToSet.active = true;
        UpdateRelevanceIcon(topicToSet, "favor", i);
      }else{
        favorableTopics[i].gameObject.SetActive(false);
      }

      if( Persistence.IsTopicActive("contrario", i) ){
        contraryTopics[i].gameObject.SetActive(true);
        topicToSet = contraryTopics[i];
        topicToSet.text.text = Persistence.GetTopicName("contrario", i);
        topicToSet.active = true;
        UpdateRelevanceIcon(topicToSet, "contrario", i);
      }else{
        contraryTopics[i].gameObject.SetActive(false);
      }
    }
  }

  public void UpdateRelevanceIcon(Topic t, string listName, int id){
    string relevance = Persistence.GetTopicRelevance(listName, id);
    if( relevance == "true" ){
      t.relevanceImg.sprite = okSymbol;
    }else if( relevance == "false" ){
      t.relevanceImg.sprite = cancelSymbol;
    }
  }


  public void SetTopicRelevance(string[] param){
    Persistence.SetTopicRelevance(param[0], int.Parse(param[1]), param[2]);

    UpdateVisibleTopics();
  }


  public void Initialize(){
    LoadTopicTitle();
    UpdateVisibleTopics();
  }



  public void DebugPrint(){
//    //Debug.log("Favorable topics:");
//    foreach(Topic t in favorableButtons){
//      //Debug.log("Topic "+ t.id + ": " + t.text);
//    }
//
//    //Debug.log("Contrary topics:");
//    foreach(Topic t in contraryButtons){
//      //Debug.log("Topic "+ t.id + ": " + t.text);
//    }
  }

}
