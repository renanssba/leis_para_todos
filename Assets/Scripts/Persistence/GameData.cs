﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class GameData{

	public static string filePath = Application.persistentDataPath + "Saves/savedGame.gd";
	public static GameData currentGame = new GameData();

	public int scriptLine { get; set;}
	public IList<int> itemsId;
	public IList<int> allObjectsId;
	public string music;
	public IList<Character> characters;
	public string dialogCharacterName;
	public bool evidence;
	public bool inventory_btn;
	public bool handbool_btn;
	public Dictionary<string, int> customVar;


	/*private SerializeVector3 _SpawnPoint;
	public Vector3 SpawnPoint {
		get {
			if(_SpawnPoint == null) {
				return Vector3.zero;
			} else {
				return(Vector3)_SpawnPoint;
			}
		} 
		set {
			_SpawnPoint =(SerializeVector3)value;
		}
	}*/
}
/*
[System.Serializable]
public class SerializeVector3 {
	
	private float x;
	private float y;
	private float z;
	
	public SerializeVector3(Vector3 vec3) {
		this.x = vec3.x;
		this.y = vec3.y;
		this.z = vec3.z;
	}
	
	public static implicit operator SerializeVector3(Vector3 vec3) {
		return new SerializeVector3(vec3);
	}
	public static explicit operator Vector3(SerializeVector3 serial_vec3) {
		return new Vector3(serial_vec3.x, serial_vec3.y, serial_vec3.z);
	}
}*/
