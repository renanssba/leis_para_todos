﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class Persistence : MonoBehaviour {
  
  public static int chapter_to_load = 1;
  public static int checkpoint_to_load = 0;
  public static bool debugMode = false;
  public static bool finishedLoadingAnims = false;
  public static bool fastRead = false;
  public static int matchVictories;
  public static string[] topicName;
  public bool isLoadingGame = false;

  public string encryptionKey;
  private readonly string achievementPrefix = "ACHIEVEMENT-";
  private readonly string leaderboardHighscorePrefix = "LEADERBOARD_HIGHSCORE-";

  private static Persistence instance = null;
  private static Dictionary<string, int> customVar = new Dictionary<string, int>();

	
  void Awake(){

    if(instance == null){
      instance = this;
      DontDestroyOnLoad(transform.gameObject);
    }else if(instance != this){
      Destroy(gameObject);
    }

    LoadDebugMode();
  }

	void LoadPersistableVariables(){
		if(PlayerPrefs.HasKey("persistableVariables")){
			string variables = PlayerPrefs.GetString("persistableVariables");
			
      foreach(string variable in variables.Split('@')){
				if (variable.Length > 0){
					if(!customVar.ContainsKey(variable)) {
						customVar.Add(variable, PlayerPrefs.GetInt(variable));
					} else{
						customVar[variable] = PlayerPrefs.GetInt(variable);
					}

          Debug.Log("Var '" + variable + "' value: " + PlayerPrefs.GetInt(variable) );
				}
			}
		}
	}


  public static void ResetTopics() {
    for(int i = 0; i < 4; i++) {
      SetTopicActive("favor", i, "false");
      SetTopicActive("contrario", i, "false");
      SetTopicsTitle("---");
    }
  }


  public static bool SetNewTopic(string[] param) {
    string prefName;

    if(param.Length < 3) {
      Debug.LogError("Script Error! Insuficient number of parameters");
      return false;
    }

    if(param[0] == "favor" || param[0] == "contrario") {
      prefName = param[0];
    } else {
      //Debug.log("Script Error! "+param[0]+" is not a valid topic list");
      return false;
    }

    if(IsTopicActive(prefName, int.Parse(param[1]))) {
      //Debug.logError("Script Error! "+param[0]+" is already set");
      return false;
    }

    SetTopicName(prefName, int.Parse(param[1]), param[2]);
    SetTopicActive(prefName, int.Parse(param[1]), "true");
    SetTopicRelevance(prefName, int.Parse(param[1]), "none");

    return true;
  }

  public static void SetTopicsTitle(string value) {
    PlayerPrefs.SetString("topicsTitle", value);
  }

  public static void SetTopicName(string listName, int key, string value) {
    PlayerPrefs.SetString(listName + key + "name", value);
  }

  public static void SetTopicActive(string listName, int key, string value) {
    PlayerPrefs.SetString(listName + key + "active", value);
  }

  public static void SetTopicRelevance(string listName, int key, string value) {
    PlayerPrefs.SetString(listName + key + "relevance", value);
  }

  public static bool IsTopicActive(string listName, int key) {
    if(PlayerPrefs.GetString(listName + key + "active") == "true") {
      return true;
    }
    return false;
  }

  public static string GetTopicsTitle() {
    return PlayerPrefs.GetString("topicsTitle", "???");
  }

  public static string GetTopicName(string listName, int key) {
    return PlayerPrefs.GetString(listName + key + "name", "");
  }

  public static string GetTopicRelevance(string listName, int key) {
    return PlayerPrefs.GetString(listName + key + "relevance", "");
  }

	public static void SetLastSavepoint(string savepointName){
		// NOT the old implementation of Carcará's checkpoint. New savepoint system.
		PlayerPrefs.SetString("lastSavepoint", savepointName);
	}

	public static string GetLastSavepoint (){
		if (PlayerPrefs.HasKey("lastSavepoint"))
			return PlayerPrefs.GetString("lastSavepoint");
		else 
			return "";
	}


  public static Persistence GetInstance() {
    if(instance == null) {
      return GameObject.FindWithTag("Persistence").GetComponent<Persistence>();
    } else {
      return instance;
    }
  }

  public int GetVariableValue(string key) {
    if(customVar.ContainsKey(key)) {
      return customVar[key];
    }
    return 0;
  }

  public int GetVariableValue(string key, int defaultValue) {
    if(customVar.ContainsKey(key)) {
      return customVar[key];
    }
    return defaultValue;
  }

  public void SaveGame() {
    //ListAllVariables();
		
    if(!customVar.ContainsKey("hasSave")){
      customVar.Add("hasSave", 1);		
		}else{
      customVar["hasSave"] = 1;
		}

		string persistableVariablesString = "";

    foreach(KeyValuePair<string, int> entry in customVar) {
      if(entry.Key != "musicVolume" && entry.Key != "sfxVolume" && entry.Key != "textSpeed") {
        PlayerPrefs.SetInt(entry.Key, customVar[entry.Key]);
      }
      PlayerPrefs.SetInt("Chapter", chapter_to_load);

			persistableVariablesString += entry.Key + "@";
    }

		PlayerPrefs.SetString("persistableVariables", persistableVariablesString);

		PlayerPrefs.Save();
    Debug.Log("Saving on day " + customVar["dia"]);

		Debug.Log("----------List of variables saved-----------");
		ListAllVariables();
		Debug.Log("--------------------------------------------");
  }

  public bool LoadGame() {		
    if(PlayerPrefs.HasKey("hasSave")) {
      if(customVar.ContainsKey("hasSave") == false) {
        customVar.Add("hasSave", PlayerPrefs.GetInt("hasSave"));
      }
    } else {
      return false;
    }

    if(customVar.ContainsKey("hasSave") && customVar["hasSave"] == 1) {
      chapter_to_load = PlayerPrefs.GetInt("Chapter");
      PlayerPrefs.SetString("checkpoint_name", "menu_dia");

			LoadPersistableVariables();
//      foreach(string s in persistableVariables) {
//        if(!customVar.ContainsKey(s)) {
//          customVar.Add(s, PlayerPrefs.GetInt(s));
//        }
//      }
      return true;
    } else {
      return false;
    }


  }

  public void SetVariableValue(string key, int value) {
    if(customVar.ContainsKey(key)) {
      customVar[key] = value;
    } else {
      customVar.Add(key, value);
    }
  }

  public void ListAllVariables() {
    foreach(KeyValuePair<string, int> entry in customVar) {
      Debug.Log(entry.Key + " - " + customVar[entry.Key]);
    }
  }


  void Start() {		
    // initialize registries
    Persistence.InitializeRegistries();

    // load all characters animations in parallel
    //StartCoroutine("CreateCharacterAnimsCoroutineDeprecated");
  }

  public void CreateCharacterAnims() {
		
    int lineCount = 1;
    string line;
    finishedLoadingAnims = false;
    TextReader animReader;
    if(Persistence.debugMode) {
      FileStream scriptFile = new FileStream("CharacterAnims.txt", FileMode.Open, FileAccess.Read);
      animReader = new StreamReader(scriptFile);
    } else {
//      animReader = new StringReader(animScript[Persistence.chapter_to_load - 1].text);
      TextAsset animsAsset = (TextAsset)Resources.Load("CharacterAnims");
      if(animsAsset == null) {
        Debug.LogError("Error loading character anims file!!!");
      }

      animReader = new StringReader(animsAsset.text);
    }
		
    while((line = animReader.ReadLine()) != null) {			
      if(line.Length <= 1){
        lineCount++;
        continue;
      }
			
      if(ScriptReader.LineStartsWith(line, "create_mood")) {
        Command.CheckAnimCommand(line, lineCount);
      }
			
      lineCount++;
    }
    //		//Debug.log("FINISHED LOADING ANIMS!");
    finishedLoadingAnims = true;
		
    animReader.Close();
  }

  public static void DeleteCharacterAnims() {
    CharacterAnimations.DeleteAnimations();
  }


  public static bool ReachCheckpoint(int chapterIndex, int checkpointIndex) {

//		//Debug.log("Checkpoint reached! chapter: "+chapterIndex+", checkpoint: "+checkpointIndex);
    SetLastPlayed(chapterIndex, checkpointIndex);
    return SetChapterProgress(chapterIndex, checkpointIndex);
  }

  public static bool Load(int chapterIndex, int checkpointIndex) {

    // save it as the last checkpoint used
    ReachCheckpoint(chapterIndex, checkpointIndex);

    checkpoint_to_load = checkpointIndex;
    chapter_to_load = chapterIndex;

//		//Debug.log("LOADING");
//		//Debug.log("Loading Chapter: "+chapter_to_load);
//		//Debug.log("Loading Checkpoint: "+checkpoint_to_load);

    SceneManager.LoadScene("Chapter");
    return true;
  }

  public static void SetChapterAccess(int chapterIndex, int access) {
		
    string chapterAccessKey = "Chapter" + chapterIndex + "Access";
    PlayerPrefs.SetInt(chapterAccessKey, access);
    PlayerPrefs.Save();
  }

  public static int IsChapterAccessible(int chapterIndex) {
		
    string chapterAccessKey = "Chapter" + chapterIndex + "Access";

    return PlayerPrefs.GetInt(chapterAccessKey);
  }


  public static void SetLastPlayed(int chapterIndex, int checkpointIndex) {
	
    PlayerPrefs.SetInt("LastPlayedChapter", chapterIndex);
    PlayerPrefs.SetInt("LastPlayedCheckpoint", checkpointIndex);
    PlayerPrefs.Save();
  }

  public static int GetLastPlayedChapter() {

    return PlayerPrefs.GetInt("LastPlayedChapter", -1);
  }

  public static int GetLastPlayedCheckpoint() {
		
    return PlayerPrefs.GetInt("LastPlayedCheckpoint");
  }

  public static bool SetChapterProgress(int chapterIndex, int latestCheckpoint) {
		
    string progressKey = "Chapter" + chapterIndex + "Progress";
    int last = -5;

    last = PlayerPrefs.GetInt(progressKey, -5); // -5 if the key does not exist

    if(last < latestCheckpoint) {
      PlayerPrefs.SetInt(progressKey, latestCheckpoint);
      PlayerPrefs.Save();
      return true;
    }
    return false;
  }

  public static int GetChapterProgress(int chapterIndex) {

    string progressKey = "Chapter" + chapterIndex + "Progress";
    return PlayerPrefs.GetInt(progressKey);
  }

  public static int GetScene(string sceneName) {

    switch(sceneName) {
      case "Credits1":
        return 0;
      case "Credits2":
        return 1;
      case "TitleScreen":
        return 2;
      case "Chapter":
        return 3;
      default:
//			//Debug.log("Scene Invalid.");
        return -1;
    }
  }

  public static void InitializeRegistries() {
    int i;
    string chapterAccessKey;
		
//		//Debug.log("Initializing ChapterLock Registries");
		
    // initialize Chapter Access
    if(!PlayerPrefs.HasKey("Chapter1Access")) {
      SetChapterAccess(1, 1);
    }
    for(i = 1; i <= 4; i++) {
      chapterAccessKey = "Chapter" + i + "Access";
			
      // initialize Chapters Access
      if(!PlayerPrefs.HasKey(chapterAccessKey)) {
        SetChapterAccess(i, 0);
      }
			
			
      // initialize progress in the chapter
      Persistence.SetChapterProgress(i, -1);
    }
		
    // initialize Registries for Continue
    if(!PlayerPrefs.HasKey("LastPlayedChapter")) {
      PlayerPrefs.SetInt("LastPlayedChapter", -1);
      PlayerPrefs.SetInt("LastPlayedCheckpoint", -1);
    }
		
    PlayerPrefs.Save();
  }

  public static void SetDebugMode(bool activated) {
    // save persistence debug mode
    debugMode = activated;
    PlayerPrefs.SetInt("DebugMode", debugMode ? 1 : 0);
    UpdateIndicatorRender();
  }

  public static void LoadDebugMode() {
    // load persistence debug mode
    debugMode = (PlayerPrefs.GetInt("DebugMode", 0) == 1) ? true : false;
    UpdateIndicatorRender();
  }

  public static void UpdateIndicatorRender(){
    GameObject debugShow = GameObject.FindWithTag("DebugShow");
    if(debugShow != null){
      debugShow.GetComponent<DebugShow>().UpdateRender();
    }
  }

  public void SaveLeaderboardHighscore(int leaderboardChapter, int score){
    SecurePlayerPrefs.SetInt(leaderboardHighscorePrefix + leaderboardChapter.ToString(), score, encryptionKey);
  }

  public int GetLeaderboardHighscore(int leaderboardChapter){
    string key = leaderboardHighscorePrefix + leaderboardChapter.ToString();
    if (SecurePlayerPrefs.HasKey(key)){
      return SecurePlayerPrefs.GetInt(key, encryptionKey);
    } else{
      return 0;
    }
  }

  public void SaveLocalAchievementProgress(Achievement achievement){
    //SetVariableValue(achievement.achievementName + "");

    SecurePlayerPrefs.SetInt(this.achievementPrefix + achievement.achievementName + "-progress", achievement.progress, encryptionKey);
    SecurePlayerPrefs.SetInt(this.achievementPrefix + achievement.achievementName + "-status", (int) achievement.status, encryptionKey);
  }


  public bool LoadLocalAchievementProgress(Achievement achievement){
    if (PlayerPrefs.HasKey(this.achievementPrefix + achievement.achievementName + "-status")){
      int progress = SecurePlayerPrefs.GetInt(this.achievementPrefix + achievement.achievementName + "-progress", encryptionKey);
      int statusInt = SecurePlayerPrefs.GetInt(this.achievementPrefix + achievement.achievementName + "-status", encryptionKey);

      achievement.status = (Achievement.Status) statusInt;
      achievement.progress = progress;

      return true;
    }
    else{
      Debug.Log("<color=red>DID NOT FIND local achievement to load:" + achievement.achievementName + "</color>");
      SaveLocalAchievementProgress(achievement);
      return false;
    }
  }

}
