﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;

public class StatusScreen : MonoBehaviour {

  public Text plText;
  public Options optionsScreen;
  public SkillsScreen skillsScreen;
  public TopicLists topicLists;
  public RelationshipLoader relationshipLoader;
  public GameObject[] perfectSpeechIcon;
  public GameObject[] perfectResearchIcon;


	public void OpenStatusScreen(){
    AudioController.GetInstance().PlayPageSound();

		gameObject.SetActive(true);
    if(relationshipLoader != null) {
      relationshipLoader.LoadRelationships();
    }
    if(skillsScreen != null){
      skillsScreen.Initialize();
    }
    if(topicLists != null){
      topicLists.Initialize();
    }

//		GetComponent<CanvasGroup>().alpha = 0;
//		GetComponent<CanvasGroup>().DOFade(1f, 0.25f);
    GetComponent<CanvasGroup>().alpha = 1f;

    if(optionsScreen != null) {
      optionsScreen.Open();
    }


    if(plText != null){
      plText.text = GetPlsProgressString();
    }

    if(perfectSpeechIcon.Length != 0){
      UpdateProgressIcons();
    }
	}


  public string GetPlsProgressString(){
    string progressString = "";

    progressString += "Transgênicos ";
    if(Persistence.GetInstance().GetVariableValue("current_chapter") >= 1) {
      progressString += ProgressString("status_transgenicos");
    }
     
    if(Persistence.GetInstance().GetVariableValue("current_chapter") >= 2) {
      progressString += "\nDesarmamento ";
      progressString += ProgressString("status_desarmamento");
    }

    if(Persistence.GetInstance().GetVariableValue("current_chapter") >= 3) {
      progressString += "\nInterrupção Gravidez ";
      progressString += ProgressString("status_aborto");
    }

    if(Persistence.GetInstance().GetVariableValue("current_chapter") >= 4) {
      progressString += "\nPolícia Legislativa ";
      progressString += ProgressString("status_corrupcao");
    }
    return progressString;
  }

  public string ProgressString(string plName){
    int progress = Persistence.GetInstance().GetVariableValue(plName);

    switch(progress){
      case 0:
        return "(Em progresso)";
      case 1:
        return "(Aprovado)";
      case 2:
        return "(Reprovado)";
    }
    return "";
  }


  void UpdateProgressIcons(){
    for(int i=0; i<4; i++){
      if(Persistence.GetInstance().GetVariableValue("discurso_perfeito_cap"+(i+1).ToString() ) == 1){
        perfectSpeechIcon[i].SetActive(true);
      }else{
        perfectSpeechIcon[i].SetActive(false);
      }

      if(Persistence.GetInstance().GetVariableValue("pesquisa_perfeita_cap"+(i+1).ToString() ) == 1){
        perfectResearchIcon[i].SetActive(true);
      }else{
        perfectResearchIcon[i].SetActive(false);
      }
    }
  }


	public void HideStatusScreen(){
		if (!DOTween.IsTweening(GetComponent<CanvasGroup>())){
			GetComponent<CanvasGroup>().DOFade(0f, 0.25f);
			transform.DOMoveY(-0.25f,0.25f).SetRelative().OnComplete(()=>{
				transform.DOMoveY(0.25f, 0f).SetRelative();
				gameObject.SetActive(false);

			});
		}
//		grayBg.DOFade(0f, 0f);
	}
}
